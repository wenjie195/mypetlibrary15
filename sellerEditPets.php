<?php
require_once dirname(__FILE__) . '/sellerAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Breed.php';
require_once dirname(__FILE__) . '/classes/Color.php';
require_once dirname(__FILE__) . '/classes/Pets.php';
require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];
$sellerName = $userData->getName();

$colorDetails = getColor($conn," WHERE type = ? ",array("type"),array(1),"s");
$breedDetails = getBreed($conn," WHERE type = ? ",array("type"),array(1),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Pets Details | Mypetslibrary" />
<title>Edit Pets Details | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Edit Pets Details</h1>
            <div class="green-border"></div>
   </div>
   <div class="border-separation">
        <div class="clear"></div>

        <!-- <form method="POST" action="utilities/editPuppyFunction.php" enctype="multipart/form-data"> -->
        <form method="POST" action="utilities/sellerEditPetsFunction.php" enctype="multipart/form-data">
        <?php
            if(isset($_POST['pet_uid']))
            {
                $conn = connDB();
                $puppyDetails = getPetsDetails($conn,"WHERE uid = ? ", array("uid") ,array($_POST['pet_uid']),"s");
            ?>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Pet Name*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $puppyDetails[0]->getName();?>"  name="update_name" id="update_name" required>      
                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">SKU*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $puppyDetails[0]->getSKU();?>"  name="update_sku" id="update_sku" required> 
                </div>        
                <div class="clear"></div>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Pet Slug (for URL, no spacing or contain any symbol except -)</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $puppyDetails[0]->getSlug();?>"   name="update_slug" id="update_slug" required>      
                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Pet Price (RM)*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $puppyDetails[0]->getPrice();?>"   name="update_price" id="update_price" required>    
                </div> 
                <div class="clear"></div>  
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Pet Age*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $puppyDetails[0]->getAge();?>"  name="update_age" id="update_age" required>      
                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Vaccinated Status*</p>
                    <select class="clean input-name admin-input" name="update_vaccinated" id="update_vaccinated" value="<?php echo $puppyDetails[0]->getVaccinated();?>" required>
                        <!-- <option>Yes</option>
                        <option>No</option> -->
                        <?php
                            if($puppyDetails[0]->getVaccinated() == '')
                            {
                            ?>
                                <option value="Yes"  name='Yes'>Yes</option>
                                <option value="No"  name='No'>No</option>
                                <option selected value=""  name=''></option>
                            <?php
                            }
                            else if($puppyDetails[0]->getVaccinated() == 'No')
                            {
                            ?>
                                <option value="Yes"  name='Yes'>Yes</option>
                                <option selected value="No"  name='No'>No</option>
                            <?php
                            }
                            else if($puppyDetails[0]->getVaccinated() == 'Yes')
                            {
                            ?>
                                <option selected value="Yes"  name='Yes'>Yes</option>
                                <option value="No"  name='No'>No</option>
                        <?php
                        }
                        ?>
                    </select>   
                </div>           
                <div class="clear"></div>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Dewormed Status*</p>
                    <select class="clean input-name admin-input" name="update_dewormed" id="update_dewormed" value="<?php echo $puppyDetails[0]->getDewormed();?>" required>
                        <!-- <option>Yes</option>
                        <option>No</option> -->
                        <?php
                            if($puppyDetails[0]->getDewormed() == '')
                            {
                            ?>
                                <option value="Yes"  name='Yes'>Yes</option>
                                <option value="No"  name='No'>No</option>
                                <option selected value=""  name=''></option>
                            <?php
                            }
                            else if($puppyDetails[0]->getDewormed() == 'No')
                            {
                            ?>
                                <option value="Yes"  name='Yes'>Yes</option>
                                <option selected value="No"  name='No'>No</option>
                            <?php
                            }
                            else if($puppyDetails[0]->getDewormed() == 'Yes')
                            {
                            ?>
                                <option selected value="Yes"  name='Yes'>Yes</option>
                                <option value="No"  name='No'>No</option>
                        <?php
                        }
                        ?>
                    </select>       
                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Gender*</p>
                    <select class="clean input-name admin-input"  name="update_gender" id="update_gender" value="<?php echo $puppyDetails[0]->getGender();?>" required>
                        <!-- <option>Male</option>
                        <option>Female</option> -->
                        <?php
                            if($puppyDetails[0]->getGender() == '')
                            {
                            ?>
                                <option value="Male"  name='Male'>Male</option>
                                <option value="Female"  name='Female'>Female</option>
                                <option selected value=""  name=''></option>
                            <?php
                            }
                            else if($puppyDetails[0]->getGender() == 'Female')
                            {
                            ?>
                                <option value="Male"  name='Male'>Male</option>
                                <option selected value="Female"  name='Female'>Female</option>
                            <?php
                            }
                            else if($puppyDetails[0]->getGender() == 'Male')
                            {
                            ?>
                                <option selected value="Male"  name='Male'>Male</option>
                                <option value="Female"  name='Female'>Female</option>
                        <?php
                        }
                        ?>
                    </select>   
                </div>  
                
                <div class="clear"></div>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Pet Color*</p>
                    <select class="clean input-name admin-input" name="update_color" id="update_color" value="<?php echo $puppyDetails[0]->getColor();?>" required>
                        <!-- <option>White</option>
                        <option>Black</option> -->
                        <?php
                        if($puppyDetails[0]->getColor() == ''){
                        ?>
                            <option selected>Please Select a Color</option>
                            <?php
                            for ($cntPro=0; $cntPro <count($colorDetails) ; $cntPro++){
                            ?>
                                <option value="<?php echo $colorDetails[$cntPro]->getName(); ?>"> 
                                    <?php echo $colorDetails[$cntPro]->getName(); ?>
                                </option>
                            <?php
                            }
                        }
                        else{
                            for ($cntPro=0; $cntPro <count($colorDetails) ; $cntPro++){
                                if ($puppyDetails[0]->getColor() == $colorDetails[$cntPro]->getName())
                                {
                                ?>
                                    <option selected value="<?php echo $colorDetails[$cntPro]->getName(); ?>"> 
                                        <?php echo $colorDetails[$cntPro]->getName(); ?>
                                    </option>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <option value="<?php echo $colorDetails[$cntPro]->getName(); ?>"> 
                                        <?php echo $colorDetails[$cntPro]->getName(); ?>
                                    </option>
                                <?php
                                }
                            }
                        }
                        ?>
                    </select>       
                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Pet Size</p>
                    <select class="clean input-name admin-input" name="update_size" id="update_size" value="<?php echo $puppyDetails[0]->getSize();?>" required>
                        <!-- <option>Small</option>
                        <option>Middle</option>
                        <option>Big</option> -->
                        <?php
                            if($puppyDetails[0]->getSize() == '')
                            {
                            ?>
                                <option value="Small"  name='Small'>Small</option>
                                <option value="Medium"  name='Medium'>Medium</option>
                                <option value="Large"  name='Large'>Large</option>
                                <option selected value="" name=''></option>
                            <?php
                            }
                            else if($puppyDetails[0]->getSize() == 'Large')
                            {
                            ?>
                                <option value="Small"  name='Small'>Small</option>
                                <option value="Medium"  name='Medium'>Medium</option>
                                <option selected value="Large" name='Large'>Large</option>
                            <?php
                            }
                            else if($puppyDetails[0]->getSize() == 'Medium')
                            {
                            ?>
                                <option value="Small"  name='Small'>Small</option>
                                <option selected value="Medium"  name='Medium'>Medium</option>
                                <option value="Large" name='Large'>Large</option>
                            <?php
                            }
                            else if($puppyDetails[0]->getSize() == 'Small')
                            {
                            ?>
                                <option selected value="Small"  name='Small'>Small</option>
                                <option value="Medium" name='Medium'>Medium</option>
                                <option value="Large"  name='Large'>Large</option>
                        <?php
                        }
                        ?>
                    </select>   
                </div>  
                <div class="clear"></div>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Status*</p>
                    <select class="clean input-name admin-input"  name="update_status" id="update_status" value="<?php echo $puppyDetails[0]->getStatus();?>" required>
                        <!-- <option>Available</option>
                        <option>Sold</option> -->
                        <?php
                            if($puppyDetails[0]->getStatus() == '')
                            {
                            ?>
                                <option value="Pending"  name='Pending'>Pending</option>
                                <option value="Available"  name='Available'>Available</option>
                                <option value="Sold"  name='Sold'>Sold</option>
                                <option value="Rejected"  name='Rejected'>Rejected</option>
                                <option selected value=""  name=''></option>
                            <?php
                            }
                            else if($puppyDetails[0]->getStatus() == 'Pending')
                            {
                            ?>
                                <option selected value="Pending"  name='Pending'>Pending</option>
                                <option value="Available"  name='Available'>Available</option>
                                <option value="Sold"  name='Sold'>Sold</option>
                                <option value="Rejected"  name='Rejected'>Rejected</option>
                            <?php
                            }
                            else if($puppyDetails[0]->getStatus() == 'Available')
                            {
                            ?>
                                <option selected value="Available"  name='Available'>Available</option>
                                <option value="Pending"  name='Pending'>Pending</option>
                                <option value="Sold"  name='Sold'>Sold</option>
                                <option value="Rejected"  name='Rejected'>Rejected</option>
                            <?php
                            }
                            else if($puppyDetails[0]->getStatus() == 'Sold')
                            {
                            ?>
                                <option selected value="Sold"  name='Sold'>Sold</option>
                                <option value="Pending"  name='Pending'>Pending</option>
                                <option value="Available"  name='Available'>Available</option>
                                <option value="Rejected"  name='Rejected'>Rejected</option>
                            <?php
                            }
                            else if($puppyDetails[0]->getStatus() == 'Rejected')
                            {
                            ?>
                                <option selected value="Rejected"  name='Rejected'>Rejected</option>
                                <option value="Pending"  name='Pending'>Pending</option>
                                <option value="Available"  name='Available'>Available</option>
                                <option value="Sold"  name='Sold'>Sold</option>
                            <?php
                            }
                        ?>
                    </select>       
                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Feature</p>
                    <select class="clean input-name admin-input" name="update_feature" id="update_feature" value="<?php echo $puppyDetails[0]->getFeature();?>" required>
                        <!-- <option>Yes</option>
                        <option>No</option> -->
                        <?php
                            if($puppyDetails[0]->getFeature() == '')
                            {
                            ?>
                                <option value="Yes"  name='Yes'>Yes</option>
                                <option value="No"  name='No'>No</option>
                                <option selected value=""  name=''></option>
                            <?php
                            }
                            else if($puppyDetails[0]->getFeature() == 'No')
                            {
                            ?>
                                <option value="Yes"  name='Yes'>Yes</option>
                                <option selected value="No"  name='No'>No</option>
                            <?php
                            }
                            else if($puppyDetails[0]->getFeature() == 'Yes')
                            {
                            ?>
                                <option selected value="Yes"  name='Yes'>Yes</option>
                                <option value="No"  name='No'>No</option>
                        <?php
                        }
                        ?>
                    </select>   
                </div>          
                <div class="clear"></div>       
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Pet Breed*</p>
                    <select class="clean input-name admin-input" name="update_breed" id="update_breed" value="<?php echo $puppyDetails[0]->getBreed();?>" required>
                        <!-- <option>Husky</option>
                        <option>Corgi</option> -->
                        <?php
                        if($puppyDetails[0]->getBreed() == ''){
                        ?>
                            <option selected>Please Select a Breed</option>
                            <?php
                            for ($cntPro=0; $cntPro <count($breedDetails) ; $cntPro++){
                            ?>
                                <option value="<?php echo $breedDetails[$cntPro]->getName(); ?>"> 
                                    <?php echo $breedDetails[$cntPro]->getName(); ?>
                                </option>
                            <?php
                            }
                        }
                        else{
                            for ($cntPro=0; $cntPro <count($breedDetails) ; $cntPro++){
                                if ($puppyDetails[0]->getBreed() == $breedDetails[$cntPro]->getName())
                                {
                                ?>
                                    <option selected value="<?php echo $breedDetails[$cntPro]->getName(); ?>"> 
                                        <?php echo $breedDetails[$cntPro]->getName(); ?>
                                    </option>
                                <?php
                                }
                                else
                                {
                                ?>
                                    <option value="<?php echo $breedDetails[$cntPro]->getName(); ?>"> 
                                        <?php echo $breedDetails[$cntPro]->getName(); ?>
                                    </option>
                                <?php
                                }
                            }
                        }
                        ?>
                    </select>       
                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Seller*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $puppyDetails[0]->getSeller();?>" name="update_seller" id="update_seller" required>                        
                </div>         
                <div class="clear"></div>
                <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Details</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $puppyDetails[0]->getDetails();?>" name="update_details" id="update_details" required>           
                </div>
                <div class="clear"></div>
                <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Video Link*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $puppyDetails[0]->getLink();?>"  required name="update_link" id="update_link">           
                </div>
                <div class="clear"></div>  
                <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Pet Photo X4*</p>
                    <!-- Photo cropping into square size feature -->
                    <div class="four-div-box1">
                    <img src="img/dog.jpg" class="pet-photo-preview">
                    <p><input class="upload-file" type="file"></p>
                    </div>
                    <div class="four-div-box1 left-four-div1">
                    <img src="img/dog.jpg" class="pet-photo-preview">
                    <p><input class="upload-file" type="file"></p>
                    </div>
                    <div class="four-div-box1 right-four-div1">               
                    <img src="img/dog.jpg" class="pet-photo-preview">
                    <p><input class="upload-file" type="file"></p>
                    </div>                    
                    <div class="four-div-box1">
                    <img src="img/dog.jpg" class="pet-photo-preview">
                    <p><input class="upload-file" type="file"></p>
                    </div>
                                     
                    
                </div>             
                <div class="clear"></div>

                <input type="hidden" id="pet_uid" name="pet_uid" value="<?php echo $puppyDetails[0]->getUid() ?>" readonly>
                <input type="hidden" id="pet_type" name="pet_type" value="<?php echo $puppyDetails[0]->getType() ?>" readonly>
        <?php
        }
        ?>
    	

        <div class="clear"></div>  
        <div class="width100 overflow text-center">     
        	<button class="green-button white-text clean2 edit-1-btn margin-auto" type="submit" id ="update" name ="update">Submit</button>
        </div>
        </form>
	</div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>