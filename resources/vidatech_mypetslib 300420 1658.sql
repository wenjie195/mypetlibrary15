-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 30, 2020 at 10:58 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_mypetslib`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `author_uid` varchar(255) DEFAULT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `article_link` varchar(255) DEFAULT NULL,
  `keyword_one` varchar(255) DEFAULT NULL,
  `keyword_two` varchar(255) DEFAULT NULL,
  `title_cover` varchar(255) DEFAULT NULL,
  `paragraph_one` text DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `paragraph_two` text DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `paragraph_three` text DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `paragraph_four` text DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `paragraph_five` text DEFAULT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `img_cover_source` text DEFAULT NULL,
  `img_one_source` text DEFAULT NULL,
  `img_two_source` text DEFAULT NULL,
  `img_three_source` text DEFAULT NULL,
  `img_four_source` text DEFAULT NULL,
  `img_five_source` text DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `display` varchar(255) DEFAULT 'YES',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `uid`, `author_uid`, `author_name`, `title`, `seo_title`, `article_link`, `keyword_one`, `keyword_two`, `title_cover`, `paragraph_one`, `image_one`, `paragraph_two`, `image_two`, `paragraph_three`, `image_three`, `paragraph_four`, `image_four`, `paragraph_five`, `image_five`, `img_cover_source`, `img_one_source`, `img_two_source`, `img_three_source`, `img_four_source`, `img_five_source`, `author`, `type`, `display`, `date_created`, `date_updated`) VALUES
(1, 'e7dbffc6970fb74569b3859a50683196', '2984dae6a2927c72561afdce6e68ae05', 'Wiskey', 'mpl article test 1', 'mplarticletest1-mpl-test-one', 'mpl-test-one', 'asd asd, zxc zxc', 'mpl, articles, one', 'e7dbffc6970fb74569b3859a50683196mmexport1573396227303.jpg', '<p><img alt=\"Blog Title\" src=\"http://localhost/mypetlibrary15/img/bg.jpg\" title=\"Blog Title\" /></p>\r\n\r\n<p style=\"text-align:center\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.</p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"Blog Title\" src=\"http://localhost/mypetlibrary15/img/bg.jpg\" title=\"Blog Title\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.</p>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tj pc', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Pending', '2020-04-30 04:54:13', '2020-04-30 05:59:15'),
(2, '3e0542d98a3fa0d56886cb6252ed298e', '2984dae6a2927c72561afdce6e68ae05', 'Wiskey', 'mpl article test 2', 'mplarticletest2-mpl-test-two', 'mpl-test-two', 'asd asd, zxc zxc, bbc bbc', 'mpl, articles, two', '3e0542d98a3fa0d56886cb6252ed298eimage-01.png', '<p><img alt=\"Blog Title\" src=\"http://localhost/mypetlibrary15/img/bg.jpg\" title=\"Blog Title\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.</p>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tevy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Pending', '2020-04-30 05:32:50', '2020-04-30 05:59:19'),
(3, 'c451edc528be772f8c7c836a990513dd', 'd156938c9b63fd253e9cddf89eba10e7', 'admin', 'admin testing articles', 'admintestingarticles-admin-testing-articles', 'admin-testing-articles', 'ini kali la', 'article,testing,end,of,month', 'c451edc528be772f8c7c836a990513ddimage-05.jpg', '<p><img alt=\"\" src=\"https://www.orientaldaily.com.my/storage/resize_cache/images/uploads/news-cover/2020/APRIL_2020/20200430/ismail.jpg/7929e87830b953d4a1b4293149ed9295.jpg\" style=\"width:100%\" /></p>\r\n\r\n<p><a href=\"https://www.orientaldaily.com.my/news/nation\">国内</a></p>\r\n\r\n<p>发布于 2020年04月30日 14时40分 &bull; 最后更新 29分钟前</p>\r\n\r\n<h1>【行动管制】避免超市人潮拥挤　政府建议若2人外出1人负责载送</h1>\r\n\r\n<p>Facebook1.5KTwitterMessengerCopy Link</p>\r\n\r\n<p>（布城30日讯）&nbsp; 虽然政府再放宽限制，允许两人共车外出购物，但政府建议一人负责载送和留在车内等待，避免超市出现人潮拥挤的情况。</p>\r\n\r\n<p>国防部高级部长拿督斯里依斯迈沙比里表示，政府在第一和第二阶段限制一人一车，是因为当时新冠肺炎的病例很多。</p>\r\n\r\n<p>不过，他说，政府收到很多投诉，如妻子需要上班但不会驾车，需要丈夫载送等等。</p>\r\n\r\n<p>&ldquo;同时，我们也看到在超市，人们都遵守政府和超市所制定的标准作业程序，包括保持社交距离，所以我相信就算现在人数可能会增加，但人们还是会遵守这SOP。&rdquo;</p>\r\n\r\n<p>无论如何，他强调，为了避免出现排长龙和人潮拥挤的情况，民众可以选择让妻子下车购物，而丈夫留在车内等候。</p>\r\n\r\n<p>依斯迈沙比里今日在记者会上，针对政府允许两人共车外出后，如何确保维持社交距离一事，作出回应。</p>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tevy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', '2020-04-30 07:20:25', '2020-04-30 07:20:25');

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `uid`, `username`, `bank_name`, `bank_account_holder`, `bank_account_no`, `status`, `date_created`, `date_updated`) VALUES
(1, 'fd1427e01f65783447c12b2e1923caf9', 'user', 'HLB', 'asd asd', '8855228', 'Active', '2020-04-24 04:37:45', '2020-04-27 04:03:13'),
(2, 'fd1427e01f65783447c12b2e1923caf9', 'user', 'man united', 'zxc zxc', '998877', 'Active', '2020-04-24 06:13:13', '2020-04-24 08:20:06'),
(3, 'fd1427e01f65783447c12b2e1923caf9', 'user', 'AMABO', 'wayne tan', '9966330', 'Active', '2020-04-24 08:19:56', '2020-04-24 08:19:56');

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `name`, `status`, `date_created`, `date_updated`) VALUES
(1, 'Brand 1', 'Available', '2020-04-21 10:20:15', '2020-04-21 10:20:15'),
(2, 'Brand 2', 'Available', '2020-04-21 10:22:14', '2020-04-21 10:22:14'),
(3, 'Brand 3', 'Available', '2020-04-21 10:22:50', '2020-04-21 10:22:50');

-- --------------------------------------------------------

--
-- Table structure for table `breed`
--

CREATE TABLE `breed` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `breed`
--

INSERT INTO `breed` (`id`, `name`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'terrier', 'Available', 1, '2020-04-21 06:53:13', '2020-04-21 06:53:13'),
(2, 'terrier', 'Available', 1, '2020-04-21 06:53:43', '2020-04-21 06:53:43'),
(3, 'puddle', 'Available', 1, '2020-04-21 07:01:32', '2020-04-21 07:01:32'),
(4, 'German Shepherd', 'Available', 1, '2020-04-21 08:11:50', '2020-04-21 08:11:50'),
(5, 'Persian', 'Available', 2, '2020-04-21 08:18:14', '2020-04-21 08:18:14'),
(6, 'Bengal', 'Available', 2, '2020-04-21 08:22:58', '2020-04-21 08:22:58'),
(7, 'Ragdoll', 'Available', 2, '2020-04-21 08:51:47', '2020-04-21 08:51:47'),
(8, 'Chihuahua', 'Available', 1, '2020-04-21 08:53:54', '2020-04-21 08:53:54'),
(9, 'Lizard', 'Available', 3, '2020-04-21 09:25:51', '2020-04-21 09:25:51'),
(10, 'Frog', 'Available', 3, '2020-04-21 09:28:28', '2020-04-21 09:28:28');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `status`, `date_created`, `date_updated`) VALUES
(1, 'Grooming', 'Available', '2020-04-21 10:24:39', '2020-04-21 10:24:39'),
(2, 'Accessory', 'Available', '2020-04-21 10:25:19', '2020-04-21 10:25:19'),
(3, 'Food', 'Available', '2020-04-21 10:25:33', '2020-04-21 10:25:33');

-- --------------------------------------------------------

--
-- Table structure for table `color`
--

CREATE TABLE `color` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `color`
--

INSERT INTO `color` (`id`, `name`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'brown', 'Available', 1, '2020-04-21 08:00:50', '2020-04-21 08:00:50'),
(2, 'gold brown', 'Available', 1, '2020-04-21 08:01:55', '2020-04-21 08:01:55'),
(3, 'black', 'Available', 3, '2020-04-21 08:05:41', '2020-04-21 08:05:41'),
(4, 'grey', 'Available', 2, '2020-04-21 08:05:51', '2020-04-21 08:05:51'),
(5, 'white', 'Available', 2, '2020-04-21 09:02:02', '2020-04-21 09:02:02'),
(6, 'Cream', 'Available', 1, '2020-04-21 09:13:20', '2020-04-21 09:13:20'),
(7, 'Orange', 'Available', 2, '2020-04-21 09:15:13', '2020-04-21 09:15:13'),
(8, 'Red', 'Available', 2, '2020-04-21 09:19:46', '2020-04-21 09:19:46'),
(9, 'green', 'Available', 3, '2020-04-21 09:32:48', '2020-04-21 09:32:48');

-- --------------------------------------------------------

--
-- Table structure for table `credit_card`
--

CREATE TABLE `credit_card` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `name_on_card` varchar(255) DEFAULT NULL,
  `card_no` int(255) DEFAULT NULL,
  `card_type` varchar(255) DEFAULT NULL,
  `ccv` int(255) DEFAULT NULL,
  `expiry_date` varchar(255) DEFAULT NULL,
  `billing_address` varchar(255) DEFAULT NULL,
  `postal_code` int(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `credit_card`
--

INSERT INTO `credit_card` (`id`, `uid`, `username`, `name_on_card`, `card_no`, `card_type`, `ccv`, `expiry_date`, `billing_address`, `postal_code`, `status`, `date_created`, `date_updated`) VALUES
(1, 'fd1427e01f65783447c12b2e1923caf9', 'user', 'barry allen', 4455660, 'Debit', 456, '2020-04-30', '234,asd, zxc, qwe', 445566, 'Active', '2020-04-24 05:55:49', '2020-04-27 04:03:19'),
(2, 'fd1427e01f65783447c12b2e1923caf9', 'user', 'clark kent', 55665566, 'Debit', 774, '2020-04-04', 'fgh,sad', 789789, 'Active', '2020-04-24 08:18:59', '2020-04-24 08:18:59');

-- --------------------------------------------------------

--
-- Table structure for table `kitten`
--

CREATE TABLE `kitten` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `vaccinated` varchar(255) DEFAULT NULL,
  `dewormed` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `feature` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `seller` varchar(255) DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kitten`
--

INSERT INTO `kitten` (`id`, `uid`, `name`, `sku`, `slug`, `price`, `age`, `vaccinated`, `dewormed`, `gender`, `color`, `size`, `status`, `feature`, `breed`, `seller`, `details`, `link`, `location`, `image_one`, `image_two`, `image_three`, `image_four`, `date_created`, `date_updated`) VALUES
(1, 'b5fdd1ce4d6f6e861bc42fc1ca4e7ce9', 'Kitty', '202004291322', 'nice-kitty-nice-cat', '1000', '1', 'Yes', 'Yes', 'Female', 'Cream', 'Medium', 'Pending', 'No', 'Chihuahua', 'Wiskey', 'kiity is a normal cat', 'www.youtube.com', NULL, 'b5fdd1ce4d6f6e861bc42fc1ca4e7ce9cat1.jpg', 'b5fdd1ce4d6f6e861bc42fc1ca4e7ce9cat1.jpg', 'b5fdd1ce4d6f6e861bc42fc1ca4e7ce9cat2.jpg', 'b5fdd1ce4d6f6e861bc42fc1ca4e7ce9cat2.jpg', '2020-04-29 05:24:09', '2020-04-29 05:24:09');

-- --------------------------------------------------------

--
-- Table structure for table `pet_details`
--

CREATE TABLE `pet_details` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `vaccinated` varchar(255) DEFAULT NULL,
  `dewormed` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `feature` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `seller` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pet_details`
--

INSERT INTO `pet_details` (`id`, `uid`, `name`, `sku`, `slug`, `price`, `age`, `vaccinated`, `dewormed`, `gender`, `color`, `size`, `status`, `feature`, `breed`, `seller`, `type`, `details`, `link`, `location`, `image_one`, `image_two`, `image_three`, `image_four`, `date_created`, `date_updated`) VALUES
(1, 'f6a9b818af7c7f294b9656184ffa223c', 'Rocky', '202004291213', 'blacky-cheap-cute-puppy', '1888', '1.5', 'Yes', 'Yes', 'Male', 'Cream', 'Large', 'Pending', 'Yes', 'Chihuahua', 'Wiskey', 'Puppy', 'cheap and cute puppy very cheap', 'www.youtube.com/asd', NULL, 'f6a9b818af7c7f294b9656184ffa223c5.jpg', 'f6a9b818af7c7f294b9656184ffa223c6.jpg', 'f6a9b818af7c7f294b9656184ffa223c5.jpg', 'f6a9b818af7c7f294b9656184ffa223c6.jpg', '2020-04-28 09:17:55', '2020-04-28 09:17:55'),
(2, 'b5fdd1ce4d6f6e861bc42fc1ca4e7ce9', 'Kitty', '202004291322', 'nice-kitty-nice-cat', '1000', '1', 'Yes', 'Yes', 'Female', 'Cream', 'Medium', 'Available', 'No', 'Chihuahua', 'Wiskey', 'Kitten', 'kiity is a normal cat', 'www.youtube.com', NULL, 'b5fdd1ce4d6f6e861bc42fc1ca4e7ce9cat1.jpg', 'b5fdd1ce4d6f6e861bc42fc1ca4e7ce9cat1.jpg', 'b5fdd1ce4d6f6e861bc42fc1ca4e7ce9cat2.jpg', 'b5fdd1ce4d6f6e861bc42fc1ca4e7ce9cat2.jpg', '2020-04-29 05:24:09', '2020-04-29 05:24:09'),
(3, 'f5bd7960a9d1071d1f1e0af6dbe617a5', 'Crockie', '202004291346', 'yikes-reptile-crockie', '500', '0.75', 'No', 'No', 'Male', 'green', 'Small', 'Pending', 'No', 'Lizard', 'Wiskey', 'Reptile', 'yikes, reptile is good ...', 'www.youtube.com/zxc', NULL, 'f5bd7960a9d1071d1f1e0af6dbe617a5reptile2.jpg', 'f5bd7960a9d1071d1f1e0af6dbe617a5reptile3.jpg', 'f5bd7960a9d1071d1f1e0af6dbe617a5reptile2.jpg', 'f5bd7960a9d1071d1f1e0af6dbe617a5reptile3.jpg', '2020-04-29 05:47:54', '2020-04-29 05:47:54');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `animal_type` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image_one` varchar(255) NOT NULL,
  `image_two` varchar(255) NOT NULL,
  `image_three` varchar(255) NOT NULL,
  `image_four` varchar(255) NOT NULL,
  `variation_one` varchar(255) NOT NULL,
  `variation_one_price` varchar(255) NOT NULL,
  `variation_one_stock` int(255) NOT NULL,
  `variation_one_image` varchar(255) NOT NULL,
  `variation_two` varchar(255) NOT NULL,
  `variation_two_price` varchar(255) NOT NULL,
  `variation_two_stock` varchar(255) NOT NULL,
  `variation_two_image` varchar(255) NOT NULL,
  `variation_three` varchar(255) NOT NULL,
  `variation_three_price` varchar(255) NOT NULL,
  `variation_three_stock` varchar(255) NOT NULL,
  `variation_three_image` varchar(255) NOT NULL,
  `variation_four` varchar(255) NOT NULL,
  `variation_four_price` varchar(255) NOT NULL,
  `variation_four_stock` varchar(255) NOT NULL,
  `variation_four_image` varchar(255) NOT NULL,
  `variation_five` varchar(255) NOT NULL,
  `variation_five_price` varchar(255) NOT NULL,
  `variation_five_stock` varchar(255) NOT NULL,
  `variation_five_image` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `uid`, `name`, `sku`, `slug`, `animal_type`, `category`, `brand`, `expiry_date`, `status`, `description`, `link`, `image_one`, `image_two`, `image_three`, `image_four`, `variation_one`, `variation_one_price`, `variation_one_stock`, `variation_one_image`, `variation_two`, `variation_two_price`, `variation_two_stock`, `variation_two_image`, `variation_three`, `variation_three_price`, `variation_three_stock`, `variation_three_image`, `variation_four`, `variation_four_price`, `variation_four_stock`, `variation_four_image`, `variation_five`, `variation_five_price`, `variation_five_stock`, `variation_five_image`, `date_created`, `date_updated`) VALUES
(1, '6e82d66f1390da3082e30015f8f1eef0', 'Padigree', '147', 'www.pets.com', 'Puppy', 'Food', 'Brand 3', '2020-04-15', 'Available', 'Food', 'http://links.com', 'pedigree1.jpg', '', '', '', '', '30', 90, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-21 13:28:41', '2020-04-21 13:28:41'),
(2, '9fcd63337d049e2639899551fe813f15', 'whiskas', '12345', 'https://www.computerhope.com', 'Kitten', 'Food', 'Brand 2', '2022-02-01', 'Available', '- Daily oral care treat for your pet.\r\n- Unique tasty X-shaped treat clinically proven to reduce plaque.\r\n- It has active ingredients zinc sulphate & sodium trio polyphosphate that help in slow down the rate of tartar build up.', 'EhhiY11Z9-U', 'product1.jpg', 'product2.jpg', 'product3.jpg', 'product4.jpg', '', '31', 100, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-21 14:18:54', '2020-04-21 14:18:54'),
(3, 'b36dce4d1eb8e7c9dc250c17b6f4702b', 'Bone Toy', '321', 'https://www.computerhope.com', 'Other', 'Accessory', 'Brand 1', '0000-00-00', 'Available', 'Toy', 'http://link.com', 'toy1.jpg', '', '', '', '', '15', 100, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-21 14:27:25', '2020-04-21 14:27:25'),
(4, 'fb6d754eba4857ed504b1742feb01807', 'Comb', '999', 'https://www.pets.com', 'Kitten', 'Grooming', 'Brand 1', '2020-04-04', 'Available', 'Best slicker brush', 'http://link.com', 'comb1.jpg', '', '', '', '', '50', 80, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-23 03:59:10', '2020-04-23 03:59:10'),
(5, '3f1174527baedc46efe9b0bb4f4039f8', 'Donut dog beds', '333', 'https://www.pets.com', 'Puppy', 'Accessory', 'Brand 3', '2020-04-30', 'Available', 'Comfy', 'http://link.com', 'donut1.jpg', '', '', '', '', '250', 50, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-23 04:09:16', '2020-04-23 04:09:16'),
(6, '139b7417b15f91efbbf0e371a9ef7ab9', 'Alpo', '789456123', 'https://www.alpo.com', 'Puppy', 'Food', 'Brand 3', '2021-12-09', 'Available', 'Yummy and most favorite', 'VhtE47ANUA0', 'alpo1.jpg', 'alpo2.png', 'alpo3.jpg', 'alpo4.png', '', '30', 100, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-24 04:22:40', '2020-04-24 04:22:40'),
(7, 'bc8369fb02ea8e8edfe48ac9efc68da4', 'squeeze', '3578951', 'https://www.pets.com', 'Reptile', 'Grooming', 'Brand 2', '2020-11-19', 'Available', 'Toy', 'EhhiY11Z9-U', 'ball1.jpg', '', '', '', '', '41', 100, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-24 05:47:11', '2020-04-24 05:47:11'),
(8, 'ed415a8065fea0e6d4d884239fa3d5c0', 'Sheba', '798456123', 'https://www.sheba.com', 'Kitten', 'Food', 'Brand 3', '2022-06-14', 'Available', 'Food', 'PFjDisXO754', 'sheba1.jpg', 'sheba2.jpg', 'sheba3.jpg', 'sheba4.jpg', '', '16', 50, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-24 05:48:47', '2020-04-24 05:48:47');

-- --------------------------------------------------------

--
-- Table structure for table `puppy`
--

CREATE TABLE `puppy` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `vaccinated` varchar(255) DEFAULT NULL,
  `dewormed` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `feature` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `seller` varchar(255) DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puppy`
--

INSERT INTO `puppy` (`id`, `uid`, `name`, `sku`, `slug`, `price`, `age`, `vaccinated`, `dewormed`, `gender`, `color`, `size`, `status`, `feature`, `breed`, `seller`, `details`, `link`, `location`, `image_one`, `image_two`, `image_three`, `image_four`, `date_created`, `date_updated`) VALUES
(1, 'f6a9b818af7c7f294b9656184ffa223c', 'Rocky', '202004291213', 'blacky-cheap-cute-puppy', '1888', '1.5', 'Yes', 'Yes', 'Male', 'Cream', 'Large', 'Pending', 'Yes', 'Chihuahua', 'Wiskey', 'cheap and cute puppy very cheap', 'www.youtube.com/asd', NULL, 'f6a9b818af7c7f294b9656184ffa223c5.jpg', 'f6a9b818af7c7f294b9656184ffa223c6.jpg', 'f6a9b818af7c7f294b9656184ffa223c5.jpg', 'f6a9b818af7c7f294b9656184ffa223c6.jpg', '2020-04-28 09:17:55', '2020-04-28 09:17:55');

-- --------------------------------------------------------

--
-- Table structure for table `reptile`
--

CREATE TABLE `reptile` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `price` int(255) NOT NULL,
  `age` int(3) NOT NULL,
  `vaccinated` varchar(255) NOT NULL,
  `dewormed` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `size` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `feature` varchar(255) NOT NULL,
  `breed` varchar(255) NOT NULL,
  `seller` varchar(255) NOT NULL,
  `details` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image_one` varchar(255) NOT NULL,
  `image_two` varchar(255) NOT NULL,
  `image_three` varchar(255) NOT NULL,
  `image_four` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reptile`
--

INSERT INTO `reptile` (`id`, `uid`, `name`, `sku`, `slug`, `price`, `age`, `vaccinated`, `dewormed`, `gender`, `color`, `size`, `status`, `feature`, `breed`, `seller`, `details`, `link`, `image_one`, `image_two`, `image_three`, `image_four`, `date_created`, `date_updated`) VALUES
(1, 'f5bd7960a9d1071d1f1e0af6dbe617a5', 'Crockie', '202004291346', 'yikes-reptile-crockie', 500, 1, 'No', 'No', 'Male', 'green', 'Small', 'Pending', 'No', 'Lizard', 'Wiskey', 'yikes, reptile is good ...', 'www.youtube.com/zxc', 'f5bd7960a9d1071d1f1e0af6dbe617a5reptile2.jpg', 'f5bd7960a9d1071d1f1e0af6dbe617a5reptile3.jpg', 'f5bd7960a9d1071d1f1e0af6dbe617a5reptile2.jpg', 'f5bd7960a9d1071d1f1e0af6dbe617a5reptile3.jpg', '2020-04-29 05:47:54', '2020-04-29 05:47:54');

-- --------------------------------------------------------

--
-- Table structure for table `seller`
--

CREATE TABLE `seller` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'random user id',
  `company_name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `registration_no` varchar(255) DEFAULT NULL,
  `contact_no` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `account_status` varchar(255) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `contact_person_no` varchar(255) DEFAULT NULL,
  `experience` varchar(255) DEFAULT NULL,
  `cert` varchar(255) DEFAULT NULL,
  `services` varchar(255) DEFAULT NULL,
  `breed_type` varchar(255) DEFAULT NULL,
  `other_info` varchar(255) DEFAULT NULL,
  `company_logo` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seller`
--

INSERT INTO `seller` (`id`, `uid`, `company_name`, `slug`, `registration_no`, `contact_no`, `address`, `state`, `account_status`, `contact_person`, `contact_person_no`, `experience`, `cert`, `services`, `breed_type`, `other_info`, `company_logo`, `date_created`, `date_updated`) VALUES
(1, '766cbd1be5407a0b08f3524feda42010', 'Padigree', 'www.padigree.com.my', 'AA1123BCC', '010520520', '12, Padigree Building, Jln SS2', 'State', 'Active', 'padigree pic', '012456456', '3', 'many many certs', 'Puppy Sellers, Kitten Sellers, Reptile Sellers', 'lots of breed', 'mengmian', '', '2020-04-28 04:12:33', '2020-04-28 04:12:33'),
(2, '2984dae6a2927c72561afdce6e68ae05', 'Wiskey', 'www.wiskey.com', 'AA121212', '0108528520', '12, Wiskey Area, Jln SS5', 'PJ KL', 'Active', 'Wiskey', '012123321', '3', 'certs', 'Puppy Sellers, Kitten Sellers', 'breeds', 'wiskey', NULL, '2020-04-28 04:41:52', '2020-04-28 04:41:52');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'random user id',
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `tac` varchar(255) DEFAULT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `fb_id` varchar(255) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `account_status` varchar(255) DEFAULT NULL,
  `receiver_name` varchar(255) DEFAULT NULL,
  `receiver_contact_no` varchar(255) DEFAULT NULL,
  `shipping_state` varchar(255) DEFAULT NULL,
  `shipping_area` varchar(255) DEFAULT NULL,
  `shipping_postal_code` varchar(255) DEFAULT NULL,
  `shipping_address` text DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `name_on_card` varchar(255) DEFAULT NULL,
  `card_no` varchar(255) DEFAULT NULL,
  `card_type` varchar(255) DEFAULT NULL,
  `expiry_date` varchar(255) DEFAULT NULL,
  `ccv` int(255) DEFAULT NULL,
  `postal_code` int(255) DEFAULT NULL,
  `billing_address` varchar(255) DEFAULT NULL,
  `email_verification_code` varchar(10) DEFAULT NULL,
  `is_email_verified` tinyint(1) NOT NULL DEFAULT 1,
  `is_phone_verified` tinyint(1) NOT NULL DEFAULT 0,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `name`, `email`, `country`, `phone_no`, `tac`, `password`, `salt`, `user_type`, `fb_id`, `birthday`, `gender`, `account_status`, `receiver_name`, `receiver_contact_no`, `shipping_state`, `shipping_area`, `shipping_postal_code`, `shipping_address`, `bank_name`, `bank_account_holder`, `bank_account_no`, `name_on_card`, `card_no`, `card_type`, `expiry_date`, `ccv`, `postal_code`, `billing_address`, `email_verification_code`, `is_email_verified`, `is_phone_verified`, `date_created`, `date_updated`) VALUES
(1, 'd156938c9b63fd253e9cddf89eba10e7', 'admin', 'admin@gmail.com', 'Malaysia', '0121122330', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-04-28 04:05:16', '2020-04-28 05:52:34'),
(2, '9349aede685bae49c49b0b895e465f6d', 'user', 'user@gmail.com', 'Malaysia', '0124455660', NULL, 'fb1343bf3ac191ede70b6d323ac4a19b10905f49ad1df13338c5b6a40ca33f54', 'e70e7e71ece4a3469028b5a4b2cf316bbb448d70', 1, NULL, NULL, NULL, 'Banned', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-04-28 04:05:55', '2020-04-28 05:56:29'),
(3, 'c2e4a20007e868502ea2857e094af93b', 'user2', 'user2@gmail.com', 'Singapore', '0127788990', NULL, 'fa0a1acac2e0844b2a9996cf8d57d86db163b04f7e93cdd157056992962273e4', '84d6588f07b2dd352d4e413a2dfe53fad13ff245', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-04-28 04:06:21', '2020-04-28 05:40:25'),
(4, '766cbd1be5407a0b08f3524feda42010', 'Padigree', NULL, NULL, '011123123', NULL, '8a237afd4ecec2ede22e3ec7b19a7563713e360654cc7520c4b87dbf5c22ed56', '93e7d0116161e1ad9807f8aaeccf149cd8a27ab3', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-04-28 04:12:33', '2020-04-28 04:41:59'),
(5, '2984dae6a2927c72561afdce6e68ae05', 'Wiskey', NULL, NULL, '0127412369852', NULL, '6e94df6abf1ff37093533c73c5ec84617081a00c3f384bd3614bc4f237da76dc', 'b20e148c5f6a3889975469997107dd95cf0cc9ca', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-04-28 04:41:52', '2020-04-29 09:06:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `breed`
--
ALTER TABLE `breed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_card`
--
ALTER TABLE `credit_card`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kitten`
--
ALTER TABLE `kitten`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pet_details`
--
ALTER TABLE `pet_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `puppy`
--
ALTER TABLE `puppy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reptile`
--
ALTER TABLE `reptile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller`
--
ALTER TABLE `seller`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `breed`
--
ALTER TABLE `breed`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `color`
--
ALTER TABLE `color`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `credit_card`
--
ALTER TABLE `credit_card`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kitten`
--
ALTER TABLE `kitten`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pet_details`
--
ALTER TABLE `pet_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `puppy`
--
ALTER TABLE `puppy`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `reptile`
--
ALTER TABLE `reptile`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `seller`
--
ALTER TABLE `seller`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
