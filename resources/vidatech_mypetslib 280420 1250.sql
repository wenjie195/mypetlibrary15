-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 28, 2020 at 06:49 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_mypetslib`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `uid`, `username`, `bank_name`, `bank_account_holder`, `bank_account_no`, `status`, `date_created`, `date_updated`) VALUES
(1, 'fd1427e01f65783447c12b2e1923caf9', 'user', 'HLB', 'asd asd', '8855228', 'Active', '2020-04-24 04:37:45', '2020-04-27 04:03:13'),
(2, 'fd1427e01f65783447c12b2e1923caf9', 'user', 'man united', 'zxc zxc', '998877', 'Active', '2020-04-24 06:13:13', '2020-04-24 08:20:06'),
(3, 'fd1427e01f65783447c12b2e1923caf9', 'user', 'AMABO', 'wayne tan', '9966330', 'Active', '2020-04-24 08:19:56', '2020-04-24 08:19:56');

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `name`, `status`, `date_created`, `date_updated`) VALUES
(1, 'Brand 1', 'Available', '2020-04-21 10:20:15', '2020-04-21 10:20:15'),
(2, 'Brand 2', 'Available', '2020-04-21 10:22:14', '2020-04-21 10:22:14'),
(3, 'Brand 3', 'Available', '2020-04-21 10:22:50', '2020-04-21 10:22:50');

-- --------------------------------------------------------

--
-- Table structure for table `breed`
--

CREATE TABLE `breed` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `breed`
--

INSERT INTO `breed` (`id`, `name`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'terrier', 'Available', 1, '2020-04-21 06:53:13', '2020-04-21 06:53:13'),
(2, 'terrier', 'Available', 1, '2020-04-21 06:53:43', '2020-04-21 06:53:43'),
(3, 'puddle', 'Available', 1, '2020-04-21 07:01:32', '2020-04-21 07:01:32'),
(4, 'German Shepherd', 'Available', 1, '2020-04-21 08:11:50', '2020-04-21 08:11:50'),
(5, 'Persian', 'Available', 2, '2020-04-21 08:18:14', '2020-04-21 08:18:14'),
(6, 'Bengal', 'Available', 2, '2020-04-21 08:22:58', '2020-04-21 08:22:58'),
(7, 'Ragdoll', 'Available', 2, '2020-04-21 08:51:47', '2020-04-21 08:51:47'),
(8, 'Chihuahua', 'Available', 1, '2020-04-21 08:53:54', '2020-04-21 08:53:54'),
(9, 'Lizard', 'Available', 3, '2020-04-21 09:25:51', '2020-04-21 09:25:51'),
(10, 'Frog', 'Available', 3, '2020-04-21 09:28:28', '2020-04-21 09:28:28');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `status`, `date_created`, `date_updated`) VALUES
(1, 'Grooming', 'Available', '2020-04-21 10:24:39', '2020-04-21 10:24:39'),
(2, 'Accessory', 'Available', '2020-04-21 10:25:19', '2020-04-21 10:25:19'),
(3, 'Food', 'Available', '2020-04-21 10:25:33', '2020-04-21 10:25:33');

-- --------------------------------------------------------

--
-- Table structure for table `color`
--

CREATE TABLE `color` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `color`
--

INSERT INTO `color` (`id`, `name`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'brown', 'Available', 1, '2020-04-21 08:00:50', '2020-04-21 08:00:50'),
(2, 'gold brown', 'Available', 1, '2020-04-21 08:01:55', '2020-04-21 08:01:55'),
(3, 'black', 'Available', 3, '2020-04-21 08:05:41', '2020-04-21 08:05:41'),
(4, 'grey', 'Available', 2, '2020-04-21 08:05:51', '2020-04-21 08:05:51'),
(5, 'white', 'Available', 2, '2020-04-21 09:02:02', '2020-04-21 09:02:02'),
(6, 'Cream', 'Available', 1, '2020-04-21 09:13:20', '2020-04-21 09:13:20'),
(7, 'Orange', 'Available', 2, '2020-04-21 09:15:13', '2020-04-21 09:15:13'),
(8, 'Red', 'Available', 2, '2020-04-21 09:19:46', '2020-04-21 09:19:46'),
(9, 'green', 'Available', 3, '2020-04-21 09:32:48', '2020-04-21 09:32:48');

-- --------------------------------------------------------

--
-- Table structure for table `credit_card`
--

CREATE TABLE `credit_card` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `name_on_card` varchar(255) DEFAULT NULL,
  `card_no` int(255) DEFAULT NULL,
  `card_type` varchar(255) DEFAULT NULL,
  `ccv` int(255) DEFAULT NULL,
  `expiry_date` varchar(255) DEFAULT NULL,
  `billing_address` varchar(255) DEFAULT NULL,
  `postal_code` int(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `credit_card`
--

INSERT INTO `credit_card` (`id`, `uid`, `username`, `name_on_card`, `card_no`, `card_type`, `ccv`, `expiry_date`, `billing_address`, `postal_code`, `status`, `date_created`, `date_updated`) VALUES
(1, 'fd1427e01f65783447c12b2e1923caf9', 'user', 'barry allen', 4455660, 'Debit', 456, '2020-04-30', '234,asd, zxc, qwe', 445566, 'Active', '2020-04-24 05:55:49', '2020-04-27 04:03:19'),
(2, 'fd1427e01f65783447c12b2e1923caf9', 'user', 'clark kent', 55665566, 'Debit', 774, '2020-04-04', 'fgh,sad', 789789, 'Active', '2020-04-24 08:18:59', '2020-04-24 08:18:59');

-- --------------------------------------------------------

--
-- Table structure for table `kitten`
--

CREATE TABLE `kitten` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `price` int(255) DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `vaccinated` varchar(255) DEFAULT NULL,
  `dewormed` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `feature` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `seller` varchar(255) DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kitten`
--

INSERT INTO `kitten` (`id`, `uid`, `name`, `sku`, `slug`, `price`, `age`, `vaccinated`, `dewormed`, `gender`, `color`, `size`, `status`, `feature`, `breed`, `seller`, `details`, `link`, `location`, `image_one`, `image_two`, `image_three`, `image_four`, `date_created`, `date_updated`) VALUES
(1, '4c2b67c8b0b1522554bee26431b28ab1', 'Luziee', '123456', 'https://www.pets.com', 1000, 1, 'Yes', 'No', 'Female', 'white', 'Small', 'Available', 'Yes', 'Bengal', 'Fur', 'Cute', 'http://link.com', 'Penang', 'f.jpg', 'f.jpg', 'f.jpg', 'f.jpg', '2020-04-21 16:31:57', '2020-04-21 16:31:57'),
(2, '19c30cd4dd8bfa39934bef1789c792e1', 'Minney', '123', 'https://www.pets.com', 1500, 1, 'Yes', 'Yes', 'Male', 'grey', 'Small', 'Available', 'Yes', 'Bengal', 'Paw', 'Cute', 'http://link.com', 'Penang', 'cat2.jpg', 'cat2.jpg', 'cat2.jpg', 'cat2.jpg', '2020-04-21 16:33:04', '2020-04-21 16:33:04'),
(3, 'acac2e9be1413729130feab3f671695d', 'Rain', '741', 'https://www.pets.com', 1000, 1, 'No', 'No', 'Male', 'Red', 'Small', 'Available', 'Yes', 'Ragdoll', 'Paw', 'Cute', 'http://link.com', 'Penang', 'cat3.jpg', 'cat3.jpg', 'cat3.jpg', 'cat3.jpg', '2020-04-21 16:45:14', '2020-04-21 16:45:14'),
(4, '36cc514a91e34b21bda197b78776c33a', 'Rossie', '951', 'https://www.pets.com', 1500, 2, 'No', 'No', 'Female', 'Orange', 'Middle', 'Available', 'No', 'Persian', 'Paw', 'smart', 'http://link.com', 'Kedah', 'f.jpg', 'f.jpg', 'f.jpg', 'f.jpg', '2020-04-22 05:12:06', '2020-04-22 05:12:06'),
(5, 'dbb07ccc3c437ff7ed6973d09d35a48d', 'Ruby', '741', 'https://www.kitten.com', 1599, 1, 'Yes', 'Yes', 'Female', 'Orange', 'Big', 'Available', 'Yes', 'Persian', 'Kitto', 'active', 'http://links.com', 'Kedah', 'e.jpg', 'e.jpg', 'e.jpg', 'e.jpg', '2020-04-22 05:21:58', '2020-04-22 05:21:58'),
(6, 'a0ad9c988a8924f50ced7ec0dd139da2', 'Resh', '74123', 'https://www.pets.com', 1000, 3, 'No', 'No', 'Female', 'Red', 'Big', 'Available', 'Yes', 'Ragdoll', 'Kitto', 'Big and Fluffy', 'http://link.com', 'Kedah', 'f.jpg', 'f.jpg', 'f.jpg', 'f.jpg', '2020-04-22 12:09:50', '2020-04-22 12:09:50');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `price` int(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `animal_type` varchar(255) NOT NULL,
  `stock` int(255) NOT NULL,
  `expiry_date` date NOT NULL,
  `status` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image_one` varchar(255) NOT NULL,
  `image_two` varchar(255) NOT NULL,
  `image_three` varchar(255) NOT NULL,
  `image_four` varchar(255) NOT NULL,
  `image_five` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `uid`, `name`, `sku`, `slug`, `price`, `category`, `brand`, `animal_type`, `stock`, `expiry_date`, `status`, `description`, `link`, `image_one`, `image_two`, `image_three`, `image_four`, `image_five`, `date_created`, `date_updated`) VALUES
(1, '6e82d66f1390da3082e30015f8f1eef0', 'Padigree', '147', 'www.pets.com', 30, 'Food', 'Brand 3', 'Puppy', 90, '2020-04-15', 'Available', 'Food', 'http://links.com', 'pedigree1.jpg', '', '', '', '', '2020-04-21 13:28:41', '2020-04-21 13:28:41'),
(2, '9fcd63337d049e2639899551fe813f15', 'whiskas', '12345', 'https://www.computerhope.com', 28, 'Food', 'Brand 2', 'Kitten', 100, '2022-02-01', 'Available', 'Food', 'EhhiY11Z9-U', 'product1.jpg', 'product2.jpg', 'product3.jpg', 'product4.jpg', 'product5.jpg', '2020-04-21 14:18:54', '2020-04-21 14:18:54'),
(3, 'b36dce4d1eb8e7c9dc250c17b6f4702b', 'Bone Toy', '321', 'https://www.computerhope.com', 28, 'Accessory', 'Brand 1', 'Other', 100, '0000-00-00', 'Available', 'Toy', 'http://link.com', 'toy1.jpg', '', '', '', '', '2020-04-21 14:27:25', '2020-04-21 14:27:25'),
(4, 'fb6d754eba4857ed504b1742feb01807', 'Comb', '999', 'https://www.pets.com', 50, 'Grooming', 'Brand 1', 'Kitten', 80, '2020-04-04', 'Available', 'Best slicker brush', 'http://link.com', 'comb1.jpg', '', '', '', '', '2020-04-23 03:59:10', '2020-04-23 03:59:10'),
(5, '3f1174527baedc46efe9b0bb4f4039f8', 'Donut dog beds', '333', 'https://www.pets.com', 250, 'Accessory', 'Brand 3', 'Puppy', 50, '2020-04-30', 'Available', 'Comfy', 'http://link.com', 'donut1.jpg', '', '', '', '', '2020-04-23 04:09:16', '2020-04-23 04:09:16'),
(6, '139b7417b15f91efbbf0e371a9ef7ab9', 'Alpo', '789456123', 'https://www.alpo.com', 30, 'Food', 'Brand 3', 'Puppy', 100, '2021-12-09', 'Available', 'Yummy and most favorite', 'VhtE47ANUA0', 'alpo1.jpg', 'alpo2.png', 'alpo3.jpg', 'alpo4.png', 'alpo5.jpg', '2020-04-24 04:22:40', '2020-04-24 04:22:40'),
(7, 'bc8369fb02ea8e8edfe48ac9efc68da4', 'squeeze', '3578951', 'https://www.pets.com', 41, 'Grooming', 'Brand 2', 'Reptile', 100, '2020-11-19', 'Available', 'Toy', 'EhhiY11Z9-U', 'ball1.jpg', '', '', '', '', '2020-04-24 05:47:11', '2020-04-24 05:47:11'),
(8, 'ed415a8065fea0e6d4d884239fa3d5c0', 'Sheba', '798456123', 'https://www.sheba.com', 16, 'Food', 'Brand 3', 'Kitten', 50, '2022-06-14', 'Available', 'Food', 'PFjDisXO754', 'sheba1.jpg', 'sheba2.jpg', 'sheba3.jpg', 'sheba4.jpg', 'sheba5.jpg', '2020-04-24 05:48:47', '2020-04-24 05:48:47');

-- --------------------------------------------------------

--
-- Table structure for table `puppy`
--

CREATE TABLE `puppy` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `price` int(255) DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `vaccinated` varchar(255) DEFAULT NULL,
  `dewormed` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `feature` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `seller` varchar(255) DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puppy`
--

INSERT INTO `puppy` (`id`, `uid`, `name`, `sku`, `slug`, `price`, `age`, `vaccinated`, `dewormed`, `gender`, `color`, `size`, `status`, `feature`, `breed`, `seller`, `details`, `link`, `location`, `image_one`, `image_two`, `image_three`, `image_four`, `date_created`, `date_updated`) VALUES
(1, '4c2b67c8b0b1522554bee26431b28ab1', 'Shadow', '963', 'https://www.puppies.com', 1500, 1, 'Yes', 'Yes', 'Female', 'gold brown', 'Small', 'Available', 'Yes', 'German Shepherd', 'Fur', 'Cuties', 'http://links.com', 'Johor', '5.jpg', '5.jpg', '5.jpg', '5.jpg', '2020-04-21 16:31:57', '2020-04-21 16:31:57'),
(2, '19c30cd4dd8bfa39934bef1789c792e1', 'Daisy', '123', 'https://www.pets.com', 1500, 1, 'Yes', 'Yes', 'Male', '', 'Middle', 'Available', 'Yes', '', '', 'Cute', 'http://link.com', 'Johor', '5.jpg', '5.jpg', '5.jpg', '5.jpg', '2020-04-21 16:33:04', '2020-04-21 16:33:04'),
(3, 'acac2e9be1413729130feab3f671695d', 'Snow', '741', 'https://www.pets.com', 1000, 1, 'No', 'No', 'Male', 'Cream', 'Small', 'Available', 'No', 'puddle', 'Paw', 'Cute', 'http://link.com', 'Johor', '6.jpg', '6.jpg', '6.jpg', '6.jpg', '2020-04-21 16:45:14', '2020-04-21 16:45:14'),
(5, '38c2972b8f92508905caaa03fde02fb4', 'Lucky', '123456', 'https://www.pets.com', 1000, 1, 'Yes', 'Yes', 'Male', 'gold brown', 'Small', 'Available', 'Yes', 'Chihuahua', 'Padigree', 'Cute', 'http://link.com', 'Johor', '6.jpg', '6.jpg', '6.jpg', '6.jpg', '2020-04-23 07:45:28', '2020-04-23 07:45:28'),
(6, '45097b1b774e067f4f85902b730166aa', 'Lucifer', '852', 'https://www.pets.com', 1700, 1, 'Yes', 'Yes', 'Male', 'Cream', 'Small', 'Available', 'Yes', 'Chihuahua', 'Padigree', 'active', 'http://link.com', 'KL', '5.jpg', '5.jpg', '5.jpg', '5.jpg', '2020-04-23 07:47:18', '2020-04-23 07:47:18'),
(7, '79bdbb4cfc660dae9dbe2832c4e065e6', 'Lime', '123456', 'https://www.pets.com', 1000, 1, 'Yes', 'Yes', 'Male', 'brown', 'Small', 'Available', 'Yes', 'terrier', 'Padigree', 'Cute', 'http://link.com', 'KL', '6.jpg', '6.jpg', '6.jpg', '6.jpg', '2020-04-23 07:55:08', '2020-04-23 07:55:08'),
(8, 'cada8db8544a96f0f37e40b7613f390d', 'Rocky', '741', 'https://www.pets.com', 1000, 1, 'Yes', 'Yes', 'Male', 'gold brown', 'Small', 'Available', 'Yes', 'Chihuahua', 'Fur', 'Cute', 'http://link.com', 'KL', '6.jpg', '6.jpg', '6.jpg', '6.jpg', '2020-04-23 08:00:25', '2020-04-23 08:00:25');

-- --------------------------------------------------------

--
-- Table structure for table `reptile`
--

CREATE TABLE `reptile` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `price` int(255) NOT NULL,
  `age` int(3) NOT NULL,
  `vaccinated` varchar(255) NOT NULL,
  `dewormed` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `size` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `feature` varchar(255) NOT NULL,
  `breed` varchar(255) NOT NULL,
  `seller` varchar(255) NOT NULL,
  `details` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image_one` varchar(255) NOT NULL,
  `image_two` varchar(255) NOT NULL,
  `image_three` varchar(255) NOT NULL,
  `image_four` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reptile`
--

INSERT INTO `reptile` (`id`, `uid`, `name`, `sku`, `slug`, `price`, `age`, `vaccinated`, `dewormed`, `gender`, `color`, `size`, `status`, `feature`, `breed`, `seller`, `details`, `link`, `image_one`, `image_two`, `image_three`, `image_four`, `date_created`, `date_updated`) VALUES
(1, '776755db989ec09fd6e929c42e371b41', 'crockie', '741', 'https://www.pets.com', 1700, 1, 'No', 'No', 'Male', 'black', 'Middle', 'Available', 'No', 'Lizard', 'Pets lib', 'long and slimy', 'http://link.com', '', '', '', '', '2020-04-22 05:33:22', '2020-04-22 05:33:22'),
(2, '8df91c170e8eb13aa8a3146564ea1fc8', 'frogie', '123456', 'https://www.pets.com', 200, 2, 'Yes', 'Yes', 'Female', 'green', 'Small', 'Available', 'Yes', 'Frog', 'Rackoon', 'slimy', 'http://link.com', '', '', '', '', '2020-04-22 05:37:16', '2020-04-22 05:37:16'),
(3, 'f13dcad221755536fdd87088bb631d1f', 'Lizz', '741', 'https://www.pets.com', 149, 1, 'No', 'No', 'Female', 'black', 'Middle', 'Available', 'Yes', 'Lizard', 'Rackoon', 'Active, move faster :)', 'http://links.com', '', '', '', '', '2020-04-22 12:24:16', '2020-04-22 12:24:16');

-- --------------------------------------------------------

--
-- Table structure for table `seller`
--

CREATE TABLE `seller` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'random user id',
  `company_name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `registration_no` varchar(255) DEFAULT NULL,
  `contact_no` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `account_status` varchar(255) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `contact_person_no` varchar(255) DEFAULT NULL,
  `experience` varchar(255) DEFAULT NULL,
  `cert` varchar(255) DEFAULT NULL,
  `services` varchar(255) DEFAULT NULL,
  `breed_type` varchar(255) DEFAULT NULL,
  `other_info` varchar(255) DEFAULT NULL,
  `company_logo` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seller`
--

INSERT INTO `seller` (`id`, `uid`, `company_name`, `slug`, `registration_no`, `contact_no`, `address`, `state`, `account_status`, `contact_person`, `contact_person_no`, `experience`, `cert`, `services`, `breed_type`, `other_info`, `company_logo`, `date_created`, `date_updated`) VALUES
(1, '766cbd1be5407a0b08f3524feda42010', 'Padigree', 'www.padigree.com.my', 'AA1123BCC', '010520520', '12, Padigree Building, Jln SS2', 'State', 'Active', 'padigree pic', '012456456', '3', 'many many certs', 'Puppy Sellers, Kitten Sellers, Reptile Sellers', 'lots of breed', 'mengmian', '', '2020-04-28 04:12:33', '2020-04-28 04:12:33'),
(2, '2984dae6a2927c72561afdce6e68ae05', 'Wiskey', 'www.wiskey.com', 'AA121212', '0108528520', '12, Wiskey Area, Jln SS5', 'PJ', 'Active', 'Wiskey PIC', '0127412369852', '2', 'certs', 'Puppy Sellers, Kitten Sellers', 'breeds', 'wiskey', NULL, '2020-04-28 04:41:52', '2020-04-28 04:41:52');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'random user id',
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `tac` varchar(255) DEFAULT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `fb_id` varchar(255) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `account_status` varchar(255) DEFAULT NULL,
  `receiver_name` varchar(255) DEFAULT NULL,
  `receiver_contact_no` varchar(255) DEFAULT NULL,
  `shipping_state` varchar(255) DEFAULT NULL,
  `shipping_area` varchar(255) DEFAULT NULL,
  `shipping_postal_code` varchar(255) DEFAULT NULL,
  `shipping_address` text DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `name_on_card` varchar(255) DEFAULT NULL,
  `card_no` varchar(255) DEFAULT NULL,
  `card_type` varchar(255) DEFAULT NULL,
  `expiry_date` varchar(255) DEFAULT NULL,
  `ccv` int(255) DEFAULT NULL,
  `postal_code` int(255) DEFAULT NULL,
  `billing_address` varchar(255) DEFAULT NULL,
  `email_verification_code` varchar(10) DEFAULT NULL,
  `is_email_verified` tinyint(1) NOT NULL DEFAULT 1,
  `is_phone_verified` tinyint(1) NOT NULL DEFAULT 0,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `name`, `email`, `country`, `phone_no`, `tac`, `password`, `salt`, `user_type`, `fb_id`, `birthday`, `gender`, `account_status`, `receiver_name`, `receiver_contact_no`, `shipping_state`, `shipping_area`, `shipping_postal_code`, `shipping_address`, `bank_name`, `bank_account_holder`, `bank_account_no`, `name_on_card`, `card_no`, `card_type`, `expiry_date`, `ccv`, `postal_code`, `billing_address`, `email_verification_code`, `is_email_verified`, `is_phone_verified`, `date_created`, `date_updated`) VALUES
(1, 'd156938c9b63fd253e9cddf89eba10e7', 'admin', 'admin@gmail.com', 'Malaysia', '0121122330', NULL, '9edacac8911ad14fb82745d6b1b2e164de9e26dc276fe6d7679d776dd4a083cc', '0d4e895329f44152f91eca02c0b274923f86d5b8', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-04-28 04:05:16', '2020-04-28 04:05:27'),
(2, '9349aede685bae49c49b0b895e465f6d', 'user', 'user@gmail.com', 'Malaysia', '0124455660', NULL, 'fb1343bf3ac191ede70b6d323ac4a19b10905f49ad1df13338c5b6a40ca33f54', 'e70e7e71ece4a3469028b5a4b2cf316bbb448d70', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-04-28 04:05:55', '2020-04-28 04:05:55'),
(3, 'c2e4a20007e868502ea2857e094af93b', 'user2', 'user2@gmail.com', 'Singapore', '0127788990', NULL, 'fa0a1acac2e0844b2a9996cf8d57d86db163b04f7e93cdd157056992962273e4', '84d6588f07b2dd352d4e413a2dfe53fad13ff245', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-04-28 04:06:21', '2020-04-28 04:06:21'),
(4, '766cbd1be5407a0b08f3524feda42010', 'Padigree', NULL, NULL, '011123123', NULL, '8a237afd4ecec2ede22e3ec7b19a7563713e360654cc7520c4b87dbf5c22ed56', '93e7d0116161e1ad9807f8aaeccf149cd8a27ab3', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-04-28 04:12:33', '2020-04-28 04:41:59'),
(5, '2984dae6a2927c72561afdce6e68ae05', 'Wiskey', NULL, NULL, '0127412369852', NULL, '6e94df6abf1ff37093533c73c5ec84617081a00c3f384bd3614bc4f237da76dc', 'b20e148c5f6a3889975469997107dd95cf0cc9ca', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-04-28 04:41:52', '2020-04-28 04:41:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `breed`
--
ALTER TABLE `breed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_card`
--
ALTER TABLE `credit_card`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kitten`
--
ALTER TABLE `kitten`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `puppy`
--
ALTER TABLE `puppy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reptile`
--
ALTER TABLE `reptile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller`
--
ALTER TABLE `seller`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `breed`
--
ALTER TABLE `breed`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `color`
--
ALTER TABLE `color`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `credit_card`
--
ALTER TABLE `credit_card`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kitten`
--
ALTER TABLE `kitten`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `puppy`
--
ALTER TABLE `puppy`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `reptile`
--
ALTER TABLE `reptile`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `seller`
--
ALTER TABLE `seller`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
