-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2020 at 11:34 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_mypetslib`
--

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `animal_type` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image_one` varchar(255) NOT NULL,
  `image_two` varchar(255) NOT NULL,
  `image_three` varchar(255) NOT NULL,
  `image_four` varchar(255) NOT NULL,
  `variation_one` varchar(255) NOT NULL,
  `variation_one_price` varchar(255) NOT NULL,
  `variation_one_stock` int(255) NOT NULL,
  `variation_one_image` varchar(255) NOT NULL,
  `variation_two` varchar(255) NOT NULL,
  `variation_two_price` varchar(255) NOT NULL,
  `variation_two_stock` varchar(255) NOT NULL,
  `variation_two_image` varchar(255) NOT NULL,
  `variation_three` varchar(255) NOT NULL,
  `variation_three_price` varchar(255) NOT NULL,
  `variation_three_stock` varchar(255) NOT NULL,
  `variation_three_image` varchar(255) NOT NULL,
  `variation_four` varchar(255) NOT NULL,
  `variation_four_price` varchar(255) NOT NULL,
  `variation_four_stock` varchar(255) NOT NULL,
  `variation_four_image` varchar(255) NOT NULL,
  `variation_five` varchar(255) NOT NULL,
  `variation_five_price` varchar(255) NOT NULL,
  `variation_five_stock` varchar(255) NOT NULL,
  `variation_five_image` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `uid`, `name`, `sku`, `slug`, `animal_type`, `category`, `brand`, `expiry_date`, `status`, `description`, `link`, `image_one`, `image_two`, `image_three`, `image_four`, `variation_one`, `variation_one_price`, `variation_one_stock`, `variation_one_image`, `variation_two`, `variation_two_price`, `variation_two_stock`, `variation_two_image`, `variation_three`, `variation_three_price`, `variation_three_stock`, `variation_three_image`, `variation_four`, `variation_four_price`, `variation_four_stock`, `variation_four_image`, `variation_five`, `variation_five_price`, `variation_five_stock`, `variation_five_image`, `date_created`, `date_updated`) VALUES
(1, '6e82d66f1390da3082e30015f8f1eef0', 'Padigree', '147', 'www.pets.com', 'Puppy', 'Food', 'Brand 3', '2020-04-15', 'Available', 'Food', 'http://links.com', 'pedigree1.jpg', '', '', '', '', '30', 90, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-21 13:28:41', '2020-04-21 13:28:41'),
(2, '9fcd63337d049e2639899551fe813f15', 'whiskas', '12345', 'https://www.computerhope.com', 'Kitten', 'Food', 'Brand 2', '2022-02-01', 'Available', '- Daily oral care treat for your pet.\r\n- Unique tasty X-shaped treat clinically proven to reduce plaque.\r\n- It has active ingredients zinc sulphate & sodium trio polyphosphate that help in slow down the rate of tartar build up.', 'EhhiY11Z9-U', 'product1.jpg', 'product2.jpg', 'product3.jpg', 'product4.jpg', '', '31', 100, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-21 14:18:54', '2020-04-21 14:18:54'),
(3, 'b36dce4d1eb8e7c9dc250c17b6f4702b', 'Bone Toy', '321', 'https://www.computerhope.com', 'Other', 'Accessory', 'Brand 1', '0000-00-00', 'Available', 'Toy', 'http://link.com', 'toy1.jpg', '', '', '', '', '15', 100, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-21 14:27:25', '2020-04-21 14:27:25'),
(4, 'fb6d754eba4857ed504b1742feb01807', 'Comb', '999', 'https://www.pets.com', 'Kitten', 'Grooming', 'Brand 1', '2020-04-04', 'Available', 'Best slicker brush', 'http://link.com', 'comb1.jpg', '', '', '', '', '50', 80, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-23 03:59:10', '2020-04-23 03:59:10'),
(5, '3f1174527baedc46efe9b0bb4f4039f8', 'Donut dog beds', '333', 'https://www.pets.com', 'Puppy', 'Accessory', 'Brand 3', '2020-04-30', 'Available', 'Comfy', 'http://link.com', 'donut1.jpg', '', '', '', '', '250', 50, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-23 04:09:16', '2020-04-23 04:09:16'),
(6, '139b7417b15f91efbbf0e371a9ef7ab9', 'Alpo', '789456123', 'https://www.alpo.com', 'Puppy', 'Food', 'Brand 3', '2021-12-09', 'Available', 'Yummy and most favorite', 'VhtE47ANUA0', 'alpo1.jpg', 'alpo2.png', 'alpo3.jpg', 'alpo4.png', '', '30', 100, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-24 04:22:40', '2020-04-24 04:22:40'),
(7, 'bc8369fb02ea8e8edfe48ac9efc68da4', 'squeeze', '3578951', 'https://www.pets.com', 'Reptile', 'Grooming', 'Brand 2', '2020-11-19', 'Available', 'Toy', 'EhhiY11Z9-U', 'ball1.jpg', '', '', '', '', '41', 100, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-24 05:47:11', '2020-04-24 05:47:11'),
(8, 'ed415a8065fea0e6d4d884239fa3d5c0', 'Sheba', '798456123', 'https://www.sheba.com', 'Kitten', 'Food', 'Brand 3', '2022-06-14', 'Available', 'Food', 'PFjDisXO754', 'sheba1.jpg', 'sheba2.jpg', 'sheba3.jpg', 'sheba4.jpg', '', '16', 50, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-24 05:48:47', '2020-04-24 05:48:47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
