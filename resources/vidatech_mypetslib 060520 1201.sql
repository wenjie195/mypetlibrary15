-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 06, 2020 at 06:01 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_mypetslib`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `author_uid` varchar(255) DEFAULT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `article_link` varchar(255) DEFAULT NULL,
  `keyword_one` varchar(255) DEFAULT NULL,
  `keyword_two` varchar(255) DEFAULT NULL,
  `title_cover` varchar(255) DEFAULT NULL,
  `paragraph_one` text DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `paragraph_two` text DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `paragraph_three` text DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `paragraph_four` text DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `paragraph_five` text DEFAULT NULL,
  `image_five` varchar(255) DEFAULT NULL,
  `img_cover_source` text DEFAULT NULL,
  `img_one_source` text DEFAULT NULL,
  `img_two_source` text DEFAULT NULL,
  `img_three_source` text DEFAULT NULL,
  `img_four_source` text DEFAULT NULL,
  `img_five_source` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `display` varchar(255) DEFAULT 'YES',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `uid`, `author_uid`, `author_name`, `title`, `seo_title`, `article_link`, `keyword_one`, `keyword_two`, `title_cover`, `paragraph_one`, `image_one`, `paragraph_two`, `image_two`, `paragraph_three`, `image_three`, `paragraph_four`, `image_four`, `paragraph_five`, `image_five`, `img_cover_source`, `img_one_source`, `img_two_source`, `img_three_source`, `img_four_source`, `img_five_source`, `author`, `type`, `display`, `date_created`, `date_updated`) VALUES
(1, '2c6ebd5d95db82529f4f935fe56d8175', 'd156938c9b63fd253e9cddf89eba10e7', 'admin', 'Admin New Article 1', 'AdminNewArticle1-admin-new-article-one', 'admin-new-article-one', 'this is a test article 1', 'admin,new,article,one', '2c6ebd5d95db82529f4f935fe56d8175image-01.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.</p>\r\n\r\n<p><img alt=\"Blog Title\" src=\"https://vidatechft.com/mypetslibrary/img/bg.jpg\" title=\"Blog Title\" /></p>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tevy 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', '2020-05-05 08:28:13', '2020-05-05 08:28:13'),
(2, '2679d629965619151a3d35c3526a2118', 'd156938c9b63fd253e9cddf89eba10e7', 'admin', 'Admin New Article 2', 'AdminNewArticle2-admin-new-article-two', 'admin-new-article-two', 'this is a test article 2', 'admin,new,article,two', '2679d629965619151a3d35c3526a2118image-02.jpg', '<p style=\"text-align:justify\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.</p>\r\n\r\n<p><img alt=\"Blog Title\" src=\"https://vidatechft.com/mypetslibrary/img/bg.jpg\" title=\"Blog Title\" /></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.</p>\r\n\r\n<p><img alt=\"Blog Title\" src=\"https://vidatechft.com/mypetslibrary/img/bg.jpg\" title=\"Blog Title\" /></p>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tevy 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', '2020-05-05 08:34:15', '2020-05-05 08:34:15'),
(3, '7bc81125b91e8d24ecbea29c745166e2', '2984dae6a2927c72561afdce6e68ae05', 'Wiskey', 'Seller New Article 1', 'SellerNewArticle1-seller-new-article-one', 'seller-new-article-one', 'this is seller test article one', 'seller,new,article,one', '7bc81125b91e8d24ecbea29c745166e2facebook-preview.jpg', '<p style=\"text-align:justify\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.</p>\r\n\r\n<p style=\"text-align:right\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.</p>\r\n\r\n<p style=\"text-align:center\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"Blog Title\" src=\"https://vidatechft.com/mypetslibrary/img/bg.jpg\" /></p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"Blog Title\" src=\"https://vidatechft.com/mypetslibrary/img/bg.jpg\" /></p>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tevy 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', '2020-05-05 08:39:15', '2020-05-05 08:43:46'),
(4, '8131f620e5e449e85e8efdf4390e699c', '2984dae6a2927c72561afdce6e68ae05', 'Wiskey', 'Seller New Article 2', 'SellerNewArticle2-seller-new-article-two', 'seller-new-article-two', 'this is seller test article four', 'seller,new,article,two', '15886682754.jpg', '<p style=\"text-align:justify\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.</p>\r\n\r\n<p style=\"text-align:justify\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.</p>\r\n\r\n<p style=\"text-align:justify\"> </p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"Blog Title\" src=\"https://vidatechft.com/mypetslibrary/img/bg.jpg\" /></p>\r\n\r\n<p style=\"text-align:center\"> </p>\r\n\r\n<p style=\"text-align:center\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.</p>\r\n\r\n<p style=\"text-align:center\"> </p>\r\n\r\n<p style=\"text-align:center\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.</p>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tevy 4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Pending', '2020-05-05 08:41:23', '2020-05-05 08:44:35'),
(5, '847688279c6a9daf2fe61cbcfdd282a2', '2984dae6a2927c72561afdce6e68ae05', 'Wiskey', 'Seller New Article 3', 'SellerNewArticle3-seller-new-article-three', 'seller-new-article-three', 'this is seller test article five', 'seller,new,article,three', '847688279c6a9daf2fe61cbcfdd282a2image-04.png', '<p style=\"text-align:justify\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.</p>\r\n\r\n<p style=\"text-align:justify\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.</p>\r\n\r\n<p style=\"text-align:justify\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"Blog Title\" src=\"https://vidatechft.com/mypetslibrary/img/bg.jpg\" /></p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.</p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sit amet sagittis est. Quisque sed neque consequat, faucibus enim et, faucibus nisl. In volutpat, massa quis pretium lacinia, mi tellus auctor sapien, et sodales massa ipsum sit amet ex. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce commodo nisl et metus accumsan, blandit ultrices libero vestibulum. Phasellus interdum ante et urna consectetur posuere vitae ac ligula. Ut sem odio, iaculis vitae enim at, varius accumsan ligula. Fusce facilisis elementum diam, in viverra elit feugiat consectetur. Nulla non leo aliquam, tempus metus sed, ornare tortor. Ut mollis sollicitudin risus, non fringilla eros rutrum non. In vitae sem sodales, dictum orci nec, rhoncus mauris.</p>\r\n\r\n<p style=\"text-align:center\">&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><img alt=\"Blog Title\" src=\"https://vidatechft.com/mypetslibrary/img/bg.jpg\" /></p>\r\n', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tevy 5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Yes', '2020-05-05 08:43:16', '2020-05-05 08:43:35');

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `uid`, `username`, `bank_name`, `bank_account_holder`, `bank_account_no`, `status`, `date_created`, `date_updated`) VALUES
(1, '9349aede685bae49c49b0b895e465f6d', 'user', 'asd asd', '123 456', 'zxc asd', 'Active', '2020-05-06 01:51:37', '2020-05-06 01:53:29'),
(2, '9349aede685bae49c49b0b895e465f6d', 'user', 'qq', '11', 'ww', 'Active', '2020-05-06 01:53:17', '2020-05-06 01:53:17');

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `name`, `status`, `date_created`, `date_updated`) VALUES
(1, 'Brand 1', 'Available', '2020-04-21 10:20:15', '2020-04-21 10:20:15'),
(2, 'Brand 2', 'Available', '2020-04-21 10:22:14', '2020-04-21 10:22:14'),
(3, 'Brand 3', 'Available', '2020-04-21 10:22:50', '2020-04-21 10:22:50');

-- --------------------------------------------------------

--
-- Table structure for table `breed`
--

CREATE TABLE `breed` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `breed`
--

INSERT INTO `breed` (`id`, `name`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'Terrier', 'Available', 1, '2020-04-21 06:53:13', '2020-04-21 06:53:13'),
(2, 'Mix Brand', 'Available', 1, '2020-04-21 06:53:43', '2020-04-21 06:53:43'),
(3, 'Puddle', 'Available', 1, '2020-04-21 07:01:32', '2020-04-21 07:01:32'),
(4, 'German Shepherd', 'Available', 1, '2020-04-21 08:11:50', '2020-04-21 08:11:50'),
(5, 'Persian', 'Available', 2, '2020-04-21 08:18:14', '2020-04-21 08:18:14'),
(6, 'Bengal', 'Available', 2, '2020-04-21 08:22:58', '2020-04-21 08:22:58'),
(7, 'Ragdoll', 'Available', 2, '2020-04-21 08:51:47', '2020-04-21 08:51:47'),
(8, 'Chihuahua', 'Available', 1, '2020-04-21 08:53:54', '2020-04-21 08:53:54'),
(9, 'Lizard', 'Available', 3, '2020-04-21 09:25:51', '2020-04-21 09:25:51'),
(10, 'Frog', 'Available', 3, '2020-04-21 09:28:28', '2020-04-21 09:28:28');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `status`, `date_created`, `date_updated`) VALUES
(1, 'Grooming', 'Available', '2020-04-21 10:24:39', '2020-04-21 10:24:39'),
(2, 'Accessory', 'Available', '2020-04-21 10:25:19', '2020-04-21 10:25:19'),
(3, 'Food', 'Available', '2020-04-21 10:25:33', '2020-04-21 10:25:33');

-- --------------------------------------------------------

--
-- Table structure for table `color`
--

CREATE TABLE `color` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `type` int(5) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `color`
--

INSERT INTO `color` (`id`, `name`, `status`, `type`, `date_created`, `date_updated`) VALUES
(1, 'Black', 'Available', 1, '2020-04-21 08:00:50', '2020-04-21 08:00:50'),
(2, 'Brown', 'Available', 1, '2020-04-21 08:01:55', '2020-04-21 08:01:55'),
(3, 'Black', 'Available', 3, '2020-04-21 08:05:41', '2020-04-21 08:05:41'),
(4, 'Grey', 'Available', 2, '2020-04-21 08:05:51', '2020-04-21 08:05:51'),
(5, 'White', 'Available', 2, '2020-04-21 09:02:02', '2020-04-21 09:02:02'),
(6, 'White', 'Available', 1, '2020-04-21 09:13:20', '2020-04-21 09:13:20'),
(7, 'Orange', 'Available', 2, '2020-04-21 09:15:13', '2020-04-21 09:15:13'),
(8, 'Red', 'Available', 2, '2020-04-21 09:19:46', '2020-04-21 09:19:46'),
(9, 'Green', 'Available', 3, '2020-04-21 09:32:48', '2020-04-21 09:32:48');

-- --------------------------------------------------------

--
-- Table structure for table `credit_card`
--

CREATE TABLE `credit_card` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `name_on_card` varchar(255) DEFAULT NULL,
  `card_no` int(255) DEFAULT NULL,
  `card_type` varchar(255) DEFAULT NULL,
  `ccv` int(255) DEFAULT NULL,
  `expiry_date` varchar(255) DEFAULT NULL,
  `billing_address` varchar(255) DEFAULT NULL,
  `postal_code` int(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `credit_card`
--

INSERT INTO `credit_card` (`id`, `uid`, `username`, `name_on_card`, `card_no`, `card_type`, `ccv`, `expiry_date`, `billing_address`, `postal_code`, `status`, `date_created`, `date_updated`) VALUES
(1, '9349aede685bae49c49b0b895e465f6d', 'user', 'qwe', 123332211, 'Credit', 123, '05/2020', 'asd 123 asd', 123321, 'Active', '2020-05-06 01:56:15', '2020-05-06 01:58:18');

-- --------------------------------------------------------

--
-- Table structure for table `kitten`
--

CREATE TABLE `kitten` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `vaccinated` varchar(255) DEFAULT NULL,
  `dewormed` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `feature` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `seller` varchar(255) DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kitten`
--

INSERT INTO `kitten` (`id`, `uid`, `name`, `sku`, `slug`, `price`, `age`, `vaccinated`, `dewormed`, `gender`, `color`, `size`, `status`, `feature`, `breed`, `seller`, `details`, `link`, `location`, `image_one`, `image_two`, `image_three`, `image_four`, `date_created`, `date_updated`) VALUES
(1, '78e49c06ecf9a4d30d6629d1cdca8732', 'Kitten 1', '202005051547', 'admin-kitten-one', '2000', '1', 'Yes', 'Yes', 'Male', 'Red', 'Small', 'Available', 'Yes', 'Persian', 'Wiskey', 'admin kitten one', 'no video for kitten one', NULL, '78e49c06ecf9a4d30d6629d1cdca8732cat1.jpg', '78e49c06ecf9a4d30d6629d1cdca8732cat2.jpg', '78e49c06ecf9a4d30d6629d1cdca8732cat1.jpg', '78e49c06ecf9a4d30d6629d1cdca8732cat2.jpg', '2020-05-05 07:49:17', '2020-05-05 07:49:17'),
(2, '33122d17e9155ee408782b480860016f', 'Kitten 2', '202005051550', 'admin-kitten-two', '2100', '1.5', 'Yes', 'Yes', 'Female', 'Orange', 'Medium', 'Available', 'No', 'Bengal', 'Wiskey', 'admin kitten two', 'no video for kitten two', NULL, '33122d17e9155ee408782b480860016fcat3.jpg', '33122d17e9155ee408782b480860016fcat2.jpg', '33122d17e9155ee408782b480860016fcat3.jpg', '33122d17e9155ee408782b480860016fcat2.jpg', '2020-05-05 07:50:49', '2020-05-05 07:50:49'),
(3, '7edceafe0cb8cafb5b56b6e0a03d050c', 'Kitten 3', '202005051551', 'admin-kitten-three', '2200', '2', 'Yes', 'No', 'Female', 'Grey', 'Large', 'Available', 'No', 'Ragdoll', 'Wiskey', 'admin kitten three', 'no video for kitten three', NULL, '7edceafe0cb8cafb5b56b6e0a03d050ccat1.jpg', '7edceafe0cb8cafb5b56b6e0a03d050ccat2.jpg', '7edceafe0cb8cafb5b56b6e0a03d050ccat3.jpg', '7edceafe0cb8cafb5b56b6e0a03d050ccat3.jpg', '2020-05-05 07:51:46', '2020-05-05 07:51:46'),
(4, '7ee0cf980a960995710ae3275147732c', 'Kitten S1', '202005051609', 'seller-kitten-one', '500', '0.5', 'Yes', 'Yes', 'Male', 'Grey', 'Small', 'Rejected', 'Yes', 'Bengal', 'Wiskey', 'seller kitten one', 'no kitten video', NULL, '7ee0cf980a960995710ae3275147732ccat3.jpg', '7ee0cf980a960995710ae3275147732cdog5.png', '7ee0cf980a960995710ae3275147732ccat3.jpg', '7ee0cf980a960995710ae3275147732cdog5.png', '2020-05-05 08:11:32', '2020-05-05 08:11:32');

-- --------------------------------------------------------

--
-- Table structure for table `pet_details`
--

CREATE TABLE `pet_details` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `vaccinated` varchar(255) DEFAULT NULL,
  `dewormed` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `feature` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `seller` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pet_details`
--

INSERT INTO `pet_details` (`id`, `uid`, `name`, `sku`, `slug`, `price`, `age`, `vaccinated`, `dewormed`, `gender`, `color`, `size`, `status`, `feature`, `breed`, `seller`, `type`, `details`, `link`, `location`, `image_one`, `image_two`, `image_three`, `image_four`, `date_created`, `date_updated`) VALUES
(1, '5ba74d2aeed84d119a991e356985c254', 'Puppy 1', '202005051536', 'admin-puppy-one', '1000', '1', 'Yes', 'Yes', 'Male', 'Black', 'Small', 'Available', 'Yes', 'Chihuahua', 'Padigree', 'Puppy', 'admin puppy one', 'no video', NULL, '5ba74d2aeed84d119a991e356985c254dog1.jpg', '5ba74d2aeed84d119a991e356985c254dog2.png', '5ba74d2aeed84d119a991e356985c254dog1.jpg', '5ba74d2aeed84d119a991e356985c254dog2.png', '2020-05-05 07:39:36', '2020-05-05 07:39:36'),
(2, '633ce1edd0290ffd721fc56bf610f6dc', 'Puppy 2', '202005051539', 'admin-puppy-two', '1100', '1.5', 'Yes', 'No', 'Female', 'Brown', 'Medium', 'Available', 'Yes', 'German Shepherd', 'Padigree', 'Puppy', 'admin puppy two', 'no video 2', NULL, '633ce1edd0290ffd721fc56bf610f6dcdog2.png', '633ce1edd0290ffd721fc56bf610f6dcdog1.jpg', '633ce1edd0290ffd721fc56bf610f6dcdog2.png', '633ce1edd0290ffd721fc56bf610f6dcdog1.jpg', '2020-05-05 07:40:33', '2020-05-05 07:40:33'),
(3, '24241059b4ff90d8f4d46acf0436eaaa', 'Puppy 3', '202005051541', 'admin-puppy-three', '1200', '2', 'Yes', 'No', 'Male', 'Black', 'Large', 'Available', 'Yes', 'Puddle', 'Padigree', 'Puppy', 'admin puppy three', 'no video 3', NULL, '24241059b4ff90d8f4d46acf0436eaaadog3.jpg', '24241059b4ff90d8f4d46acf0436eaaadog4.png', '24241059b4ff90d8f4d46acf0436eaaadog3.jpg', '24241059b4ff90d8f4d46acf0436eaaadog4.png', '2020-05-05 07:41:55', '2020-05-05 07:41:55'),
(4, '78e49c06ecf9a4d30d6629d1cdca8732', 'Kitten 1', '202005051547', 'admin-kitten-one', '2000', '1', 'Yes', 'Yes', 'Male', 'Red', 'Small', 'Available', 'Yes', 'Persian', 'Wiskey', 'Kitten', 'admin kitten one', 'no video for kitten one', NULL, '78e49c06ecf9a4d30d6629d1cdca8732cat1.jpg', '78e49c06ecf9a4d30d6629d1cdca8732cat2.jpg', '78e49c06ecf9a4d30d6629d1cdca8732cat1.jpg', '78e49c06ecf9a4d30d6629d1cdca8732cat2.jpg', '2020-05-05 07:49:17', '2020-05-05 07:49:17'),
(5, '33122d17e9155ee408782b480860016f', 'Kitten 2', '202005051550', 'admin-kitten-two', '2100', '1.5', 'Yes', 'Yes', 'Female', 'Orange', 'Medium', 'Available', 'No', 'Bengal', 'Wiskey', 'Kitten', 'admin kitten two', 'no video for kitten two', NULL, '33122d17e9155ee408782b480860016fcat3.jpg', '33122d17e9155ee408782b480860016fcat2.jpg', '33122d17e9155ee408782b480860016fcat3.jpg', '33122d17e9155ee408782b480860016fcat2.jpg', '2020-05-05 07:50:49', '2020-05-05 07:50:49'),
(6, '7edceafe0cb8cafb5b56b6e0a03d050c', 'Kitten 3', '202005051551', 'admin-kitten-three', '2200', '2', 'Yes', 'No', 'Female', 'Grey', 'Large', 'Available', 'No', 'Ragdoll', 'Wiskey', 'Kitten', 'admin kitten three', 'no video for kitten three', NULL, '7edceafe0cb8cafb5b56b6e0a03d050ccat1.jpg', '7edceafe0cb8cafb5b56b6e0a03d050ccat2.jpg', '7edceafe0cb8cafb5b56b6e0a03d050ccat3.jpg', '7edceafe0cb8cafb5b56b6e0a03d050ccat3.jpg', '2020-05-05 07:51:46', '2020-05-05 07:51:46'),
(7, 'b36c9ea0ac4833c21891340d5089bd36', 'Reptile 1', '202005051553', 'admin-reptile-onw', '3000', '1', 'Yes', 'Yes', 'Male', 'Black', 'Small', 'Available', 'Yes', 'Lizard', 'Padigree', 'Reptile', 'admin reptile one', 'no reptile video', NULL, 'b36c9ea0ac4833c21891340d5089bd36reptile1.jpg', 'b36c9ea0ac4833c21891340d5089bd36reptile2.jpg', 'b36c9ea0ac4833c21891340d5089bd36reptile1.jpg', 'b36c9ea0ac4833c21891340d5089bd36reptile2.jpg', '2020-05-05 07:54:34', '2020-05-05 07:54:34'),
(8, '9fef3d61655769ea38a7caa1ccb4e9f9', 'Puppy Seller 1', '202005051600', 'seller-puppy-one', '800', '2', 'Yes', 'Yes', 'Male', 'Black', 'Small', 'Available', 'Yes', 'Terrier', 'Wiskey', 'Puppy', 'seller puppy one', 'no', NULL, '9fef3d61655769ea38a7caa1ccb4e9f9e.jpg', '9fef3d61655769ea38a7caa1ccb4e9f9f.jpg', '9fef3d61655769ea38a7caa1ccb4e9f9e.jpg', '9fef3d61655769ea38a7caa1ccb4e9f9f.jpg', '2020-05-05 08:01:49', '2020-05-05 08:01:49'),
(9, '64f687ed69cd2f1bc23f1940c7cf8695', 'Puppy Seller 2', '202005051606', 'seller-puppy-two', '800', '2.5', 'Yes', 'Yes', 'Female', 'Brown', 'Medium', 'Available', 'Yes', 'Mix Brand', 'Wiskey', 'Puppy', 'seller puppy two', 'no', NULL, '64f687ed69cd2f1bc23f1940c7cf8695f.jpg', '64f687ed69cd2f1bc23f1940c7cf8695dog3.jpg', '64f687ed69cd2f1bc23f1940c7cf8695f.jpg', '64f687ed69cd2f1bc23f1940c7cf8695dog3.jpg', '2020-05-05 08:07:06', '2020-05-05 08:07:06'),
(10, '7ee0cf980a960995710ae3275147732c', 'Kitten S1', '202005051609', 'seller-kitten-one', '500', '0.5', 'Yes', 'Yes', 'Male', 'Grey', 'Small', 'Rejected', 'Yes', 'Bengal', 'Wiskey', 'Kitten', 'seller kitten one', 'no kitten video', NULL, '7ee0cf980a960995710ae3275147732ccat3.jpg', '7ee0cf980a960995710ae3275147732cdog5.png', '7ee0cf980a960995710ae3275147732ccat3.jpg', '7ee0cf980a960995710ae3275147732cdog5.png', '2020-05-05 08:11:32', '2020-05-05 08:11:32'),
(11, '9044d700fca81f5751f54cebe650f5b4', 'Reptile Seller 1', '202005051616', 'seller-reptile-one', '5000', '2.5', 'Yes', 'Yes', 'Male', 'Green', 'Medium', 'Available', 'No', 'Frog', 'Wiskey', 'Reptile', 'seller reptile one', 'no reptile video', NULL, '9044d700fca81f5751f54cebe650f5b4reptile4.jpg', '9044d700fca81f5751f54cebe650f5b4reptile3.jpg', '9044d700fca81f5751f54cebe650f5b4reptile3.jpg', '9044d700fca81f5751f54cebe650f5b4reptile4.jpg', '2020-05-05 08:17:12', '2020-05-05 08:17:12');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `animal_type` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image_one` varchar(255) NOT NULL,
  `image_two` varchar(255) NOT NULL,
  `image_three` varchar(255) NOT NULL,
  `image_four` varchar(255) NOT NULL,
  `variation_one` varchar(255) NOT NULL,
  `variation_one_price` varchar(255) NOT NULL,
  `variation_one_stock` int(255) NOT NULL,
  `variation_one_image` varchar(255) NOT NULL,
  `variation_two` varchar(255) NOT NULL,
  `variation_two_price` varchar(255) NOT NULL,
  `variation_two_stock` varchar(255) NOT NULL,
  `variation_two_image` varchar(255) NOT NULL,
  `variation_three` varchar(255) NOT NULL,
  `variation_three_price` varchar(255) NOT NULL,
  `variation_three_stock` varchar(255) NOT NULL,
  `variation_three_image` varchar(255) NOT NULL,
  `variation_four` varchar(255) NOT NULL,
  `variation_four_price` varchar(255) NOT NULL,
  `variation_four_stock` varchar(255) NOT NULL,
  `variation_four_image` varchar(255) NOT NULL,
  `variation_five` varchar(255) NOT NULL,
  `variation_five_price` varchar(255) NOT NULL,
  `variation_five_stock` varchar(255) NOT NULL,
  `variation_five_image` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `uid`, `name`, `sku`, `slug`, `animal_type`, `category`, `brand`, `expiry_date`, `status`, `description`, `link`, `image_one`, `image_two`, `image_three`, `image_four`, `variation_one`, `variation_one_price`, `variation_one_stock`, `variation_one_image`, `variation_two`, `variation_two_price`, `variation_two_stock`, `variation_two_image`, `variation_three`, `variation_three_price`, `variation_three_stock`, `variation_three_image`, `variation_four`, `variation_four_price`, `variation_four_stock`, `variation_four_image`, `variation_five`, `variation_five_price`, `variation_five_stock`, `variation_five_image`, `date_created`, `date_updated`) VALUES
(1, '6e82d66f1390da3082e30015f8f1eef0', 'Padigree', '147', 'www.pets.com', 'Puppy', 'Food', 'Brand 3', '2020-04-15', 'Available', 'Food', 'http://links.com', 'pedigree1.jpg', '', '', '', '', '30', 90, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-21 13:28:41', '2020-04-21 13:28:41'),
(2, '9fcd63337d049e2639899551fe813f15', 'whiskas', '12345', 'https://www.computerhope.com', 'Kitten', 'Food', 'Brand 2', '2022-02-01', 'Available', '- Daily oral care treat for your pet.\r\n- Unique tasty X-shaped treat clinically proven to reduce plaque.\r\n- It has active ingredients zinc sulphate & sodium trio polyphosphate that help in slow down the rate of tartar build up.', 'EhhiY11Z9-U', 'product1.jpg', 'product2.jpg', 'product3.jpg', 'product4.jpg', '', '31', 100, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-21 14:18:54', '2020-04-21 14:18:54'),
(3, 'b36dce4d1eb8e7c9dc250c17b6f4702b', 'Bone Toy', '321', 'https://www.computerhope.com', 'Other', 'Accessory', 'Brand 1', '0000-00-00', 'Available', 'Toy', 'http://link.com', 'toy1.jpg', '', '', '', '', '15', 100, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-21 14:27:25', '2020-04-21 14:27:25'),
(4, 'fb6d754eba4857ed504b1742feb01807', 'Comb', '999', 'https://www.pets.com', 'Kitten', 'Grooming', 'Brand 1', '2020-04-04', 'Available', 'Best slicker brush', 'http://link.com', 'comb1.jpg', '', '', '', '', '50', 80, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-23 03:59:10', '2020-04-23 03:59:10'),
(5, '3f1174527baedc46efe9b0bb4f4039f8', 'Donut dog beds', '333', 'https://www.pets.com', 'Puppy', 'Accessory', 'Brand 3', '2020-04-30', 'Available', 'Comfy', 'http://link.com', 'donut1.jpg', '', '', '', '', '250', 50, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-23 04:09:16', '2020-04-23 04:09:16'),
(6, '139b7417b15f91efbbf0e371a9ef7ab9', 'Alpo', '789456123', 'https://www.alpo.com', 'Puppy', 'Food', 'Brand 3', '2021-12-09', 'Available', 'Yummy and most favorite', 'VhtE47ANUA0', 'alpo1.jpg', 'alpo2.png', 'alpo3.jpg', 'alpo4.png', '', '30', 100, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-24 04:22:40', '2020-04-24 04:22:40'),
(7, 'bc8369fb02ea8e8edfe48ac9efc68da4', 'squeeze', '3578951', 'https://www.pets.com', 'Reptile', 'Grooming', 'Brand 2', '2020-11-19', 'Available', 'Toy', 'EhhiY11Z9-U', 'ball1.jpg', '', '', '', '', '41', 100, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-24 05:47:11', '2020-04-24 05:47:11'),
(8, 'ed415a8065fea0e6d4d884239fa3d5c0', 'Sheba', '798456123', 'https://www.sheba.com', 'Kitten', 'Food', 'Brand 3', '2022-06-14', 'Available', 'Food', 'PFjDisXO754', 'sheba1.jpg', 'sheba2.jpg', 'sheba3.jpg', 'sheba4.jpg', '', '16', 50, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2020-04-24 05:48:47', '2020-04-24 05:48:47');

-- --------------------------------------------------------

--
-- Table structure for table `puppy`
--

CREATE TABLE `puppy` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `vaccinated` varchar(255) DEFAULT NULL,
  `dewormed` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `feature` varchar(255) DEFAULT NULL,
  `breed` varchar(255) DEFAULT NULL,
  `seller` varchar(255) DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) DEFAULT NULL,
  `image_two` varchar(255) DEFAULT NULL,
  `image_three` varchar(255) DEFAULT NULL,
  `image_four` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `puppy`
--

INSERT INTO `puppy` (`id`, `uid`, `name`, `sku`, `slug`, `price`, `age`, `vaccinated`, `dewormed`, `gender`, `color`, `size`, `status`, `feature`, `breed`, `seller`, `details`, `link`, `location`, `image_one`, `image_two`, `image_three`, `image_four`, `date_created`, `date_updated`) VALUES
(1, '5ba74d2aeed84d119a991e356985c254', 'Puppy 1', '202005051536', 'admin-puppy-one', '1000', '1', 'Yes', 'Yes', 'Male', 'Black', 'Small', 'Available', 'Yes', 'Chihuahua', 'Padigree', 'admin puppy one', 'no video', NULL, '5ba74d2aeed84d119a991e356985c254dog1.jpg', '5ba74d2aeed84d119a991e356985c254dog2.png', '5ba74d2aeed84d119a991e356985c254dog1.jpg', '5ba74d2aeed84d119a991e356985c254dog2.png', '2020-05-05 07:39:36', '2020-05-05 07:39:36'),
(2, '633ce1edd0290ffd721fc56bf610f6dc', 'Puppy 2', '202005051539', 'admin-puppy-two', '1100', '1.5', 'Yes', 'No', 'Female', 'Brown', 'Medium', 'Available', 'Yes', 'German Shepherd', 'Padigree', 'admin puppy two', 'no video 2', NULL, '633ce1edd0290ffd721fc56bf610f6dcdog2.png', '633ce1edd0290ffd721fc56bf610f6dcdog1.jpg', '633ce1edd0290ffd721fc56bf610f6dcdog2.png', '633ce1edd0290ffd721fc56bf610f6dcdog1.jpg', '2020-05-05 07:40:33', '2020-05-05 07:40:33'),
(3, '24241059b4ff90d8f4d46acf0436eaaa', 'Puppy 3', '202005051541', 'admin-puppy-three', '1200', '2', 'Yes', 'No', 'Male', 'Black', 'Large', 'Available', 'Yes', 'Puddle', 'Padigree', 'admin puppy three', 'no video 3', NULL, '24241059b4ff90d8f4d46acf0436eaaadog3.jpg', '24241059b4ff90d8f4d46acf0436eaaadog4.png', '24241059b4ff90d8f4d46acf0436eaaadog3.jpg', '24241059b4ff90d8f4d46acf0436eaaadog4.png', '2020-05-05 07:41:55', '2020-05-05 07:41:55'),
(4, '9fef3d61655769ea38a7caa1ccb4e9f9', 'Puppy Seller 1', '202005051600', 'seller-puppy-one', '800', '2', 'Yes', 'Yes', 'Male', 'Black', 'Small', 'Available', 'Yes', 'Terrier', 'Wiskey', 'seller puppy one', 'no', NULL, '9fef3d61655769ea38a7caa1ccb4e9f9e.jpg', '9fef3d61655769ea38a7caa1ccb4e9f9f.jpg', '9fef3d61655769ea38a7caa1ccb4e9f9e.jpg', '9fef3d61655769ea38a7caa1ccb4e9f9f.jpg', '2020-05-05 08:01:49', '2020-05-05 08:01:49'),
(5, '64f687ed69cd2f1bc23f1940c7cf8695', 'Puppy Seller 2', '202005051606', 'seller-puppy-two', '800', '2.5', 'Yes', 'Yes', 'Female', 'Brown', 'Medium', 'Available', 'Yes', 'Mix Brand', 'Wiskey', 'seller puppy two', 'no', NULL, '64f687ed69cd2f1bc23f1940c7cf8695f.jpg', '64f687ed69cd2f1bc23f1940c7cf8695dog3.jpg', '64f687ed69cd2f1bc23f1940c7cf8695f.jpg', '64f687ed69cd2f1bc23f1940c7cf8695dog3.jpg', '2020-05-05 08:07:06', '2020-05-05 08:07:06');

-- --------------------------------------------------------

--
-- Table structure for table `reported_article`
--

CREATE TABLE `reported_article` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `article_uid` varchar(255) DEFAULT NULL,
  `reason` text DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reptile`
--

CREATE TABLE `reptile` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `price` int(255) NOT NULL,
  `age` int(3) NOT NULL,
  `vaccinated` varchar(255) NOT NULL,
  `dewormed` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `size` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `feature` varchar(255) NOT NULL,
  `breed` varchar(255) NOT NULL,
  `seller` varchar(255) NOT NULL,
  `details` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `location` varchar(255) DEFAULT NULL,
  `image_one` varchar(255) NOT NULL,
  `image_two` varchar(255) NOT NULL,
  `image_three` varchar(255) NOT NULL,
  `image_four` varchar(255) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reptile`
--

INSERT INTO `reptile` (`id`, `uid`, `name`, `sku`, `slug`, `price`, `age`, `vaccinated`, `dewormed`, `gender`, `color`, `size`, `status`, `feature`, `breed`, `seller`, `details`, `link`, `location`, `image_one`, `image_two`, `image_three`, `image_four`, `date_created`, `date_updated`) VALUES
(1, 'b36c9ea0ac4833c21891340d5089bd36', 'Reptile 1', '202005051553', 'admin-reptile-onw', 3000, 1, 'Yes', 'Yes', 'Male', 'Black', 'Small', 'Available', 'Yes', 'Lizard', 'Padigree', 'admin reptile one', 'no reptile video', NULL, 'b36c9ea0ac4833c21891340d5089bd36reptile1.jpg', 'b36c9ea0ac4833c21891340d5089bd36reptile2.jpg', 'b36c9ea0ac4833c21891340d5089bd36reptile1.jpg', 'b36c9ea0ac4833c21891340d5089bd36reptile2.jpg', '2020-05-05 07:54:34', '2020-05-05 07:54:34'),
(2, '9044d700fca81f5751f54cebe650f5b4', 'Reptile Seller 1', '202005051616', 'seller-reptile-one', 5000, 3, 'Yes', 'Yes', 'Male', 'Green', 'Medium', 'Available', 'No', 'Frog', 'Wiskey', 'seller reptile one', 'no reptile video', NULL, '9044d700fca81f5751f54cebe650f5b4reptile4.jpg', '9044d700fca81f5751f54cebe650f5b4reptile3.jpg', '9044d700fca81f5751f54cebe650f5b4reptile3.jpg', '9044d700fca81f5751f54cebe650f5b4reptile4.jpg', '2020-05-05 08:17:12', '2020-05-05 08:17:12');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `author_uid` varchar(255) DEFAULT NULL,
  `author_name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `paragraph_one` text DEFAULT NULL,
  `paragraph_two` text DEFAULT NULL,
  `paragraph_three` text DEFAULT NULL,
  `paragraph_four` text DEFAULT NULL,
  `image` text DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `display` varchar(255) DEFAULT 'YES',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `uid`, `author_uid`, `author_name`, `title`, `paragraph_one`, `paragraph_two`, `paragraph_three`, `paragraph_four`, `image`, `type`, `display`, `date_created`, `date_updated`) VALUES
(1, '53fab3d15ede806c3712cef3c64463a8', '9349aede685bae49c49b0b895e465f6d', 'user', NULL, 'qwe asd', NULL, NULL, NULL, '9349aede685bae49c49b0b895e465f6dshare_6886.png', NULL, 'Pending', '2020-05-06 03:59:21', '2020-05-06 03:59:21');

-- --------------------------------------------------------

--
-- Table structure for table `seller`
--

CREATE TABLE `seller` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'random user id',
  `company_name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `registration_no` varchar(255) DEFAULT NULL,
  `contact_no` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `account_status` varchar(255) DEFAULT NULL,
  `contact_person` varchar(255) DEFAULT NULL,
  `contact_person_no` varchar(255) DEFAULT NULL,
  `experience` varchar(255) DEFAULT NULL,
  `cert` varchar(255) DEFAULT NULL,
  `services` varchar(255) DEFAULT NULL,
  `breed_type` varchar(255) DEFAULT NULL,
  `other_info` varchar(255) DEFAULT NULL,
  `company_logo` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seller`
--

INSERT INTO `seller` (`id`, `uid`, `company_name`, `slug`, `registration_no`, `contact_no`, `address`, `state`, `account_status`, `contact_person`, `contact_person_no`, `experience`, `cert`, `services`, `breed_type`, `other_info`, `company_logo`, `date_created`, `date_updated`) VALUES
(1, '766cbd1be5407a0b08f3524feda42010', 'Padigree', 'www.padigree.com.my', 'AA1123BCC', '010520520', '12, Padigree Building, Jln SS2', 'State', 'Active', 'padigree pic', '012456456', '3', 'many many certs', 'Puppy Sellers, Kitten Sellers, Reptile Sellers', 'lots of breed', 'mengmian', '', '2020-04-28 04:12:33', '2020-04-28 04:12:33'),
(2, '2984dae6a2927c72561afdce6e68ae05', 'Wiskey', 'www.wiskey.com', 'AA121212', '0108528520', '12, Wiskey Area, Jln SS5', 'PJ KL', 'Active', 'Wiskey', '012123321', '3', 'certs', 'Puppy Sellers, Kitten Sellers', 'breeds', 'wiskey', NULL, '2020-04-28 04:41:52', '2020-04-28 04:41:52');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL COMMENT 'random user id',
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `phone_no` varchar(255) DEFAULT NULL,
  `tac` varchar(255) DEFAULT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `fb_id` varchar(255) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `account_status` varchar(255) DEFAULT NULL,
  `receiver_name` varchar(255) DEFAULT NULL,
  `receiver_contact_no` varchar(255) DEFAULT NULL,
  `shipping_state` varchar(255) DEFAULT NULL,
  `shipping_area` varchar(255) DEFAULT NULL,
  `shipping_postal_code` varchar(255) DEFAULT NULL,
  `shipping_address` text DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_account_holder` varchar(255) DEFAULT NULL,
  `bank_account_no` varchar(255) DEFAULT NULL,
  `name_on_card` varchar(255) DEFAULT NULL,
  `card_no` varchar(255) DEFAULT NULL,
  `card_type` varchar(255) DEFAULT NULL,
  `expiry_date` varchar(255) DEFAULT NULL,
  `ccv` int(255) DEFAULT NULL,
  `postal_code` int(255) DEFAULT NULL,
  `billing_address` varchar(255) DEFAULT NULL,
  `email_verification_code` varchar(10) DEFAULT NULL,
  `is_email_verified` tinyint(1) NOT NULL DEFAULT 1,
  `is_phone_verified` tinyint(1) NOT NULL DEFAULT 0,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `name`, `email`, `country`, `phone_no`, `tac`, `password`, `salt`, `user_type`, `fb_id`, `birthday`, `gender`, `account_status`, `receiver_name`, `receiver_contact_no`, `shipping_state`, `shipping_area`, `shipping_postal_code`, `shipping_address`, `bank_name`, `bank_account_holder`, `bank_account_no`, `name_on_card`, `card_no`, `card_type`, `expiry_date`, `ccv`, `postal_code`, `billing_address`, `email_verification_code`, `is_email_verified`, `is_phone_verified`, `date_created`, `date_updated`) VALUES
(1, 'd156938c9b63fd253e9cddf89eba10e7', 'admin', 'admin@gmail.com', 'Malaysia', '0121122330', NULL, '273b45ab12adb8cca007bd5070c63b98e20a17dc9c7c624b84eeb4687174c6a2', '3b601f88e6cfac445f25b44c65b08c60d72aca00', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-04-28 04:05:16', '2020-04-28 05:52:34'),
(2, '9349aede685bae49c49b0b895e465f6d', 'user', 'user@gmail.com', 'Malaysia', '0124455660', NULL, 'fb1343bf3ac191ede70b6d323ac4a19b10905f49ad1df13338c5b6a40ca33f54', 'e70e7e71ece4a3469028b5a4b2cf316bbb448d70', 1, NULL, NULL, NULL, NULL, 'user', '0126012', 'cvb', 'asd zxc', '123321', '123,asd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-04-28 04:05:55', '2020-05-06 01:49:08'),
(3, 'c2e4a20007e868502ea2857e094af93b', 'user2', 'user2@gmail.com', 'Singapore', '0127788990', NULL, 'fa0a1acac2e0844b2a9996cf8d57d86db163b04f7e93cdd157056992962273e4', '84d6588f07b2dd352d4e413a2dfe53fad13ff245', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-04-28 04:06:21', '2020-04-28 05:40:25'),
(4, '766cbd1be5407a0b08f3524feda42010', 'Padigree', NULL, NULL, '011123123', NULL, '8a237afd4ecec2ede22e3ec7b19a7563713e360654cc7520c4b87dbf5c22ed56', '93e7d0116161e1ad9807f8aaeccf149cd8a27ab3', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-04-28 04:12:33', '2020-04-28 04:41:59'),
(5, '2984dae6a2927c72561afdce6e68ae05', 'Wiskey', NULL, NULL, '0127412369852', NULL, '6e94df6abf1ff37093533c73c5ec84617081a00c3f384bd3614bc4f237da76dc', 'b20e148c5f6a3889975469997107dd95cf0cc9ca', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-04-28 04:41:52', '2020-04-29 09:06:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `breed`
--
ALTER TABLE `breed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `credit_card`
--
ALTER TABLE `credit_card`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kitten`
--
ALTER TABLE `kitten`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pet_details`
--
ALTER TABLE `pet_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `puppy`
--
ALTER TABLE `puppy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reported_article`
--
ALTER TABLE `reported_article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reptile`
--
ALTER TABLE `reptile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller`
--
ALTER TABLE `seller`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `breed`
--
ALTER TABLE `breed`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `color`
--
ALTER TABLE `color`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `credit_card`
--
ALTER TABLE `credit_card`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kitten`
--
ALTER TABLE `kitten`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pet_details`
--
ALTER TABLE `pet_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `puppy`
--
ALTER TABLE `puppy`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `reported_article`
--
ALTER TABLE `reported_article`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reptile`
--
ALTER TABLE `reptile`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `seller`
--
ALTER TABLE `seller`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
