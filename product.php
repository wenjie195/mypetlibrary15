<?php
//require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Product.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$products = getProduct($conn);

$productListHtml = "";

if(isset($_POST['product-list-quantity-input'])){
    $productListHtml = createProductList($products,1,$_POST['product-list-quantity-input']);
}
else{
    $productListHtml = createProductList($products);
}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Malaysia Pet Food Toy Product | Mypetslibrary" />
<title>Malaysia Pet Food Toy Product | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="pet food, pet grooming, pet shampoo, toy for pet, pet product, Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'userHeaderAfterLogin.php'; ?>

<!-- Product Filter Modal -->
<div id="variation-modal">
<!-- <div id="variation-modal" class="modal-css"> -->

<!-- Modal content -->
<div class="modal-content-css filter-modal variation-modal-css">
  <span class="close-css close-variation">&times;</span>
  <h2 class="green-text h2-title">Amount and Variation</h2>
  <div class="green-border filter-border"></div>
  <div class="clear"></div>
  <div class="variation-left-div">
      <div class="variation-product-preview">
      <div  id="Variation1" class="tabcontent block2">
          <img src="img/product-1.jpg" class="variation-img" alt="Product Name" title="Product Name"  />
      </div>
      <div  id="Variation2" class="tabcontent">
          <img src="img/product-2.jpg" class="variation-img" alt="Product Name" title="Product Name"  />
      </div>
      <div  id="Variation3" class="tabcontent">
          <img src="img/product-3.jpg" class="variation-img" alt="Product Name" title="Product Name"  />
      </div>
      <div  id="Variation4" class="tabcontent">
          <img src="img/product-4.jpg" class="variation-img" alt="Product Name" title="Product Name"  />    
      </div> 
      
      </div>
  </div>
  <div class="variation-right-div">
          <div class="tab variation-tab">

              <button class="tablinks active variation-btn" onclick="openTab(event, 'Variation1')">
                  <label for="variation1" class="filter-label filter-label3 variation-label">Variation 1
                      <input type="checkbox" name="variation1" id="variation1" class="filter-input" value="0"/>
                      <span class="checkmark"></span>
                  </label>
              </button>
              <div id="variation1Div" class="variation-hidden-div" style="display:none">
                  <p class="left-stock-p">Stock: 200</p>
                  <p class="right-price-p">RM28.00</p>
                  <div class="clear"></div>
                  <!--
                  <p class="quantity-p">Quantity</p>
                  <p class="right-amount-p">0</p>
                  -->
                  <p class="quantity-p">Quantity</p>
                  <div class="numbers-row numbers-row-css">



                  </div>

              </div>
              <button class="tablinks variation-btn" onclick="openTab(event, 'Variation2')">
                  <label for="variation2" class="filter-label filter-label3 variation-label">Variation 2
                      <input type="checkbox" name="variation2" id="variation2" class="filter-input" value="0"/>
                      <span class="checkmark"></span>
                  </label>
              </button>
              <div id="variation2Div" class="variation-hidden-div" style="display:none">
                  <p class="left-stock-p">Stock: 200</p>
                  <p class="right-price-p">RM28.00</p>
                  <div class="clear"></div>
                  <!--
                  <p class="quantity-p">Quantity</p>
                  <p class="right-amount-p">0</p>
                  -->
                  <p class="quantity-p">Quantity</p>
                  <div class="numbers-row2 numbers-row-css">



                  </div>

              </div> 
              <button class="tablinks variation-btn" onclick="openTab(event, 'Variation3')">
                  <label for="variation3" class="filter-label filter-label3 variation-label">Variation 3
                      <input type="checkbox" name="variation3" id="variation3" class="filter-input" value="0"/>
                      <span class="checkmark"></span>
                  </label>
              </button>
              <div id="variation3Div" class="variation-hidden-div" style="display:none">
                  <p class="left-stock-p">Stock: 200</p>
                  <p class="right-price-p">RM28.00</p>
                  <div class="clear"></div>
                  <!--
                  <p class="quantity-p">Quantity</p>
                  <p class="right-amount-p">0</p>
                  -->
                  <p class="quantity-p">Quantity</p>
                  <div class="numbers-row3 numbers-row-css">



                  </div>

              </div>                 
              <button class="tablinks variation-btn" onclick="openTab(event, 'Variation4')">
                  <label for="variation4" class="filter-label filter-label3 variation-label">Variation 4
                      <input type="checkbox" name="variation4" id="variation4" class="filter-input" value="0"/>
                      <span class="checkmark"></span>
                  </label>
              </button>
              <div id="variation4Div" class="variation-hidden-div" style="display:none">
                  <p class="left-stock-p">Stock: 200</p>
                  <p class="right-price-p">RM28.00</p>
                  <div class="clear"></div>
                  <!--
                  <p class="quantity-p">Quantity</p>
                  <p class="right-amount-p">0</p>
                  -->
                  <p class="quantity-p">Quantity</p>
                  <div class="numbers-row4 numbers-row-css">



                  </div>

              </div>  
              <div class="clear"></div>               
          
          </div>   

         </div>
      <div class="clear"></div>
      <div class="purchase-bottom-div overflow">
              <div class="width100 variation-bottom-div">
                  <p class="left-total-p">Total</p>
                  <p class="right-total-price">RM0.00</p>
              
              </div>         
          <button class="left-orange-div clean orange-button">Add to Cart</button>
          <button class="right-red-div clean red-btn">Buy Now</button>
      </div>
  </div>      

</div>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>