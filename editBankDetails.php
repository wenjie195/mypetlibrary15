<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Bank.php';
require_once dirname(__FILE__) . '/classes/CreditCard.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$userBank = getBank($conn, "WHERE uid =? AND status = 'Active' ",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

$userCreditCard = getCreditCard($conn, "WHERE uid =? AND status = 'Active' ",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Bank Details | Mypetslibrary" />
<title>Edit Bank Details | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="width100 same-padding overflow min-height menu-distance2">

    <?php
    if($userCreditCard)
    {   
        $totalCard = count($userCreditCard);
    }
    else
    {   $totalCard = 0;   }
    ?>

    <p class="review-product-name left-align-p">Credit Card (<?php echo $totalCard;?>)</p>
    
    <a href="bankCard.php">
        <div class="left-add-btn green-button white-text clean2">Add</div>
    </a>

    <div class="clear"></div>

        <table class="card-table">
            <!-- <tr>
                <td>Card No. XXXX XXXX 1200</td>
                <td><a href="bankCardEdit.php" class="green-a hover1"> <img src="img/edit.png" class="edit-png hover1a" alt="Edit" title="Edit"><img src="img/edit2.png" class="edit-png hover1b" alt="Edit" title="Edit">Edit</a></td>
                <td><div class="transparent-button clean delete-btn hover1 open-confirm"><img src="img/close2.png" class="edit-png hover1a" alt="Delete" title="Delete"><img src="img/close3.png" class="edit-png hover1b" alt="Delete" title="Delete">Delete</div></td>
            </tr>
            <tr>
                <td>Card No. XXXX XXXX 2200</td>
                <td><a href="bankCardEdit.php" class="green-a hover1"> <img src="img/edit.png" class="edit-png hover1a" alt="Edit" title="Edit"><img src="img/edit2.png" class="edit-png hover1b" alt="Edit" title="Edit">Edit</a></td>
                <td><div class="transparent-button clean delete-btn hover1 open-confirm"><img src="img/close2.png" class="edit-png hover1a" alt="Delete" title="Delete"><img src="img/close3.png" class="edit-png hover1b" alt="Delete" title="Delete">Delete</div></td>
            </tr>             -->

            <?php
            $conn = connDB();
            if($userCreditCard)
            {
                for($cntAA = 0;$cntAA < count($userCreditCard) ;$cntAA++)
                {
                ?>
                <tr>            
                    <td>Card Number : <?php echo $userCreditCard[$cntAA]->getCardNo();?></td>               

                    <td>
                        <form method="POST" action="bankCardEdit.php">
                            <button class="clean green-a hover1 transparent-button" type="submit" name="card_id" value="<?php echo $userCreditCard[$cntAA]->getId();?>">
                                <img src="img/edit.png" class="edit-png hover1a" alt="Edit" title="Edit"><img src="img/edit2.png" class="edit-png hover1b" alt="Edit" title="Edit">Edit
                            </button>
                        </form>

                    </td>

                    <td>
                        <form method="POST" action="utilities/deleteBankCardFunction.php">
                            <button class="clean green-a hover1 transparent-button" type="submit" id="card_number" name="card_number" value="<?php echo $userCreditCard[$cntAA]->getCardNo();?>">
                                <img src="img/close2.png" class="edit-png hover1a" alt="Delete" title="Delete"><img src="img/close3.png" class="edit-png hover1b" alt="Delete" title="Delete">Delete
                            </button>
                            <input class="input-name clean" type="hidden" value="<?php echo $userCreditCard[$cntAA]->getUsername();?>" name="card_user" id="card_user" readonly>    
                        </form>
                    </td>
                            
                </tr>
                <?php
                }
                ?>
            <?php
            }
            $conn->close();
            ?>

        </table>

        <div class="clear"></div>

        <?php
        if($userBank)
        {   
            $totalBank = count($userBank);
        }
        else
        {   $totalBank = 0;   }
        ?>

        <p class="review-product-name left-align-p">Bank Account (<?php echo $totalBank;?>)</p>
        <a href="bankDetails.php">
            <div class="left-add-btn green-button white-text clean2">Add</div>
        </a>
        
        <div class="clear"></div>
        
        <table class="card-table">
            <!-- <tr>
                <td>Bank No. XXXX XXXX 1200</td>
                <td><a href="bankDetailsEdit.php" class="green-a hover1"> <img src="img/edit.png" class="edit-png hover1a" alt="Edit" title="Edit"><img src="img/edit2.png" class="edit-png hover1b" alt="Edit" title="Edit">Edit</a></td>
                <td><div class="transparent-button clean delete-btn hover1 open-confirm"><img src="img/close2.png" class="edit-png hover1a" alt="Delete" title="Delete"><img src="img/close3.png" class="edit-png hover1b" alt="Delete" title="Delete">Delete</div></td>
            </tr>
            <tr>
                <td>Bank No. XXXX XXXX 2200</td>
                <td><a href="bankDetailsEdit.php" class="green-a hover1"> <img src="img/edit.png" class="edit-png hover1a" alt="Edit" title="Edit"><img src="img/edit2.png" class="edit-png hover1b" alt="Edit" title="Edit">Edit</a></td>
                <td><div class="transparent-button clean delete-btn hover1 open-confirm"><img src="img/close2.png" class="edit-png hover1a" alt="Delete" title="Delete"><img src="img/close3.png" class="edit-png hover1b" alt="Delete" title="Delete">Delete</div></td>
            </tr>             -->

            <?php
            $conn = connDB();
            if($userBank)
            {
                for($cnt = 0;$cnt < count($userBank) ;$cnt++)
                {
                ?>
                <tr>            
                    <td>Bank Number : <?php echo $userBank[$cnt]->getBankAccountNo();?></td>               

                    <td>
                        <form method="POST" action="bankDetailsEdit.php">
                            <button class="clean edit-btn green-a hover1 transparent-button" type="submit" name="bank_id" value="<?php echo $userBank[$cnt]->getId();?>">
                                <img src="img/edit.png" class="edit-png hover1a" alt="Edit" title="Edit"><img src="img/edit2.png" class="edit-png hover1b" alt="Edit" title="Edit">Edit
                            </button>
                        </form>
                    </td>

                    <td>
                        <form method="POST" action="utilities/deleteBankAccountFunction.php">
                            <button class="clean green-a hover1 transparent-button" type="submit" id="bank_acc_number" name="bank_acc_number" value="<?php echo $userBank[$cnt]->getBankAccountNo();?>">
                                <img src="img/close2.png" class="edit-png hover1a" alt="Delete" title="Delete"><img src="img/close3.png" class="edit-png hover1b" alt="Delete" title="Delete">Delete
                            </button>
                            <input class="input-name clean" type="hidden" value="<?php echo $userBank[$cnt]->getUsername();?>" name="bank_user" id="bank_user" readonly>    
                        </form>
                    </td>
                            
                </tr>
                <?php
                }
                ?>
            <?php
            }
            $conn->close();
            ?>

        </table>

        <!-- Double Confirm Modal -->
        <div id="confirm-modal" class="modal-css">
            <!-- Modal content -->
            <div class="modal-content-css confirm-modal-margin">
                <span class="close-css close-confirm">&times;</span>
                <div class="clear"></div>
                <h2 class="green-text h2-title confirm-title">Confirm Delete?</h2>
                <div class="clean cancel-btn close-confirm">Cancel</div>
                <button class="clean red-btn delete-btn2">Delete</button>
                <div class="clear"></div>
            </div>
        </div>      

    <div class="clear"></div>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Credit Card Added Successfully"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Bank Account Added Successfully"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Credit Card Updated"; 
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Bank Account Updated"; 
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Fail to Update Credit Card Details !!"; 
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "ERROR"; 
        }
        else if($_GET['type'] == 7)
        {
            $messageType = "Fail to Update Bank Details !!"; 
        }

        else if($_GET['type'] == 8)
        {
            $messageType = "Credtit Card Deleted !"; 
        }
        else if($_GET['type'] == 9)
        {
            $messageType = "Bank Account Deleted !"; 
        }
        else if($_GET['type'] == 10)
        {
            $messageType = "Fail"; 
        }

        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>