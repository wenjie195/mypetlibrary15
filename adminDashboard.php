<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';
require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Kitten.php';
require_once dirname(__FILE__) . '/classes/Pets.php';
require_once dirname(__FILE__) . '/classes/Puppy.php';
require_once dirname(__FILE__) . '/classes/Reptile.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$userAmount = getUser($conn," WHERE user_type = 1 ");
// $sellerAmount = getSeller($conn);
$sellerAmount = getSeller($conn," WHERE account_status = 'Active' ");

$kittenAmount = getKitten($conn);
$puppyAmount = getPuppy($conn);
$reptileAmount = getReptile($conn);

// $petAmount = getPetsDetails($conn);
$petAmount = getPetsDetails($conn," WHERE status = 'Available' ");
$pendingPet = getPetsDetails($conn," WHERE status = 'Pending' ");

$articles = getArticles($conn, " WHERE display = 'Pending' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Admin Dashboard | Mypetslibrary" />
<title>Admin Dashboard | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance">
	<h1 class="green-text h1-title">Dashboard</h1>
	<div class="green-border"></div>
    <div class="clear"></div>
    <div class="width100 border-separation">
    	<a href="allUsers.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box">
                <img src="img/pet-sellers.png" alt="Total Users" title="Total Users" class="four-div-img">
                <p class="four-div-p">Total Users</p>

                <?php
                if($userAmount)
                {   
                    $totalUser = count($userAmount);
                }
                else
                {   $totalUser = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalUser;?></b></p>
                <!-- <p class="four-div-amount-p"><b>1000</b></p> -->
            </div>
        </a>
        <a href="seller.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box second-four-div-box left-four-div">
                <img src="img/pet-owner.png" alt="Total Sellers" title="Total Sellers" class="four-div-img">
                <p class="four-div-p">Total Sellers</p>

                <?php
                if($sellerAmount)
                {   
                    $totalSeller = count($sellerAmount);
                }
                else
                {   $totalSeller = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalSeller;?></b></p>

                <!-- <p class="four-div-amount-p"><b>50</b></p> -->
            </div> 
        </a>
        <a href="petSummary.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box right-four-div">
                <img src="img/kitten.png" alt="Total Available Pets" title="Total Available Pets" class="four-div-img">
                <p class="four-div-p">Total Available Pets</p>

                <?php
                if($petAmount)
                {   
                    $totalPets = count($petAmount);
                }
                else
                {   $totalPets = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalPets;?></b></p>
                <!-- <p class="four-div-amount-p"><b>15000</b></p> -->
            </div>  
        </a>
        <a href="allSales.php" class="opacity-hover">       
            <div class="white-dropshadow-box four-div-box second-four-div-box forth-div">
                <img src="img/income.png" alt="Product Sales (RM)" title="Product Sales (RM)" class="four-div-img">
                <p class="four-div-p">Product Sales (RM)</p>
                <p class="four-div-amount-p"><b>3,000,000.00</b></p>
            </div>  
        </a>
        </div>
        <div class="clear"></div>
        <h1 class="green-text h1-title">Pending</h1>
        <div class="green-border"></div>        
        <div class="width100 border-separation">
        <a href="pendingPets.php" class="opacity-hover">       
            <div class="white-dropshadow-box four-div-box">
                <img src="img/pending-pet.png" alt="Pending Pets" title="Pending Pets" class="four-div-img">
                <p class="four-div-p">Pending Pets</p>
                <?php
                if($pendingPet)
                {   
                    $totalPendingPet = count($pendingPet);
                }
                else
                {   $totalPendingPet = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalPendingPet;?></b></p>
                <!-- <p class="four-div-amount-p"><b>40</b></p> -->
            </div> 
        </a>
        <a  href="pendingReview.php"class="opacity-hover">
            <div class="white-dropshadow-box four-div-box second-four-div-box left-four-div">
                <img src="img/review-pet.png" alt="Pending Reviews" title="Pending Reviews" class="four-div-img">
                <p class="four-div-p">Pending Reviews</p>
                <p class="four-div-amount-p"><b>10</b></p>
            </div> 
        </a>
        <a href="pendingArticle.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box right-four-div">
                <img src="img/pet-article.png" alt="Pending Articles" title="Pending Articles" class="four-div-img">
                <p class="four-div-p">Pending Articles</p>

                <?php
                if($articles)
                {   
                    $totalPendingArticles = count($articles);
                }
                else
                {   $totalPendingArticles = 0;   }
                ?>

                <!-- <p class="four-div-amount-p"><b>10</b></p> -->
                <p class="four-div-amount-p"><b><?php echo $totalPendingArticles;?></b></p>
            </div>  
        </a>
        <a href="shippingRequest.php" class="opacity-hover">       
            <div class="white-dropshadow-box four-div-box second-four-div-box forth-div">
                <img src="img/delivery.png" alt="Shipping Requests" title="Shipping Requests" class="four-div-img">
                <p class="four-div-p">Shipping Requests</p>
                <p class="four-div-amount-p"><b>10</b></p>
            </div>  
        </a>        
        
        
        
                      
    </div>    

</div>
<div class="clear"></div>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Password Successfully"; 
        }
        else if($_GET['type'] == 2)
        {
            // $messageType = "Fail to update password !! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>