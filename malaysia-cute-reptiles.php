<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Reptile.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$reptile = getReptile($conn, "WHERE status = 'Available' ORDER BY date_created DESC");
// $puppies = getPuppy($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Reptile On Sale in Malaysia | Mypetslibrary" />
<title>Reptile On Sale in Malaysia | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>

<div class="fix-filter width100 small-padding overflow">
      <h1 class="green-text user-title left-align-title">Reptile</h1>
      <div class="filter-div">
            <button class="transparent-button filter-btn clean opacity-hover"><img src="img/male.png" alt="Male" title="Male" class="filter-icon"></button>
            <button class="transparent-button filter-btn clean opacity-hover"><img src="img/female.png" alt="Female" title="Female" class="filter-icon"></button>
            <button class="transparent-button filter-btn clean opacity-hover"><img src="img/both-gender.png" alt="Both Gender" title="Both Gender" class="filter-icon"></button> 
            <button class="transparent-button filter-btn clean opacity-hover"><img src="img/price-filter.png" alt="Price" title="Price" class="filter-icon"></button>
            <a class="open-filter filter-a green-a">Filter</a>
      </div>
</div>

<div class="clear"></div>    

<div class="width100  small-padding overflow min-height-with-filter filter-distance">
      <div class="width103">

      <?php
      $conn = connDB();
      if($reptile)
      {
        for($cnt = 0;$cnt < count($reptile) ;$cnt++)
        {
        ?>
            <a href='reptileDetails.php?id=<?php echo $reptile[$cnt]->getUid();?>'>
                  <div class="shadow-white-box featured four-box-size">
                        <div class="width100 white-bg">
                        <img src="uploads/<?php echo $reptile[$cnt]->getImageOne();?>" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                        </div>
                        <div class="width100 product-details-div">
                              <p class="width100 text-overflow slider-product-name"><?php echo $reptile[$cnt]->getName();?></p>
                              <p class="slider-product-price">RM<?php echo $reptile[$cnt]->getPrice();?></p>
                              <p class="width100 text-overflow slider-location"><?php echo $reptile[$cnt]->getLocation();?></p>
                        </div>
                  </div>
            </a> 
        <?php
        }
        ?>
      <?php
      }
      $conn->close();
      ?>

      </div>
</div>

<div class="clear"></div>

<style>
	.animated.slideUp{
		animation:none !important;}
	.animated{
		animation:none !important;}
	.puppy-a .hover1a{
		display:none !important;}
	.puppy-a .hover1b{
		display:inline-block !important;}	
</style>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>