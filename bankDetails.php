<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Bank Details | Mypetslibrary" />
<title>Bank Details | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
	<div class="width100 same-padding overflow min-height menu-distance2">
    	<p class="review-product-name">Bank Details</p>
        <!-- <form> -->
        <form method="POST" action="utilities/addBankAccountFunction.php">
        <div class="dual-input">
            <p class="input-top-p">Bank</p>
            <input class="input-name clean" type="text" placeholder="Bank Name" name="bank_name" id="bank_name" required>    
        	<!-- <select class="input-name clean" required >
            	<option>Bank</option>
                <option>Maybank</option>
                <option>CIMB</option>
            </select>      -->
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p">Bank Account Holder Name</p>
        	<input class="input-name clean" type="text" placeholder="Bank Account Holder Name" name="bank_acc_name" id="bank_acc_name" required>      
        </div>        
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p">Bank Account No.</p>
        	<input class="input-name clean" type="text" placeholder="Bank Account No." name="bank_acc_number" id="bank_acc_number" required>         
        </div>
       
        <div class="clear"></div>
       
        <div class="width100 overflow text-center">     
        	<button class="green-button white-text clean2 edit-1-btn margin-auto" name="submit">Submit</button>
        </div>
        </form>
	</div>
<div class="clear"></div>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>