<?php
if (session_id() == "")
{
     session_start();
}
 
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewUser($conn,$uid,$name,$email,$country,$phoneNo,$finalPassword,$salt)
{
     if(insertDynamicData($conn,"user",array("uid","name","email","country","phone_no","password","salt"),
          array($uid,$name,$email,$country,$phoneNo,$finalPassword,$salt),"sssssss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          // promptError("error registering new account.The account already exist");
          // return false;
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $name = rewrite($_POST['register_name']);
     $email = rewrite($_POST['register_email_user']);
     $country = rewrite($_POST['register_country']);
     $phoneNo = rewrite($_POST['register_phone']);
     // $tac = rewrite($_POST['register_tac']);
     $register_password = rewrite($_POST['register_password']);
     $register_password_validation = strlen($register_password);
     $register_retype_password = rewrite($_POST['register_retype_password']);
     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $name."<br>";
     // echo $email."<br>";
     // echo $finalPassword."<br>";
     // echo $salt."<br>";
     // echo $phoneNo ."<br>";
     // echo $tac."<br>";

     if($register_password == $register_retype_password)
     {
          if($register_password_validation >= 6)
          {
               $usernameRows = getUser($conn," WHERE name = ? ",array("name"),array($_POST['register_name']),"s");
               $usernameDetails = $usernameRows[0];

               $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($_POST['register_email_user']),"s");
               $userEmailDetails = $userEmailRows[0];

               $userPhoneRows = getUser($conn," WHERE phone_no = ? ",array("phone_no"),array($_POST['register_phone']),"s");
               $userPhoneDetails = $userPhoneRows[0];

               if(!$usernameDetails && !$userEmailDetails && !$userPhoneDetails)
               {
                    if(registerNewUser($conn,$uid,$name,$email,$country,$phoneNo,$finalPassword,$salt))
                    {
                         $_SESSION['messageType'] = 1;
                         header('Location: ../index.php?type=1');
                         // echo "done register";   
                         // $_SESSION['uid'] = $uid;
                         // echo "<script>alert('Register Successfully!');window.location='../index.php'</script>";
                    }
               }
               else
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../index.php?type=5');
               }
          }
          else 
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../index.php?type=3');
          }
     }
     else 
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../index.php?type=4');
     }      
}
else 
{
     header('Location: ../index.php');
}
?>