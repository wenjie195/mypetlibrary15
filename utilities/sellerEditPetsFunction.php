<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sellerAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Kitten.php';
require_once dirname(__FILE__) . '/../classes/Pets.php';
require_once dirname(__FILE__) . '/../classes/Puppy.php';
require_once dirname(__FILE__) . '/../classes/Reptile.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = $_POST["pet_uid"];
    $type = $_POST["pet_type"];

    $name = rewrite($_POST['update_name']);
    $sku = rewrite($_POST['update_sku']);
    $slug = rewrite($_POST['update_slug']);
    $price = rewrite($_POST['update_price']);
    $age= rewrite($_POST['update_age']);
    $vaccinated = rewrite($_POST['update_vaccinated']);
    $dewormed = rewrite($_POST['update_dewormed']);
    $gender = rewrite($_POST['update_gender']);
    $color = rewrite($_POST['update_color']);
    $size = rewrite($_POST['update_size']);
    $status = rewrite($_POST['update_status']);
    $feature = rewrite($_POST['update_feature']);
    $breed = rewrite($_POST['update_breed']);
    $seller = rewrite($_POST['update_seller']);
    $details = rewrite($_POST['update_details']);
    $link = rewrite($_POST['update_link']);

    //   FOR DEBUGGING 
    //  echo "<br>";
    //  echo $name."<br>";

    if(isset($_POST['update']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($name)
        {
            array_push($tableName,"name");
            array_push($tableValue,$name);
            $stringType .=  "s";
        }
        if($sku)
        {
            array_push($tableName,"sku");
            array_push($tableValue,$sku);
            $stringType .=  "s";
        }
        if($slug)
        {
            array_push($tableName,"slug");
            array_push($tableValue,$slug);
            $stringType .=  "s";
        }
        if($price)
        {
            array_push($tableName,"price");
            array_push($tableValue,$price);
            $stringType .=  "s";
        }
        if($age)
        {
            array_push($tableName,"age");
            array_push($tableValue,$age);
            $stringType .=  "s";
        }
        if($vaccinated)
        {
            array_push($tableName,"vaccinated");
            array_push($tableValue,$vaccinated);
            $stringType .=  "s";
        }
        if($dewormed)
        {
            array_push($tableName,"dewormed");
            array_push($tableValue,$dewormed);
            $stringType .=  "s";
        }
        if($gender)
        {
            array_push($tableName,"gender");
            array_push($tableValue,$gender);
            $stringType .=  "s";
        }
        if($color)
        {
            array_push($tableName,"color");
            array_push($tableValue,$color);
            $stringType .=  "s";
        }
        if($size)
        {
            array_push($tableName,"size");
            array_push($tableValue,$size);
            $stringType .=  "s";
        }
        if($status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        if($feature)
        {
            array_push($tableName,"feature");
            array_push($tableValue,$feature);
            $stringType .=  "s";
        }
        if($breed)
        {
            array_push($tableName,"breed");
            array_push($tableValue,$breed);
            $stringType .=  "s";
        }
        if($seller)
        {
            array_push($tableName,"seller");
            array_push($tableValue,$seller);
            $stringType .=  "s";
        }
        if($details)
        {
            array_push($tableName,"details");
            array_push($tableValue,$details);
            $stringType .=  "s";
        }
        if($link)
        {
            array_push($tableName,"link");
            array_push($tableValue,$link);
            $stringType .=  "s";
        }
        array_push($tableValue,$uid);
        $stringType .=  "s";
        $updatePetDetails = updateDynamicData($conn,"pet_details"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($updatePetDetails)
        {
            // echo "data updated";
            // echo "<br>";

            if($type == 'Kitten')
            {

                if(isset($_POST['update']))
                {   
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
                    if($name)
                    {
                        array_push($tableName,"name");
                        array_push($tableValue,$name);
                        $stringType .=  "s";
                    }
                    if($sku)
                    {
                        array_push($tableName,"sku");
                        array_push($tableValue,$sku);
                        $stringType .=  "s";
                    }
                    if($slug)
                    {
                        array_push($tableName,"slug");
                        array_push($tableValue,$slug);
                        $stringType .=  "s";
                    }
                    if($price)
                    {
                        array_push($tableName,"price");
                        array_push($tableValue,$price);
                        $stringType .=  "s";
                    }
                    if($age)
                    {
                        array_push($tableName,"age");
                        array_push($tableValue,$age);
                        $stringType .=  "s";
                    }
                    if($vaccinated)
                    {
                        array_push($tableName,"vaccinated");
                        array_push($tableValue,$vaccinated);
                        $stringType .=  "s";
                    }
                    if($dewormed)
                    {
                        array_push($tableName,"dewormed");
                        array_push($tableValue,$dewormed);
                        $stringType .=  "s";
                    }
                    if($gender)
                    {
                        array_push($tableName,"gender");
                        array_push($tableValue,$gender);
                        $stringType .=  "s";
                    }
                    if($color)
                    {
                        array_push($tableName,"color");
                        array_push($tableValue,$color);
                        $stringType .=  "s";
                    }
                    if($size)
                    {
                        array_push($tableName,"size");
                        array_push($tableValue,$size);
                        $stringType .=  "s";
                    }
                    if($status)
                    {
                        array_push($tableName,"status");
                        array_push($tableValue,$status);
                        $stringType .=  "s";
                    }
                    if($feature)
                    {
                        array_push($tableName,"feature");
                        array_push($tableValue,$feature);
                        $stringType .=  "s";
                    }
                    if($breed)
                    {
                        array_push($tableName,"breed");
                        array_push($tableValue,$breed);
                        $stringType .=  "s";
                    }
                    if($seller)
                    {
                        array_push($tableName,"seller");
                        array_push($tableValue,$seller);
                        $stringType .=  "s";
                    }
                    if($details)
                    {
                        array_push($tableName,"details");
                        array_push($tableValue,$details);
                        $stringType .=  "s";
                    }
                    if($link)
                    {
                        array_push($tableName,"link");
                        array_push($tableValue,$link);
                        $stringType .=  "s";
                    }
                    array_push($tableValue,$uid);
                    $stringType .=  "s";
                    $updateKittenDetails = updateDynamicData($conn,"kitten"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($updateKittenDetails)
                    {
                        // echo "data updated";            
                        $_SESSION['messageType'] = 1;
                        header('Location: ../sellerAllPets.php?type=1');
                    }
                    else
                    {
                        $_SESSION['messageType'] = 1;
                        header('Location: ../editBankDetails.php?type=2');
                    }
                }
                else
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../editBankDetails.php?type=3');
                }

            }
            elseif($type == 'Puppy')
            {

                if(isset($_POST['update']))
                {   
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
                    if($name)
                    {
                        array_push($tableName,"name");
                        array_push($tableValue,$name);
                        $stringType .=  "s";
                    }
                    if($sku)
                    {
                        array_push($tableName,"sku");
                        array_push($tableValue,$sku);
                        $stringType .=  "s";
                    }
                    if($slug)
                    {
                        array_push($tableName,"slug");
                        array_push($tableValue,$slug);
                        $stringType .=  "s";
                    }
                    if($price)
                    {
                        array_push($tableName,"price");
                        array_push($tableValue,$price);
                        $stringType .=  "s";
                    }
                    if($age)
                    {
                        array_push($tableName,"age");
                        array_push($tableValue,$age);
                        $stringType .=  "s";
                    }
                    if($vaccinated)
                    {
                        array_push($tableName,"vaccinated");
                        array_push($tableValue,$vaccinated);
                        $stringType .=  "s";
                    }
                    if($dewormed)
                    {
                        array_push($tableName,"dewormed");
                        array_push($tableValue,$dewormed);
                        $stringType .=  "s";
                    }
                    if($gender)
                    {
                        array_push($tableName,"gender");
                        array_push($tableValue,$gender);
                        $stringType .=  "s";
                    }
                    if($color)
                    {
                        array_push($tableName,"color");
                        array_push($tableValue,$color);
                        $stringType .=  "s";
                    }
                    if($size)
                    {
                        array_push($tableName,"size");
                        array_push($tableValue,$size);
                        $stringType .=  "s";
                    }
                    if($status)
                    {
                        array_push($tableName,"status");
                        array_push($tableValue,$status);
                        $stringType .=  "s";
                    }
                    if($feature)
                    {
                        array_push($tableName,"feature");
                        array_push($tableValue,$feature);
                        $stringType .=  "s";
                    }
                    if($breed)
                    {
                        array_push($tableName,"breed");
                        array_push($tableValue,$breed);
                        $stringType .=  "s";
                    }
                    if($seller)
                    {
                        array_push($tableName,"seller");
                        array_push($tableValue,$seller);
                        $stringType .=  "s";
                    }
                    if($details)
                    {
                        array_push($tableName,"details");
                        array_push($tableValue,$details);
                        $stringType .=  "s";
                    }
                    if($link)
                    {
                        array_push($tableName,"link");
                        array_push($tableValue,$link);
                        $stringType .=  "s";
                    }
                    array_push($tableValue,$uid);
                    $stringType .=  "s";
                    $updatePuppyDetails = updateDynamicData($conn,"puppy"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($updatePuppyDetails)
                    {
                        // echo "data updated";          
                        $_SESSION['messageType'] = 1;
                        header('Location: ../sellerAllPets.php?type=4');  
                    }
                    else
                    {
                        $_SESSION['messageType'] = 1;
                        header('Location: ../sellerAllPets.php?type=2');
                    }
                }
                else
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../sellerAllPets.php?type=3');
                }

            }
            elseif($type == 'Reptile')
            {

                if(isset($_POST['update']))
                {   
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
                    if($name)
                    {
                        array_push($tableName,"name");
                        array_push($tableValue,$name);
                        $stringType .=  "s";
                    }
                    if($sku)
                    {
                        array_push($tableName,"sku");
                        array_push($tableValue,$sku);
                        $stringType .=  "s";
                    }
                    if($slug)
                    {
                        array_push($tableName,"slug");
                        array_push($tableValue,$slug);
                        $stringType .=  "s";
                    }
                    if($price)
                    {
                        array_push($tableName,"price");
                        array_push($tableValue,$price);
                        $stringType .=  "s";
                    }
                    if($age)
                    {
                        array_push($tableName,"age");
                        array_push($tableValue,$age);
                        $stringType .=  "s";
                    }
                    if($vaccinated)
                    {
                        array_push($tableName,"vaccinated");
                        array_push($tableValue,$vaccinated);
                        $stringType .=  "s";
                    }
                    if($dewormed)
                    {
                        array_push($tableName,"dewormed");
                        array_push($tableValue,$dewormed);
                        $stringType .=  "s";
                    }
                    if($gender)
                    {
                        array_push($tableName,"gender");
                        array_push($tableValue,$gender);
                        $stringType .=  "s";
                    }
                    if($color)
                    {
                        array_push($tableName,"color");
                        array_push($tableValue,$color);
                        $stringType .=  "s";
                    }
                    if($size)
                    {
                        array_push($tableName,"size");
                        array_push($tableValue,$size);
                        $stringType .=  "s";
                    }
                    if($status)
                    {
                        array_push($tableName,"status");
                        array_push($tableValue,$status);
                        $stringType .=  "s";
                    }
                    if($feature)
                    {
                        array_push($tableName,"feature");
                        array_push($tableValue,$feature);
                        $stringType .=  "s";
                    }
                    if($breed)
                    {
                        array_push($tableName,"breed");
                        array_push($tableValue,$breed);
                        $stringType .=  "s";
                    }
                    if($seller)
                    {
                        array_push($tableName,"seller");
                        array_push($tableValue,$seller);
                        $stringType .=  "s";
                    }
                    if($details)
                    {
                        array_push($tableName,"details");
                        array_push($tableValue,$details);
                        $stringType .=  "s";
                    }
                    if($link)
                    {
                        array_push($tableName,"link");
                        array_push($tableValue,$link);
                        $stringType .=  "s";
                    }
                    array_push($tableValue,$uid);
                    $stringType .=  "s";
                    $updateReptileDetails = updateDynamicData($conn,"reptile"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($updateReptileDetails)
                    {           
                        $_SESSION['messageType'] = 1;
                        header('Location: ../sellerAllPets.php?type=5');
                    }
                    else
                    {
                        $_SESSION['messageType'] = 1;
                        header('Location: ../sellerAllPets.php?type=2');
                    }
                }
                else
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../sellerAllPets.php?type=3');
                }

            }

        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../sellerAllPets.php?type=6');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../sellerAllPets.php?type=7');
    }
}
else
{
    header('Location: ../index.php');
}
?>