<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/product.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $id = $_POST["id"];

     $name = rewrite($_POST['update_name']);
     $sku = rewrite($_POST['update_sku']);
     $slug = rewrite($_POST['update_slug']);
     $price = rewrite($_POST['update_price']);
     $category= rewrite($_POST['update_category']);
     $brand = rewrite($_POST['update_brand']);
     $type = rewrite($_POST['update_animal_type']);
     $stock = rewrite($_POST['update_stock']);
     $expiry = rewrite($_POST['update_expiry_date']);
     $status = rewrite($_POST['update_status']);
     $description = rewrite($_POST['update_description']);
     //$link = rewrite($_POST['update_link']);

     $link = rewrite($_POST['register_link']);
     if($link != '')
     {
          $tubeOne = "Yes";
     }
     else
     {
          $tubeOne = "No";
     }

     $imageOne = $_FILES['image_one']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imageOne);
     }

     $imageTwo = $_FILES['image_two']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_two"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_two']['tmp_name'],$target_dir.$imageTwo);
     }

     $imageThree = $_FILES['image_three']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_three"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_three']['tmp_name'],$target_dir.$imageThree);
     }

     $imageFour = $_FILES['image_four']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_four"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_four']['tmp_name'],$target_dir.$imageFour);
     }

     $imageFive = $_FILES['image_five']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_five"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_five']['tmp_name'],$target_dir.$imageFive);
     }

     //   FOR DEBUGGING 
    //  echo "<br>";
    //  echo $name."<br>";
    //  echo $sku."<br>";
    //  echo $slug."<br>";
    //  echo $price."<br>";
    //  echo $category."<br>";
    //  echo $brand ."<br>";
    //  echo $type."<br>";
    //  echo $stock."<br>";
    //  echo $expiry."<br>";
    //  echo $status."<br>";
    //  echo $description."<br>";
    //  echo $link."<br>";
}

if(isset($_POST['editSubmit']))
{   
    $tableName = array();
    $tableValue =  array();
    $stringType =  "";

    //echo "save to database";
    if($name)
    {
        array_push($tableName,"name");
        array_push($tableValue,$name);
        $stringType .=  "s";
    }

    if($sku)
    {
        array_push($tableName,"sku");
        array_push($tableValue,$sku);
        $stringType .=  "s";
    }

    if($slug)
    {
        array_push($tableName,"slug");
        array_push($tableValue,$slug);
        $stringType .=  "s";
    }

    if($price)
    {
        array_push($tableName,"price");
        array_push($tableValue,$price);
        $stringType .=  "s";
    }

    if($category)
    {
        array_push($tableName,"category");
        array_push($tableValue,$category);
        $stringType .=  "s";
    }

    if($brand)
    {
        array_push($tableName,"brand");
        array_push($tableValue,$brand);
        $stringType .=  "s";
    }

    if($type)
    {
        array_push($tableName,"animal_type");
        array_push($tableValue,$type);
        $stringType .=  "s";
    }

    if($stock)
    {
        array_push($tableName,"stock");
        array_push($tableValue,$stock);
        $stringType .=  "s";
    }

    if($expiry)
    {
        array_push($tableName,"expiry_date");
        array_push($tableValue,$expiry);
        $stringType .=  "s";
    }

    if($status)
    {
        array_push($tableName,"status");
        array_push($tableValue,$status);
        $stringType .=  "s";
    }

    if($description)
    {
        array_push($tableName,"description");
        array_push($tableValue,$description);
        $stringType .=  "s";
    }

    if($link)
    {
        array_push($tableName,"link");
        array_push($tableValue,$link);
        $stringType .=  "s";
    }

    if($imageOne)
    {
        array_push($tableName,"image_one");
        array_push($tableValue,$imageOne);
        $stringType .=  "s";
    }

    if($imageTwo)
    {
        array_push($tableName,"image_two");
        array_push($tableValue,$imageTwo);
        $stringType .=  "s";
    }

    if($imageThree)
    {
        array_push($tableName,"image_three");
        array_push($tableValue,$imageThree);
        $stringType .=  "s";
    }

    if($imageFour)
    {
        array_push($tableName,"image_four");
        array_push($tableValue,$imageFour);
        $stringType .=  "s";
    }

    if($imageFive)
    {
        array_push($tableName,"image_five");
        array_push($tableValue,$imageFive);
        $stringType .=  "s";
    }

    array_push($tableValue,$id);
    $stringType .=  "s";
    $updateProductDetails = updateDynamicData($conn,"product"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        
    if($updateProductDetails)
    {
        $_SESSION['messageType'] = 1;
        echo "<script>alert('Data Updated and Stored !');window.location='../allProducts.php'</script>"; 
    }
    else
    {
        $_SESSION['messageType'] = 1;
        echo "<script>alert('Fail to Update Data !');window.location='../allProducts.php'</script>"; 
    }
}
else
{
    header('Location: ../index.php');
    // $_SESSION['messageType'] = 1;
    // header('Location: ../editProfile.php?type=1');
}

?>