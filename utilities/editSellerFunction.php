<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Seller.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $id = $_POST["id"];

    $name = rewrite($_POST['update_name']);
    $slug = rewrite($_POST['update_slug']);
    $reg = rewrite($_POST['update_reg']);
    $contact = rewrite($_POST['update_contact']);
    $address = rewrite($_POST['update_address']);
    $state = rewrite($_POST['update_state']);
    $status = rewrite($_POST['update_status']);
    $contactPerson = rewrite($_POST['update_contact_person']);
    $contactPersonNo = rewrite($_POST['update_contact_personNo']);
    $experience = rewrite($_POST['update_experience']);
    $cert = rewrite($_POST['update_cert']);
    // $services = rewrite($_POST['update_servives']);
    $breed = rewrite($_POST['update_breed']);
    $info = rewrite($_POST['update_info']);

    //   FOR DEBUGGING 
    //  echo "<br>";
    //  echo $name."<br>";
    //  echo $slug."<br>";
    //  echo $reg."<br>";
    //  echo $contact."<br>";
    //  echo $address."<br>";
    //  echo $state."<br>";
    //  echo $status."<br>";
    //  echo $contactPerson."<br>";
    //  echo $contactPersonNo."<br>";
    //  echo $experience."<br>";
    //  echo $cert."<br>";
    //  echo $services."<br>";
    //  echo $breed."<br>";
    //  echo $info."<br>";
}

if(isset($_POST['editSubmit']))
{   
    $tableName = array();
    $tableValue =  array();
    $stringType =  "";

    //echo "save to database";
    if($name)
    {
        array_push($tableName,"company_name");
        array_push($tableValue,$name);
        $stringType .=  "s";
    }

    if($slug)
    {
        array_push($tableName,"slug");
        array_push($tableValue,$slug);
        $stringType .=  "s";
    }

    if($reg)
    {
        array_push($tableName,"registration_no");
        array_push($tableValue,$reg);
        $stringType .=  "s";
    }

    if($contact)
    {
        array_push($tableName,"contact_no");
        array_push($tableValue,$contact);
        $stringType .=  "s";
    }

    if($address)
    {
        array_push($tableName,"address");
        array_push($tableValue,$address);
        $stringType .=  "s";
    }

    if($state)
    {
        array_push($tableName,"state");
        array_push($tableValue,$state);
        $stringType .=  "s";
    }

    if($status)
    {
        array_push($tableName,"account_status");
        array_push($tableValue,$status);
        $stringType .=  "s";
    }

    if($contactPerson)
    {
        array_push($tableName,"contact_person");
        array_push($tableValue,$contactPerson);
        $stringType .=  "s";
    }

    if($contactPersonNo)
    {
        array_push($tableName,"contact_person_no");
        array_push($tableValue,$contactPersonNo);
        $stringType .=  "s";
    }

    if($experience)
    {
        array_push($tableName,"experience");
        array_push($tableValue,$experience);
        $stringType .=  "s";
    }

    if($cert)
    {
        array_push($tableName,"cert");
        array_push($tableValue,$cert);
        $stringType .=  "s";
    }

    // if($services)
    // {
    //     array_push($tableName,"services");
    //     array_push($tableValue,$services);
    //     $stringType .=  "s";
    // }

    if($breed)
    {
        array_push($tableName,"breed_type");
        array_push($tableValue,$breed);
        $stringType .=  "s";
    }

    if($info)
    {
        array_push($tableName,"other_info");
        array_push($tableValue,$info);
        $stringType .=  "s";
    }

    array_push($tableValue,$id);
    $stringType .=  "s";
    $updateSellerDetails = updateDynamicData($conn,"seller"," WHERE id = ? ",$tableName,$tableValue,$stringType);

    if($updateSellerDetails)
    {
        $_SESSION['messageType'] = 1;
        echo "<script>alert('Data Updated and Stored !');window.location='../seller.php'</script>"; 
    }
    else
    {
        $_SESSION['messageType'] = 1;
        echo "<script>alert('Fail to Update Data !');window.location='../seller.php'</script>"; 
    }
}
else
{
    header('Location: ../index.php');
    // $_SESSION['messageType'] = 1;
    // header('Location: ../editProfile.php?type=1');
}

?>