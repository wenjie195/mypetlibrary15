<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Article.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST["article_uid"]);
    $display = "Delete";
    $type = "Delete";
    $result = "Delete";

    // //   FOR DEBUGGING
    // echo "<br>";
    // echo $bankNumber."<br>";
    // echo $bankUser."<br>";

    if(isset($_POST['article_uid']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($display)
        {
            array_push($tableName,"display");
            array_push($tableValue,$display);
            $stringType .=  "s";
        }    
        if($type)
        {
            array_push($tableName,"type");
            array_push($tableValue,$type);
            $stringType .=  "s";
        }  

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $updateReport = updateDynamicData($conn,"articles"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($updateReport)
        {
            // echo "success";

            if(isset($_POST['article_uid']))
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($result)
                {
                    array_push($tableName,"result");
                    array_push($tableValue,$result);
                    $stringType .=  "s";
                }            
                array_push($tableValue,$uid);
                $stringType .=  "s";
                $updateReport = updateDynamicData($conn,"reported_article"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($updateReport)
                {
                    // echo "success";
                    $_SESSION['messageType'] = 1;
                    header('Location: ../reportedArticle.php?type=2');
                }
                else
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../reportedArticle.php?type=3');
                }
            }
            else
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../reportedArticle.php?type=4');
            }

        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../reportedArticle.php?type=5');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../reportedArticle.php?type=6');
    }
    
}
else
{
     header('Location: ../index.php');
}
?>