<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Puppy.php';
require_once dirname(__FILE__) . '/../classes/Pets.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewPuppy($conn,$uid,$name,$sku,$slug,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,
                                   $breed,$seller,$details,$link,$imageOne,$imageTwo,$imageThree,$imageFour)
{
     if(insertDynamicData($conn,"puppy",array("uid","name","sku","slug","price","age","vaccinated","dewormed","gender","color","size",
     "status","feature","breed","seller","details","link","image_one","image_two","image_three","image_four"),
          array($uid,$name,$sku,$slug,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,
          $breed,$seller,$details,$link,$imageOne,$imageTwo,$imageThree,$imageFour),"sssssssssssssssssssss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

function registerNewPets($conn,$uid,$name,$sku,$slug,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,$breed,$seller,$type,
                              $details,$link,$imageOne,$imageTwo,$imageThree,$imageFour)
{
     if(insertDynamicData($conn,"pet_details",array("uid","name","sku","slug","price","age","vaccinated","dewormed","gender","color","size","status","feature","breed","seller",
                                   "type","details","link","image_one","image_two","image_three","image_four"),
     array($uid,$name,$sku,$slug,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,$breed,$seller,$type,
               $details,$link,$imageOne,$imageTwo,$imageThree,$imageFour),"ssssssssssssssssssssss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $name = rewrite($_POST['register_name']);
     $sku = rewrite($_POST['register_sku']);
     $slug = rewrite($_POST['register_slug']);
     $price = rewrite($_POST['register_price']);
     $age= rewrite($_POST['register_age']);
     $vaccinated = rewrite($_POST['register_vaccinated']);
     $dewormed = rewrite($_POST['register_dewormed']);
     $gender = rewrite($_POST['register_gender']);
     $color = rewrite($_POST['register_color']);
     $size = rewrite($_POST['register_size']);

     $status = "Available";

     $feature = rewrite($_POST['register_feature']);
     $breed = rewrite($_POST['register_breed']);
     $seller = rewrite($_POST['register_seller']);
     $details = rewrite($_POST['register_details']);

     $type = "Puppy";

     $link = rewrite($_POST['register_link']);

     $imageOne = $uid.$_FILES['image_one']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imageOne);
     }

     $imageTwo = $uid.$_FILES['image_two']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_two"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_two']['tmp_name'],$target_dir.$imageTwo);
     }

     $imageThree = $uid.$_FILES['image_three']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_three"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_three']['tmp_name'],$target_dir.$imageThree);
     }

     $imageFour = $uid.$_FILES['image_four']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_four"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_four']['tmp_name'],$target_dir.$imageFour);
     }

     // // FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $name."<br>";
     // echo $sku."<br>";
     // echo $slug."<br>";
     // echo $price."<br>";
     // echo $age."<br>";
     // echo $vaccinated ."<br>";
     // echo $dewormed."<br>";
     // echo $gender."<br>";
     // echo $color."<br>";
     // echo $size."<br>";
     // echo $status."<br>";
     // echo $feature."<br>";
     // echo $breed."<br>";
     // echo $seller."<br>";
     // echo $details."<br>";
     // echo $link."<br>";

     $puppyNameDetails = getPuppy($conn," WHERE name = ? ",array("name"),array($_POST['register_name']),"s");
     $registeredPuppyName = $puppyNameDetails[0];

     $puppySKUDetails = getPuppy($conn," WHERE sku = ? ",array("sku"),array($_POST['register_sku']),"s");
     $registeredPuppySKU = $puppySKUDetails[0];

     if(!$registeredPuppyName && !$registeredPuppySKU)
     // if($name && $sku)
     {
          if(registerNewPuppy($conn,$uid,$name,$sku,$slug,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,
          $breed,$seller,$details,$link,$imageOne,$imageTwo,$imageThree,$imageFour))
          {
               // $_SESSION['messageType'] = 1;
               // header('Location: ../addPuppy.php?type=1');

               if(registerNewPets($conn,$uid,$name,$sku,$slug,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,$breed,$seller,$type,
                                        $details,$link,$imageOne,$imageTwo,$imageThree,$imageFour))
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../addPuppy.php?type=1');
               }
               else
               { 
                    $_SESSION['messageType'] = 1;
                    header('Location: ../addPuppy.php?type=2');
               } 

          }
          else
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../addPuppy.php?type=3');
          }  
     }
     else
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../addPuppy.php?type=4');
     }     
}
else 
{
     header('Location: ../index.php');
}
?>