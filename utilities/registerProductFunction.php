<?php

if (session_id() == ""){
     session_start();
 }
 
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
//require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Product.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewProduct($conn,$uid,$name,$sku,$slug,$animalType,$category,$brand,$expiryDate,$status,$description,$link,$imageOne,
$imageTwo,$imageThree,$imageFour)
{
     if(insertDynamicData($conn,"product",array("uid","name","sku","slug","animal_type","category","brand","expiry_date","status","description",
     "link","image_one","image_two","image_three","image_four"),
          array($uid,$name,$sku,$slug,$animalType,$category,$brand,$expiryDate,$status,$description,$link,$imageOne,$imageTwo,
          $imageThree,$imageFour),"sssssssssssssss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

function registerNewProductV1($conn,$uid,$name,$sku,$slug,$animalType,$category,$brand,$expiryDate,$status,$description,$link,$imageOne,
$imageTwo,$imageThree,$imageFour,$variationOne,$variationOnePrice,$variationOneStock,$variationOneImage)
{
     if(insertDynamicData($conn,"product",array("uid","name","sku","slug","animal_type","category","brand","expiry_date","status","description",
     "link","image_one","image_two","image_three","image_four","variation_one","variation_one_price","variation_one_stock","variation_one_image"),
          array($uid,$name,$sku,$slug,$animalType,$category,$brand,$expiryDate,$status,$description,$link,$imageOne,$imageTwo,
          $imageThree,$imageFour,$variationOne,$variationOnePrice,$variationOneStock,$variationOneImage),"sssssssssssssssssss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

function registerNewProductV2($conn,$uid,$name,$sku,$slug,$animalType,$category,$brand,$expiryDate,$status,$description,$link,$imageOne,
$imageTwo,$imageThree,$imageFour,$variationOne,$variationOnePrice,$variationOneStock,$variationOneImage,$variationTwo,$variationTwoPrice,
$variationTwoStock,$variationTwoImage)
{
     if(insertDynamicData($conn,"product",array("uid","name","sku","slug","animal_type","category","brand","expiry_date","status","description",
     "link","image_one","image_two","image_three","image_four","variation_one","variation_one_price","variation_one_stock","variation_one_image",
     "variation_two","variation_two_price","variation_two_stock","variation_two_image"),
          array($uid,$name,$sku,$slug,$animalType,$category,$brand,$expiryDate,$status,$description,$link,$imageOne,$imageTwo,$imageThree,
          $imageFour,$variationOne,$variationOnePrice,$variationOneStock,$variationOneImage,$variationTwo,$variationTwoPrice,
          $variationTwoStock,$variationTwoImage),"sssssssssssssssssssssss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

function registerNewProductV3($conn,$uid,$name,$sku,$slug,$animalType,$category,$brand,$expiryDate,$status,$description,$link,$imageOne,
$imageTwo,$imageThree,$imageFour,$variationOne,$variationOnePrice,$variationOneStock,$variationOneImage,$variationTwo,$variationTwoPrice,
$variationTwoStock,$variationTwoImage,$variationThree,$variationThreePrice,$variationThreeStock,$variationThreeImage)
{
     if(insertDynamicData($conn,"product",array("uid","name","sku","slug","animal_type","category","brand","expiry_date","status","description",
     "link","image_one","image_two","image_three","image_four","variation_one","variation_one_price","variation_one_stock","variation_one_image",
     "variation_two","variation_two_price","variation_two_stock","variation_two_image","variation_three","variation_three_price",
     "variation_three_stock","variation_three_image"),
          array($uid,$name,$sku,$slug,$animalType,$category,$brand,$expiryDate,$status,$description,$link,$imageOne,
          $imageTwo,$imageThree,$imageFour,$variationOne,$variationOnePrice,$variationOneStock,$variationOneImage,$variationTwo,
          $variationTwoPrice,$variationTwoStock,$variationTwoImage,$variationThree,$variationThreePrice,$variationThreeStock,
          $variationThreeImage),"sssssssssssssssssssssssssss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

function registerNewProductV4($conn,$uid,$name,$sku,$slug,$animalType,$category,$brand,$expiryDate,$status,$description,$link,$imageOne,
$imageTwo,$imageThree,$imageFour,$variationOne,$variationOnePrice,$variationOneStock,$variationOneImage,$variationTwo,$variationTwoPrice,
$variationTwoStock,$variationTwoImage,$variationThree,$variationThreePrice,$variationThreeStock,$variationThreeImage,$variationFour,
$variationFourPrice,$variationFourStock,$variationFourImage)
{
     if(insertDynamicData($conn,"product",array("uid","name","sku","slug","animal_type","category","brand","expiry_date","status","description",
     "link","image_one","image_two","image_three","image_four","variation_one","variation_one_price","variation_one_stock","variation_one_image",
     "variation_two","variation_two_price","variation_two_stock","variation_two_image","variation_three","variation_three_price",
     "variation_three_stock","variation_three_image","variation_four","variation_four_price","variation_four_stock","variation_four_image"),
          array($uid,$name,$sku,$slug,$animalType,$category,$brand,$expiryDate,$status,$description,$link,$imageOne,
          $imageTwo,$imageThree,$imageFour,$variationOne,$variationOnePrice,$variationOneStock,$variationOneImage,$variationTwo,$variationTwoPrice,
          $variationTwoStock,$variationTwoImage,$variationThree,$variationThreePrice,$variationThreeStock,$variationThreeImage,$variationFour,
          $variationFourPrice,$variationFourStock,$variationFourImage),"sssssssssssssssssssssssssssssss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $name = rewrite($_POST['register_name']);
     $sku = rewrite($_POST['register_sku']);
     $slug = rewrite($_POST['register_slug']);
     $animalType = rewrite($_POST['register_animal_type']);
     $category= rewrite($_POST['register_category']);
     $brand = rewrite($_POST['register_brand']);
     $expiryDate = rewrite($_POST['register_expiry_date']);
     $status = rewrite($_POST['register_status']);
     $description = rewrite($_POST['register_description']);
     //$link = rewrite($_POST['register_link']);

     $link = rewrite($_POST['register_link']);
     // if($link != '')
     // {
     //      $tubeOne = "Yes";
     // }
     // else
     // {
     //      $tubeOne = "No";
     // }

     $imageOne = $_FILES['image_one']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imageOne);
     }

     $imageTwo = $_FILES['image_two']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_two"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_two']['tmp_name'],$target_dir.$imageTwo);
     }

     $imageThree = $_FILES['image_three']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_three"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_three']['tmp_name'],$target_dir.$imageThree);
     }

     $imageFour = $_FILES['image_four']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["image_four"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_four']['tmp_name'],$target_dir.$imageFour);
     }

     $variationOne = rewrite($_POST['register_variation_one']);
     $variationOnePrice = rewrite($_POST['register_variation_one_price']);
     $variationOneStock = rewrite($_POST['register_variation_one_stock']);

     $variationOneImage = $_FILES['register_variation_one_image']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["register_variation_one_image"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['register_variation_one_image']['tmp_name'],$target_dir.$variationOneImage);
     }

     $variationTwo = rewrite($_POST['register_variation_two']);
     $variationTwoPrice = rewrite($_POST['register_variation_two_price']);
     $variationTwoStock = rewrite($_POST['register_variation_two_stock']);

     $variationTwoImage = $_FILES['register_variation_two_image']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["register_variation_two_image"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['register_variation_two_image']['tmp_name'],$target_dir.$variationTwoImage);
     }

     $variationThree = rewrite($_POST['register_variation_three']);
     $variationThreePrice = rewrite($_POST['register_variation_three_price']);
     $variationThreeStock = rewrite($_POST['register_variation_three_stock']);

     $variationThreeImage = $_FILES['register_variation_three_image']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["register_variation_three_image"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['register_variation_three_image']['tmp_name'],$target_dir.$variationThreeImage);
     }

     $variationFour = rewrite($_POST['register_variation_four']);
     $variationFourPrice = rewrite($_POST['register_variation_four_price']);
     $variationFourStock = rewrite($_POST['register_variation_four_stock']);

     $variationFourImage = $_FILES['register_variation_four_image']['name'];
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["register_variation_four_image"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['register_variation_four_image']['tmp_name'],$target_dir.$variationFourImage);
     }

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $name."<br>";
     // echo $sku."<br>";
     // echo $slug."<br>";
     // echo $animalType."<br>";
     // echo $category."<br>";
     // echo $brand ."<br>";
     // echo $expiryDate."<br>";
     // echo $status."<br>";
     // echo $description."<br>";
     // echo $link."<br>";
     // echo $imageOne."<br>";
     // echo $imageTwo."<br>";
     // echo $imageThree."<br>";
     // echo $imageFour."<br>";
     // echo $variationOne."<br>";
     // echo $variationOnePrice."<br>";
     // echo $variationOneStock."<br>";
     // echo $variationOneImage."<br>";
     // echo $variationTwo."<br>";
     // echo $variationTwoPrice."<br>";
     // echo $variationTwoStock."<br>";
     // echo $variationTwoImage."<br>";
     // echo $variationThree."<br>";
     // echo $variationThreePrice."<br>";
     // echo $variationThreeStock."<br>";
     // echo $variationThreeImage."<br>";
     // echo $variationFour."<br>";
     // echo $variationFourPrice."<br>";
     // echo $variationFourStock."<br>";
     // echo $variationFourImage."<br>";

     if($variationFour && $variationThree && $variationTwo && $variationOne){
          if(registerNewProductV4($conn,$uid,$name,$sku,$slug,$animalType,$category,$brand,$expiryDate,$status,$description,
               $link,$imageOne,$imageTwo,$imageThree,$imageFour,$variationOne,$variationOnePrice,$variationOneStock,$variationOneImage,$variationTwo,
               $variationTwoPrice,$variationTwoStock,$variationTwoImage,$variationThree,$variationThreePrice,$variationThreeStock,
               $variationThreeImage,$variationFour,$variationFourPrice,$variationFourStock,$variationFourImage))
               {
               $_SESSION['messageType'] = 1;
               header('Location: ../addProduct.php?type=1');
               // echo "done register";   
               // $_SESSION['uid'] = $uid;
               // echo "<script>alert('Register Success Without Upline!');window.location='../userDashboard.php'</script>";
          
          }

     }

     else if($variationThree && $variationTwo && $variationOne){
          if(registerNewProductV3($conn,$uid,$name,$sku,$slug,$animalType,$category,$brand,$expiryDate,$status,$description,
               $link,$imageOne,$imageTwo,$imageThree,$imageFour,$variationOne,$variationOnePrice,$variationOneStock,$variationOneImage,$variationTwo,
               $variationTwoPrice,$variationTwoStock,$variationTwoImage,$variationThree,$variationThreePrice,$variationThreeStock,
               $variationThreeImage))
               {
               $_SESSION['messageType'] = 1;
               header('Location: ../addProduct.php?type=1');
               // echo "done register";   
               // $_SESSION['uid'] = $uid;
               // echo "<script>alert('Register Success Without Upline!');window.location='../userDashboard.php'</script>";
          
          }
     }
     else if($variationTwo && $variationOne){
          if(registerNewProductV2($conn,$uid,$name,$sku,$slug,$animalType,$category,$brand,$expiryDate,$status,$description,
               $link,$imageOne,$imageTwo,$imageThree,$imageFour,$variationOne,$variationOnePrice,$variationOneStock,$variationOneImage,$variationTwo,
               $variationTwoPrice,$variationTwoStock,$variationTwoImage))
               {
               $_SESSION['messageType'] = 1;
               header('Location: ../addProduct.php?type=1');
               // echo "done register";   
               // $_SESSION['uid'] = $uid;
               // echo "<script>alert('Register Success Without Upline!');window.location='../userDashboard.php'</script>";
          
          }
     }
     else if($variationOne){
          if(registerNewProductV1($conn,$uid,$name,$sku,$slug,$animalType,$category,$brand,$expiryDate,$status,$description,
               $link,$imageOne,$imageTwo,$imageThree,$imageFour,$variationOne,$variationOnePrice,$variationOneStock,$variationOneImage))
               {
               $_SESSION['messageType'] = 1;
               header('Location: ../addProduct.php?type=1');
               // echo "done register";   
               // $_SESSION['uid'] = $uid;
               // echo "<script>alert('Register Success Without Upline!');window.location='../userDashboard.php'</script>";
               
          }
     }
     if($name){
          if(registerNewProduct($conn,$uid,$name,$sku,$slug,$animalType,$category,$brand,$expiryDate,$status,$description,
          $link,$imageOne,$imageTwo,$imageThree,$imageFour))
          {
              $_SESSION['messageType'] = 1;
              header('Location: ../addProduct.php?type=1');
              // echo "done register";   
              // $_SESSION['uid'] = $uid;
              // echo "<script>alert('Register Success Without Upline!');window.location='../userDashboard.php'</script>";
              
          }
     }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../addProduct.php?type=2');
        //echo "fail to register register";
        //echo "<script>alert('register details has been used by others');window.location='../addUser.php'</script>";
    }     
}
else 
{
     header('Location: ../index.php');
}

?>