<?php

if (session_id() == ""){
     session_start();
 }
 
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Color.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewColor($conn,$name,$status,$type)
{
     if(insertDynamicData($conn,"color",array("name","status","type"),
          array($name,$status,$type),"sss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $name = rewrite($_POST['register_name']);
     $status = rewrite($_POST['register_status']);
     $type = rewrite($_POST['register_type']);


     //   FOR DEBUGGING 
    //  echo "<br>";
    //  echo $name."<br>";
    //  echo $status."<br>";

    if($type == 1)
    {
        if($name && $status)
        {
            if(registerNewColor($conn,$name,$status,$type))
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../puppyColor.php?type=1');
                //echo "done register";   
                // $_SESSION['uid'] = $uid;
                // echo "<script>alert('Register Success Without Upline!');window.location='../userDashboard.php'</script>";
            }
            else
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../addPuppyColor.php?type=4');
                //echo "done failed";
            }
        }
        else if(!$name)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../addPuppyColor.php?type=2');
            //echo "fail to register color";
            //echo "<script>alert('register details has been used by others');window.location='../addUser.php'</script>";
        }
        else if(!$status)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../addPuppyColor.php?type=3');
            //echo "fail to register color";
            //echo "<script>alert('register details has been used by others');window.location='../addUser.php'</script>";
        }
    }

    if($type == 2)
    {
        if($name && $status)
        {
            if(registerNewColor($conn,$name,$status,$type))
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../kittenColor.php?type=1');
                //echo "done register";   
                // $_SESSION['uid'] = $uid;
                // echo "<script>alert('Register Success Without Upline!');window.location='../userDashboard.php'</script>";
            }
            else
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../addKittenColor.php?type=4');
                //echo "done failed";
            }
        }
        else if(!$name)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../addKittenColor.php?type=2');
            //echo "fail to register color";
            //echo "<script>alert('register details has been used by others');window.location='../addUser.php'</script>";
        }
        else if(!$status)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../addKittenColor.php?type=3');
            //echo "fail to register color";
            //echo "<script>alert('register details has been used by others');window.location='../addUser.php'</script>";
        }
    }

    if($type == 3)
    {
        if($name && $status)
        {
            if(registerNewColor($conn,$name,$status,$type))
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../reptileColor.php?type=1');
                //echo "done register";   
                // $_SESSION['uid'] = $uid;
                // echo "<script>alert('Register Success Without Upline!');window.location='../userDashboard.php'</script>";
            }
            else
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../addReptileColor.php?type=4');
                //echo "done failed";
            }
        }
        else if(!$name)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../addReptileColor.php?type=2');
            //echo "fail to register color";
            //echo "<script>alert('register details has been used by others');window.location='../addUser.php'</script>";
        }
        else if(!$status)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../addReptileColor.php?type=3');
            //echo "fail to register color";
            //echo "<script>alert('register details has been used by others');window.location='../addUser.php'</script>";
        }
    }
}
else 
{
     header('Location: ../index.php');
}

?>