<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userID = $_SESSION['uid'];

function addBankAccount($conn,$uid,$username,$bank,$bankHolder,$bankNo,$status)
{
     if(insertDynamicData($conn,"bank",array("uid","username","bank_name","bank_account_holder","bank_account_no","status"),
          array($uid,$username,$bank,$bankHolder,$bankNo,$status),"ssssss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = $userID;

     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userID),"s");

     $username = $userDetails[0]->getName();

     $bank = rewrite($_POST['bank_name']);
     $bankHolder = rewrite($_POST['bank_acc_name']);
     $bankNo = rewrite($_POST['bank_acc_number']);

     $status = "Active";

    //   FOR DEBUGGING 
    //  echo "<br>";
    //  echo $uid."<br>";
    //  echo $username."<br>";
    //  echo $bank."<br>";
    //  echo $bankHolder."<br>";
    //  echo $bankNo."<br>";

     if(addBankAccount($conn,$uid,$username,$bank,$bankHolder,$bankNo,$status))
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../editBankDetails.php?type=2');
     }
     else 
     {
          echo "fail";   
     }
 
}
else 
{
     header('Location: ../index.php');
}

?>