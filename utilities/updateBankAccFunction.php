<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Bank.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = $_SESSION['uid'];

    $bankId = rewrite($_POST["bank_id"]);

    $bankName = rewrite($_POST["edit_bank_name"]);
    $bankAccName = rewrite($_POST["edit_bank_acc_name"]);
    $bankAccNo = rewrite($_POST["edit_bank_acc_no"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $cardId."<br>";
    // echo $cardName."<br>";

    $bank = getBank($conn," id = ?",array("id"),array($cardId),"i");    

    if(!$bank)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($bankName)
        {
            array_push($tableName,"	bank_name");
            array_push($tableValue,$bankName);
            $stringType .=  "s";
        }
        if($bankAccName)
        {
            array_push($tableName,"bank_account_holder");
            array_push($tableValue,$bankAccName);
            $stringType .=  "s";
        }
        if($bankAccNo)
        {
            array_push($tableName,"bank_account_no");
            array_push($tableValue,$bankAccNo);
            $stringType .=  "s";
        }

        array_push($tableValue,$bankId);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"bank"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../editBankDetails.php?type=4');
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../editBankDetails.php?type=7');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../editBankDetails.php?type=6');
    }

}
else 
{
    header('Location: ../index.php');
}
?>