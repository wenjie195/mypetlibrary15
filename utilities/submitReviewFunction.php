<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Reviews.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userID = $_SESSION['uid'];

function addReviews($conn,$reviewUid,$uid,$username,$details,$imageOne,$status)
{
     if(insertDynamicData($conn,"reviews",array("uid","author_uid","author_name","paragraph_one","image","display"),
          array($reviewUid,$uid,$username,$details,$imageOne,$status),"ssssss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();


     $reviewUid = md5(uniqid());

     $uid = $userID;
     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userID),"s");
     $username = $userDetails[0]->getName();
     $details = rewrite($_POST['review_details']);
     $status = "Pending";

     $imageOne = $uid.$_FILES['image_one']['name'];
     $target_dir = "../reviewImages/";
     $target_file = $target_dir . basename($_FILES["image_one"]["name"]);
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif");
     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['image_one']['tmp_name'],$target_dir.$imageOne);
     }

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $username."<br>";

     if(addReviews($conn,$reviewUid,$uid,$username,$details,$imageOne,$status))
     {
          // echo "submit";   
          $_SESSION['messageType'] = 1;
          header('Location: ../petSellerReview.php?type=1');
     }
     else 
     {
          // echo "fail";   
          $_SESSION['messageType'] = 1;
          header('Location: ../petSellerReview.php?type=2');
     }
 
}
else 
{
     header('Location: ../index.php');
}

?>