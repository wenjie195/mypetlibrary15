<?php
// if (session_id() == "")
// {
//      session_start();
// }
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewUser($conn,$uid,$name,$email,$fbId,$phoneNo,$birthday,$gender,$status,$receiverName,$receiverNo,
$state,$area,$postal,$address,$bank,$bankHolder,$bankNo,$nameOnCard,$cardNo,$cardType,$expiry,$ccv,$postalCode,
$billingAddress,$finalPassword,$salt)
{
     if(insertDynamicData($conn,"user",array("uid","name","email","fb_id","phone_no","birthday","gender","account_status",
     "receiver_name","receiver_contact_no","shipping_state","shipping_area","shipping_postal_code","shipping_address",
     "bank_name","bank_account_holder","bank_account_no","name_on_card","card_no","card_type","expiry_date","ccv",
     "postal_code","billing_address","password","salt"),
          array($uid,$name,$email,$fbId,$phoneNo,$birthday,$gender,$status,$receiverName,$receiverNo,
          $state,$area,$postal,$address,$bank,$bankHolder,$bankNo,$nameOnCard,$cardNo,$cardType,$expiry,$ccv,$postalCode,
          $billingAddress,$finalPassword,$salt),"sssssssssssssssssssssiisss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $name = rewrite($_POST['register_name']);
     $email = rewrite($_POST['register_email']);
     $fbId = rewrite($_POST['register_fbId']);
     $phoneNo = rewrite($_POST['register_phone']);
     $birthday = rewrite($_POST['register_birthday']);
     $gender = rewrite($_POST['register_gender']);
     $status = rewrite($_POST['register_status']);
     $receiverName = rewrite($_POST['register_receiver_name']);
     $receiverNo = rewrite($_POST['register_receiver_no']);
     $state = rewrite($_POST['register_state']);
     $area = rewrite($_POST['register_area']);
     $postal = rewrite($_POST['register_postal']);
     $address = rewrite($_POST['register_address']);
     $bank = rewrite($_POST['register_bank']);
     $bankHolder = rewrite($_POST['register_bank_holder']);
     $bankNo = rewrite($_POST['register_bank_no']);
     $nameOnCard = rewrite($_POST['register_name_onCard']);
     $cardNo = rewrite($_POST['register_card_no']);
     $cardType = rewrite($_POST['register_card_type']);
     $expiry= rewrite($_POST['register_expiry']);
     $ccv = rewrite($_POST['register_ccv']);
     $postalCode = rewrite($_POST['register_postal_code']);
     $billingAddress = rewrite($_POST['register_billing_address']);

     $register_password = "123321";
     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     //   FOR DEBUGGING 
     //  echo "<br>";
     //  echo $uid."<br>";
     //  echo $name."<br>";
     //  echo $email."<br>";
     //  echo $fbId."<br>";
     //  echo $phoneNo."<br>";
     //  echo $birthday."<br>";
     //  echo "<br>";
     //  echo $gender."<br>";
     //  echo $status."<br>";
     //  echo $receiverName."<br>";
     //  echo $receiverNo."<br>";
     //  echo $state."<br>";
     //  echo $area."<br>";
     //  echo "<br>";
     //  echo $postal."<br>";
     //  echo $address."<br>";
     //  echo $bank."<br>";
     //  echo $bankHolder."<br>";
     //  echo $bankNo."<br>";
     //  echo $nameOnCard."<br>";
     //  echo "<br>";
     //  echo $cardNo."<br>";
     //  echo $cardType."<br>";
     //  echo $expiry."<br>";
     //  echo $ccv."<br>";
     //  echo $postalCode."<br>";

     $usernameRows = getUser($conn," WHERE name = ? ",array("name"),array($_POST['register_name']),"s");
     $usernameDetails = $usernameRows[0];

     $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($_POST['register_email']),"s");
     $userEmailDetails = $userEmailRows[0];

     $userPhoneRows = getUser($conn," WHERE phone_no = ? ",array("phone_no"),array($_POST['register_phone']),"s");
     $userPhoneDetails = $userPhoneRows[0];

     if(!$usernameDetails && !$userEmailDetails && !$userPhoneDetails)
     // if($name && $email)
     {
          if(registerNewUser($conn,$uid,$name,$email,$fbId,$phoneNo,$birthday,$gender,$status,$receiverName,$receiverNo,
          $state,$area,$postal,$address,$bank,$bankHolder,$bankNo,$nameOnCard,$cardNo,$cardType,$expiry,$ccv,$postalCode,
          $billingAddress,$finalPassword,$salt))
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../addUser.php?type=1');
          }
          else
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../addUser.php?type=5');
          }
     }
     else
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../addUser.php?type=6');
     }     
}
else 
{
     header('Location: ../index.php');
}
?>