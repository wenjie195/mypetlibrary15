<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/allNoticeModals.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //todo validation on server side
    //TODO change login with email to use username instead
    //TODO add username field to register's backend
    $conn = connDB();

    if(isset($_POST['loginButton'])){

        $name = rewrite($_POST['register_name']);
        $password = rewrite($_POST['register_password']);

        $userRows = getUser($conn," WHERE name = ? ",array("name"),array($name),"s");
        if($userRows)
        {
            $user = $userRows[0];

            $tempPass = hash('sha256',$password);
            $finalPassword = hash('sha256', $user->getSalt() . $tempPass);
    
                if($finalPassword == $user->getPassword()) 
                {
                    // if(isset($_POST['remember-me'])) 
                    // {
                    //     setcookie('name-mypetslib', $name, time() + (86400 * 30), "/");
                    //     setcookie('password-mypetslib', $password, time() + (86400 * 30), "/");
                    //     setcookie('remember-mypetslib', 1, time() + (86400 * 30), "/");
                    //     // echo 'remember me';
                    // }
                    // else 
                    // {
                    //     setcookie('name-mypetslib', '', time() + (86400 * 30), "/");
                    //     setcookie('password-mypetslib', '', time() + (86400 * 30), "/");
                    //     setcookie('remember-mypetslib', 0, time() + (86400 * 30), "/");
                    //     // echo 'null';
                    // }

                    $_SESSION['uid'] = $user->getUid();
                    $_SESSION['usertype'] = $user->getUserType();
                    
                    if($user->getUserType() == 0)
                    {
                        header('Location: ../adminDashboard.php');
                        // echo "admin page";
                    }
                    elseif($user->getUserType() == 2)
                    {
                        header('Location: ../sellerDashboard.php');
                    }
                    else
                    {
                        // header('Location: ../profile.php?lang=ch');
                        $_SESSION['messageType'] = 1;
                        header('Location: ../login.php?type=7');
                    }
                }
                else 
                {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../login.php?type=4');
                    //header('Location: ../index.php?type=11');
                    //echo "<script>alert('incorrect password');window.location='../index.php'</script>";
                }
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../adminLogin.php?type=2');
          //header('Location: ../index.php?type=7');
          //echo "// no user with this email ";
          //echo "<script>alert('no user with this username');window.location='../index.php'</script>";
        }
    }

    $conn->close();
}
?>