<?php

if (session_id() == ""){
     session_start();
 }
 
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Breed.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewBreed($conn,$name,$status,$type)
{
     if(insertDynamicData($conn,"breed",array("name","status","type"),
          array($name,$status,$type),"sss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $name = rewrite($_POST['register_name']);
     $status = rewrite($_POST['register_status']);
     $type = rewrite($_POST['register_type']);


     //   FOR DEBUGGING 
    //  echo "<br>";
    //  echo $name."<br>";
    //  echo $status."<br>";
    //  echo $type."<br>";

    if($type == 1)
    {
        if($name && $status)
        {
            if(registerNewBreed($conn,$name,$status,$type))
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../puppyBreed.php?type=1');
                //echo "done register";   
                // $_SESSION['uid'] = $uid;
                // echo "<script>alert('Register Success Without Upline!');window.location='../userDashboard.php'</script>";
            }
            else
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../addPuppyBreed.php?type=4');
                //echo "done failed";
            }
        }
        else if(!$name)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../addPuppyBreed.php?type=2');
            //echo "fail to register breed";
            //echo "<script>alert('register details has been used by others');window.location='../addUser.php'</script>";
        }
        else if(!$status)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../addPuppyBreed.php?type=3');
            //echo "fail to register breed";
            //echo "<script>alert('register details has been used by others');window.location='../addUser.php'</script>";
        }  
    }
    else if($type == 2)
    {
        if($name && $status)
        {
            if(registerNewBreed($conn,$name,$status,$type))
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../kittenBreed.php?type=1');
                //echo "done register";   
                // $_SESSION['uid'] = $uid;
                // echo "<script>alert('Register Success Without Upline!');window.location='../userDashboard.php'</script>";
            }
            else
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../addKittenBreed.php?type=4');
                //echo "done failed";
            }
        }
        else if(!$name)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../addKittenBreed.php?type=2');
            //echo "fail to register breed";
            //echo "<script>alert('register details has been used by others');window.location='../addUser.php'</script>";
        }
        else if(!$status)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../addKittenBreed.php?type=3');
            //echo "fail to register breed";
            //echo "<script>alert('register details has been used by others');window.location='../addUser.php'</script>";
        }  
    }
    else if($type == 3)
    {
        if($name && $status)
        {
            if(registerNewBreed($conn,$name,$status,$type))
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../reptileBreed.php?type=1');
                //echo "done register";   
                // $_SESSION['uid'] = $uid;
                // echo "<script>alert('Register Success Without Upline!');window.location='../userDashboard.php'</script>";
            }
            else
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../addReptileBreed.php?type=4');
                //echo "done failed";
            }
        }
        else if(!$name)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../addReptileBreed.php?type=2');
            //echo "fail to register breed";
            //echo "<script>alert('register details has been used by others');window.location='../addUser.php'</script>";
        }
        else if(!$status)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../addReptileBreed.php?type=3');
            //echo "fail to register breed";
            //echo "<script>alert('register details has been used by others');window.location='../addUser.php'</script>";
        }  
    } 
}
else 
{
     header('Location: ../index.php');
}

?>