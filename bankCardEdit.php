<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

// require_once dirname(__FILE__) . '/classes/Bank.php';
require_once dirname(__FILE__) . '/classes/CreditCard.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Credit/Debit Card | Mypetslibrary" />
<title>Credit/Debit Card | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
	<div class="width100 same-padding overflow min-height menu-distance2">

    <?php
    if(isset($_POST['card_id']))
    {
    $conn = connDB();
    $cardId = getCreditCard($conn,"WHERE id = ? ", array("id") ,array($_POST['card_id']),"i");
    $cardDetails = $cardId[0];
    ?>

    	<p class="review-product-name">Credit/Debit Card</p>
        <!-- <form> -->
        <form action="utilities/updateCreditCardFunction.php" method="POST">
        <div class="dual-input">
        	<p class="input-top-p">Name on Card</p>
        	<input class="input-name clean input-textarea" type="text" placeholder="Name on Card" value="<?php echo $cardDetails->getNameOnCard();?>" name="edit_card_name" id="edit_card_name" required> 
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p">Card Number</p>
        	<input class="input-name clean input-textarea" type="text" placeholder="Card Number" value="<?php echo $cardDetails->getCardNo();?>" name="edit_card_no" id="edit_card_no" required>
        </div>        
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p">Card type</p>
        	<select class="input-name clean" name="edit_card_type" id="edit_card_type" required>
                <!-- <option>Card Type</option>
                <option>Credit</option>
                <option>Debit</option> -->

                <?php
                    if($cardDetails->getCardType() == 'Credit')
                    {
                    ?>
                        <option selected value="Credit"  name='Credit'>Credit</option>
                        <option value="Debit"  name='Debit'>Debit</option>
                    <?php
                    }
                    elseif($cardDetails->getCardType() == 'Debit')
                    {
                    ?>
                        <option selected value="Debit"  name='Debit'>Debit</option>
                        <option value="Credit"  name='Credit'>Credit</option>
                    <?php
                    }
                ?>

            </select>      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p">Expiry Date</p>
        	<input class="input-name clean input-textarea" type="text" placeholder="Expiry Date" value="<?php echo $cardDetails->getExpiryDate();?>" name="edit_expiry_date" id="edit_expiry_date">           
        </div>         
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p">CCV</p>
            <div class="edit-password-input-div">
                <!-- <input class="input-name clean input-password edit-password-input input-textarea"  type="text" placeholder="CCV" value="" name="" id="" required></textarea> -->
                <input class="input-name clean input-password edit-password-input input-textarea"  type="text" placeholder="CCV" value="<?php echo $cardDetails->getCcv();?>" name="edit_ccv" id="edit_ccv" required>
                <p class="edit-p-password open-ccv"><img src="img/ccv.png" class="hover1a edit-password-img ccv-img" alt="CCV" title="CCV"><img src="img/ccv2.png" class="hover1b edit-password-img ccv-img" alt="CCV" title="CCV"></p>
            </div>     
        </div> 
        <div class="dual-input second-dual-input">
        	<p class="input-top-p">Postal Code</p>
        	<input class="input-name clean input-textarea" type="text" placeholder="Postal Code" required value="<?php echo $cardDetails->getPostalCode();?>" name="edit_postal_code" id="edit_postal_code" required>         
        </div>         
        <div class="clear"></div>
        <div class="width100 overflow">
        	<p class="input-top-p">Billing Address</p>
        	<textarea class="input-name clean address-textarea" type="text" placeholder="Billing Address" name="edit_billing_address" id="edit_billing_address" required><?php echo $cardDetails->getBillingAddress();?></textarea>      
        </div>  
               
        <input class="input-name clean input-textarea" type="hidden" value="<?php echo $cardDetails->getId();?>" name="card_id" id="card_id" readonly>

        <div class="clear"></div>   

        <div class="width100 overflow text-center">     
        	<button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
        </div>

        <div class="width100 overflow text-center padding-top-bottom">   
        	<!-- <button class="red-btn bottom-delete white-text clean2 edit-1-btn margin-auto">Delete</button> -->
        </div>        
        
        </form>

    <?php
    }
    ?>

	</div>
<div class="clear"></div>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>