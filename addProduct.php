<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
//require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/Category.php';
require_once dirname(__FILE__) . '/classes/Brand.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$categoryDetails = getCategory($conn);
$brandDetails = getBrand($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add New Product | Mypetslibrary" />
<title>Add New Product | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'userHeaderAfterLogin.php'; ?>
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Add New Product</h1>
            <div class="green-border"></div>
   </div>
   <div class="border-separation">
        <div class="clear"></div>
        <form action="utilities/registerProductFunction.php" method="POST" enctype="multipart/form-data">
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Product Name*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Product Name" required name="register_name" id="register_name">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">SKU*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="SKU" required name="register_sku" id="register_sku">     
        </div>        
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Product Slug (for URL, no spacing or contain any symbol except -)</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Product Slug"  required name="register_slug" id="register_slug">      
        </div>
        <!-- The price will display as RM (Variation Min Price) - (Variation Max Price) -->
        <!--<div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Price (RM)*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Price (RM)"  required name="register_price" id="register_price">    
        </div> -->

        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">For Animal Type*</p>
        	<select class="input-name clean admin-input" required name="register_animal_type" id="register_animal_type">
            	<option>Puppy</option>
                <option>Kitten</option>
                <option>Reptile</option>
                <option>Other</option>
            </select>       
        </div>        
        
        <div class="clear"></div>  
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Category*</p>
        	<select class="input-name clean admin-input" required value="<?php echo $categoryDetails[0]->getName();?>" name="register_category" id="register_category">
                <!-- <option>Accessory</option>
                <option>Food</option>
                <option>Grooming</option>
                <option>Health</option>
                <option>Supplies</option>
                <option>Toys</option> -->
                <option value="">Please Select a Category</option>
                <?php
                for ($cntPro=0; $cntPro <count($categoryDetails) ; $cntPro++)
                {
                ?>
                    <option value="<?php echo $categoryDetails[$cntPro]->getName(); ?>"> 
                        <?php echo $categoryDetails[$cntPro]->getName(); ?>
                    </option>
                <?php
                }
                ?>
            </select>        
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Brand*</p>
        	<select class="input-name clean admin-input" required value="<?php echo $brandDetails[0]->getName();?>" name="register_brand" id="register_brand">
            	<!-- <option>Pedigree</option>
                <option>Pedigree</option> -->
                <option value="">Please Select a Brand</option>
                <?php
                for ($cntPro=0; $cntPro <count($brandDetails) ; $cntPro++)
                {
                ?>
                    <option value="<?php echo $brandDetails[$cntPro]->getName(); ?>"> 
                        <?php echo $brandDetails[$cntPro]->getName(); ?>
                    </option>
                <?php
                }
                ?>
            </select>   
        </div>           
		<!-- The stock will display as total amount of all stock -->
        <!--<div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Stock*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Amount" name="register_stock" id="register_stock" required>  
        </div>  -->
        
        <div class="clear"></div>
         <div class="dual-input">
        	<p class="input-top-p admin-top-p">Expiry Date*</p>
        	<input class="input-name clean input-textarea admin-input" type="date" placeholder="Expiry Date" name="register_expiry_date" id="register_expiry_date" required>         
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Status</p>
        	<select class="input-name clean admin-input" required name="register_status" id="register_status">
            	<option>Available</option>
                <option>Sold</option>
            </select>   
        </div>  
        <div class="clear"></div>
         <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Product Description*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Product Description" name="register_description" id="register_description" required>              
        </div>
        <div class="clear"></div>
        <div class="width100 overflow">
            <p class="input-top-p admin-top-p">Video Link (Optional)</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Video Link" name="register_link" id="register_link">           
        </div>    
		<!-- Video will display as the 1st image -->
        <div class="clear"></div>       
		<div class="width100 overflow margin-bottom10">
        	<p class="input-top-p admin-top-p">Upload Product Photo (Maximum 4)</p>
           
            <p><input id="file-upload" type="file" name="image_one" id="image_one" accept="image/*" class="margin-bottom10" /></p> 
          
            <p><input id="file-upload" type="file" name="image_two" id="image_two" accept="image/*" class="margin-bottom10" /></p> 
       
            <p><input id="file-upload" type="file" name="image_three" id="image_three" accept="image/*" class="margin-bottom10" /></p> 
         
            <p><input id="file-upload" type="file" name="image_four" id="image_four" accept="image/*" class="margin-bottom10" /></p>
    
          <!--  <input id="file-upload" type="file" name="image_five" id="image_five" accept="image/*" /> -->
            <!-- Photo cropping into square size feature -->
        </div>             
		<div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Variation 1 Name*</p>
        	<input class="input-name clean input-textarea admin-input" onkeyup="myFunction()" type="text" placeholder="Variation 1 Name" name="register_variation_one" id="register_variation_one" required>      
        </div>
        <!-- <div style="display: none" id="showLater"> -->
            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Price*</p>
                <input class="input-name clean input-textarea admin-input" type="number" placeholder="0.00" name="register_variation_one_price" id="register_variation_one_price" required>     		
            </div>        
            <div class="clear"></div>        
            <div class="dual-input">
                <p class="input-top-p admin-top-p">Stock*</p>
                <input class="input-name clean input-textarea admin-input" type="number" placeholder="0" name="register_variation_one_stock" id="register_variation_one_stock" required>      
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Photo*</p>
                <p class="margin-bottom30"><input id="file-upload" type="file" name="register_variation_one_image" id="register_variation_one_image" accept="image/*" class="margin-bottom10" required/></p>     		
            </div>
        <!-- </div>         -->
        <div class="clear"></div>         
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Variation 2 Name (Optional)</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Variation 2 Name" name="register_variation_two" id="register_variation_two">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Price</p>
        	<input class="input-name clean input-textarea admin-input" type="number" placeholder="0.00" name="register_variation_two_price" id="register_variation_two_price">     		
        </div>        
        <div class="clear"></div>        
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Stock</p>
        	<input class="input-name clean input-textarea admin-input" type="number" placeholder="0" name="register_variation_two_stock" id="register_variation_two_stock">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Photo</p>
        	<p class="margin-bottom30"><input id="file-upload" type="file" name="register_variation_two_image" id="register_variation_two_image" accept="image/*" class="margin-bottom10" /></p>     		
        </div>        
        <div class="clear"></div>         
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Variation 3 Name (Optional)</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Variation 3 Name" name="register_variation_three" id="register_variation_three">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Price</p>
        	<input class="input-name clean input-textarea admin-input" type="number" placeholder="0.00" name="register_variation_three_price" id="register_variation_three_price">     		
        </div>        
        <div class="clear"></div>        
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Stock</p>
        	<input class="input-name clean input-textarea admin-input" type="number" placeholder="0" name="register_variation_three_stock" id="register_variation_three_stock">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Photo</p>
        	<p class="margin-bottom30"><input id="file-upload" type="file" name="register_variation_three_image" id="register_variation_three_image" accept="image/*"  class="margin-bottom10"/></p>     		
        </div>        
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Variation 4 Name (Optional)</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Variation 4 Name" name="register_variation_four" id="register_variation_four">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Price</p>
        	<input class="input-name clean input-textarea admin-input" type="number" placeholder="0.00" name="register_variation_four_price" id="register_variation_four_price">     		
        </div>        
        <div class="clear"></div>        
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Stock</p>
        	<input class="input-name clean input-textarea admin-input" type="number" placeholder="0" name="register_variation_four_stock" id="register_variation_four_stock">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Photo</p>
        	<p class="margin-bottom30"><input id="file-upload" type="file" name="register_variation_four_image" id="register_variation_four_image" accept="image/*" class="margin-bottom10" /></p>     		
        </div>        
        <div class="clear"></div>         
        <div class="width100 overflow text-center">     
        	<div class="green-button white-text clean2 edit-1-btn margin-auto ow-peach-button" onclick="showVariation();this.style.visibility= 'hidden';">Add More Variation</div>
        </div>        
        <div class="clear"></div>
        <!-- <div id="variationDiv" style="display:none">  
            <div class="dual-input">
                <p class="input-top-p admin-top-p">Variation 5 Name (Optional)</p>
                <input class="input-name clean input-textarea admin-input" type="text" placeholder="Variation 5 Name" name="register_link" id="register_link">      
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Price</p>
                <input class="input-name clean input-textarea admin-input" type="number" placeholder="0.00" name="register_link" id="register_link">     		
            </div>        
            <div class="clear"></div>        
            <div class="dual-input">
                <p class="input-top-p admin-top-p">Stock</p>
                <input class="input-name clean input-textarea admin-input" type="number" placeholder="0" name="register_link" id="register_link">      
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Photo</p>
                <p class="margin-bottom30"><input id="file-upload" type="file" name="image_six" id="image_six" accept="image/*"  class="margin-bottom10"/></p>     		
            </div>        
            <div class="clear"></div>         	
            <div class="dual-input">
                <p class="input-top-p admin-top-p">Variation 6 Name (Optional)</p>
                <input class="input-name clean input-textarea admin-input" type="text" placeholder="Variation 6 Name" name="register_link" id="register_link">      
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Price</p>
                <input class="input-name clean input-textarea admin-input" type="number" placeholder="0.00" name="register_link" id="register_link">     		
            </div>        
            <div class="clear"></div>        
            <div class="dual-input">
                <p class="input-top-p admin-top-p">Stock</p>
                <input class="input-name clean input-textarea admin-input" type="number" placeholder="0" name="register_link" id="register_link">      
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Photo</p>
                <p class="margin-bottom30"><input id="file-upload" type="file" name="image_six" id="image_six" accept="image/*" class="margin-bottom10" /></p>     		
            </div>        
            <div class="clear"></div> 
            
            <div class="dual-input">
                <p class="input-top-p admin-top-p">Variation 7 Name (Optional)</p>
                <input class="input-name clean input-textarea admin-input" type="text" placeholder="Variation 7 Name" name="register_link" id="register_link">      
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Price</p>
                <input class="input-name clean input-textarea admin-input" type="number" placeholder="0.00" name="register_link" id="register_link">     		
            </div>        
            <div class="clear"></div>        
            <div class="dual-input">
                <p class="input-top-p admin-top-p">Stock</p>
                <input class="input-name clean input-textarea admin-input" type="number" placeholder="0" name="register_link" id="register_link">      
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Photo</p>
                <p class="margin-bottom30"><input id="file-upload" type="file" name="image_six" id="image_six" accept="image/*"  class="margin-bottom10"/></p>     		
            </div>        
            <div class="clear"></div>             
            <div class="dual-input">
                <p class="input-top-p admin-top-p">Variation 8 Name (Optional)</p>
                <input class="input-name clean input-textarea admin-input" type="text" placeholder="Variation 8 Name" name="register_link" id="register_link">      
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Price</p>
                <input class="input-name clean input-textarea admin-input" type="number" placeholder="0.00" name="register_link" id="register_link">     		
            </div>        
            <div class="clear"></div>        
            <div class="dual-input">
                <p class="input-top-p admin-top-p">Stock</p>
                <input class="input-name clean input-textarea admin-input" type="number" placeholder="0" name="register_link" id="register_link">      
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Photo</p>
                <p class="margin-bottom30"><input id="file-upload" type="file" name="image_six" id="image_six" accept="image/*"  class="margin-bottom10"/></p>     		
            </div>        
            <div class="clear"></div>
            <div class="dual-input">
                <p class="input-top-p admin-top-p">Variation 9 Name (Optional)</p>
                <input class="input-name clean input-textarea admin-input" type="text" placeholder="Variation 9 Name" name="register_link" id="register_link">      
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Price</p>
                <input class="input-name clean input-textarea admin-input" type="number" placeholder="0.00" name="register_link" id="register_link">     		
            </div>        
            <div class="clear"></div>        
            <div class="dual-input">
                <p class="input-top-p admin-top-p">Stock</p>
                <input class="input-name clean input-textarea admin-input" type="number" placeholder="0" name="register_link" id="register_link">      
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Photo</p>
                <p class="margin-bottom30"><input id="file-upload" type="file" name="image_six" id="image_six" accept="image/*"  class="margin-bottom10"/></p>     		
            </div>        
            <div class="clear"></div>             
            <div class="dual-input">
                <p class="input-top-p admin-top-p">Variation 10 Name (Optional)</p>
                <input class="input-name clean input-textarea admin-input" type="text" placeholder="Variation 10 Name" name="register_link" id="register_link">      
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Price</p>
                <input class="input-name clean input-textarea admin-input" type="number" placeholder="0.00" name="register_link" id="register_link">     		
            </div>        
            <div class="clear"></div>        
            <div class="dual-input">
                <p class="input-top-p admin-top-p">Stock</p>
                <input class="input-name clean input-textarea admin-input" type="number" placeholder="0" name="register_link" id="register_link">      
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p admin-top-p">Photo</p>
                <p class="margin-bottom30"><input id="file-upload" type="file" name="image_six" id="image_six" accept="image/*"  class="margin-bottom10"/></p>     		
            </div>        
            <div class="clear"></div>              -->
                                 
        </div>
        
        
        
        
        
        
        <div class="width100 overflow text-center">     
        	<button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
        </div>
        </form>
	</div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

<?php

if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully registered new product!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Registration of new seller failed!";
        } 
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<!-- <script type="text/javascript">
      $(document).ready(function(){
        $("#register_variation_one").change(function(){
          if ($("#register_variation_one").val() == 'CDM') {
            $("#showLater").slideDown(function(){
              $(this).show();
            });
          }
          else {
            $("#showLater").slideUp(function(){
              $(this).hide();
            });
          }
        });
    });


function myFunction() {
    $('#showLater').show();
}

    $(document).ready(function(){
        $("#register_variation_one").onkeyup(function(){
          if ($("#register_variation_one").val().length = 0) {
            $("#showLater").hide();
            // $("#showLater").slideUp(function(){
            //     $(this).hide();
            // $("#showLater").slideDown(function(){
            //   $(this).show();
            });
          }
          else {
            $("#showLater").show();
            // $("#showLater").slideDown(function(){
            //   $(this).show();
            // $("#showLater").slideUp(function(){
            //   $(this).hide();
            });
          }
        }).keyup();
    });

// Bind keyup event on the input
$('#register_variation_one').keyup(function() {
  
  // If value is not empty
  if ($(this).val().length == 0) {
    // Hide the element
    $('.showLater').hide();
  } else {
    // Otherwise show it
    $('.showLater').show();
  }
}).keyup(); // Trigger the keyup event, thus running the handler on page load
</script>



    <script type="text/javascript">
      $(document).ready(function(){
        $("#payment_method").change(function(){
          if ($("#payment_method").val() == 'CDM') {
            $("#showLater").slideDown(function(){
              $(this).show();
            });
          }
          else {
            $("#showLater").slideUp(function(){
              $(this).hide();
            });
          }
        });
    });
        </script> -->

</body>
</html>