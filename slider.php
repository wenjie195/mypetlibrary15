<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Slider | Mypetslibrary" />
<title>Slider | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance">
	<div class="width100">
		<div class="left-h1-div two-left-h1-div">
            <h1 class="green-text h1-title">Slider</h1>
            <div class="green-border"></div>
		</div>
        <div class="right-add-div two-right-add-div">
        	<a href="addSlider.php"><div class="green-button white-text">Add Slider</div></a>
        </div>        
        
    </div>
    <div class="clear"></div>
	<div class="width100 border-separation">
 		<div class="width100 overflow margin-bottom-25">
        <form>
        	<p class="review-product-name left-slider">Slider 1</p>
            <div class="right-slider">
					<div class="clean red-btn delete-btn2 open-confirm slider-delete">Delete</div>
                    <a href="updateSlider.php"><div class="clean green-button close-confirm slider-update">Update</div></a>
            </div>
            <div class="clear"></div>
            <div class="width100 overflow">
            	<img src="img/slider.jpg" class="width100"> 
            </div>
            
            
                            <!-- Double Confirm Modal -->
                            <div id="confirm-modal" class="modal-css">
                            
                              <!-- Modal content -->
                              <div class="modal-content-css confirm-modal-margin">
                                <span class="close-css close-confirm">&times;</span>
                                <div class="clear"></div>
                                <h2 class="green-text h2-title confirm-title">Confirm Delete?</h2>
                                <div class="clean cancel-btn close-confirm">Cancel</div>
                                <button class="clean red-btn delete-btn2">Delete</button>
                                <div class="clear"></div>
                                
                                   
                              </div>
                            
                            </div>             
        </form>    
        </div>
        <div class="clear"></div>
 		<div class="width100 overflow margin-bottom-25">
   
        	<p class="review-product-name left-slider">Slider 2</p>
            <div class="right-slider">
					<div class="clean red-btn delete-btn2 open-confirm slider-delete">Delete</div>
                    <a href="updateSlider.php"><div class="clean green-button close-confirm slider-update">Update</div></a>
            </div>
            <div class="clear"></div>
            <div class="width100 overflow">
            	<img src="img/slider.jpg" class="width100"> 
            </div>

        </div> 
        <div class="clear"></div>       
 		<div class="width100 overflow margin-bottom-25">

        	<p class="review-product-name left-slider">Slider 3</p>
            <div class="right-slider">
					<div class="clean red-btn delete-btn2 open-confirm slider-delete">Delete</div>
                    <a href="updateSlider.php"><div class="clean green-button close-confirm slider-update">Update</div></a>
            </div>
            <div class="clear"></div>
            <div class="width100 overflow">
            	<img src="img/slider.jpg" class="width100"> 
            </div>

        </div>        
        <div class="clear"></div>
        
        
    </div>
    <div class="clear"></div>


</div>
<div class="clear"></div>



<?php include 'js.php'; ?>
</body>
</html>