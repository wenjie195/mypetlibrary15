<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Cart | Mypetslibrary" />
<title>Cart | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary - Pedigree Dentastix Dog Treats - Daily oral care treat for your pet.
Unique tasty X-shaped treat clinically proven to reduce plaque. It has active ingredients zinc sulphate &amp; sodium trio polyphosphate that help in slowly down the rate of tartar build up." />
<meta name="description" content="Mypetslibrary - Pedigree Dentastix Dog Treats - Daily oral care treat for your pet. Unique tasty X-shaped treat clinically proven to reduce plaque. It has active ingredients zinc sulphate &amp; sodium trio polyphosphate that help in slowly down the rate of tartar build up." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'userHeaderAfterLogin.php'; ?>
<div class="sticky-tab menu-distance2">
	<div class="tab sticky-tab-tab">
		<button class="tablinks active hover1 tab-tab-btn" onclick="openTab(event, 'Cart')">
            <div class="green-dot"></div>
            <img src="img/cart-1.png" class="tab-icon hover1a" alt="Cart" title="Cart">
            <img src="img/cart-2.png" class="tab-icon hover1b" alt="Cart" title="Cart">
            <p class="tab-tab-p">In Cart</p>
            
        </button>
		<button class="tablinks hover1 tab-tab-btn" onclick="openTab(event, 'Ship')">
        	<div class="green-dot"></div>
            <img src="img/to-ship-1.png" class="tab-icon hover1a" alt="To Ship" title="To Ship">
            <img src="img/to-ship-2.png" class="tab-icon hover1b" alt="To Ship" title="To Ship">        
            <p class="tab-tab-p">To Ship</p>
        </button>
		<button class="tablinks hover1 tab-tab-btn" onclick="openTab(event, 'Receive')">
        	<div class="green-dot"></div>
            <img src="img/to-receive-1.png" class="tab-icon hover1a" alt="To Receive" title="To Receive">
            <img src="img/to-receive-2.png" class="tab-icon hover1b" alt="To Receive" title="To Receive">         
            <p class="tab-tab-p">To Receive</p>
        </button>
		<button class="tablinks hover1 tab-tab-btn" onclick="openTab(event, 'Received')">
        	<div class="green-dot"></div>
            <img src="img/received-1.png" class="tab-icon hover1a" alt="Received" title="Received">
            <img src="img/received-2.png" class="tab-icon hover1b" alt="Received" title="Received">         
            <p class="tab-tab-p">Received</p>
        </button>        
	</div>
</div>

    <div class="two-menu-space width100"></div>    
<div class="width100 same-padding min-height4 adjust-padding">
	<div  id="Cart" class="tabcontent block same-padding">
		<button class="right-delete clean transparent-button">Delete</button>
        <div class="clear"></div>
        <div class="per-product-div">
        	<div class="left-product-check">
                <label for="product1" class="filter-label filter-label3">
                	<div class="left-cart-img-div">
                    	<img src="img/product-2.jpg" class="width100" alt="Product Name" title="Product Name">
                    </div>
                    <input type="checkbox" name="product1" id="product1" value="0" class="filter-input" />
                    <span class="checkmark"></span>
                </label>
                    <div class="left-product-details">
                    	<p class="text-overflow width100 green-text cart-product-title">
                        	Pedigree Dentastix Puppy 56g Dog Treats
                        </p>
                    	<p class="left-quantity1">
                        	Quantity
                        </p>  
                        <div class="numbers-row5 numbers-row-css cart-numbers">
                        
						</div>  
                        <div class="clear"></div> 
                        <p class="left-shipping1">
                        	Shipping
                        </p> 
                        <p class="right-rm">
                        	RM0
                        </p>
                        <div class="clear"></div> 
                        <div class="cart-border"></div>
                        <p class="left-total">
                        	Total (Exclude Shipping)
                        </p> 
                        <p class="right-rm2">
                        	RM6.67
                        </p>                                                
                                          
                    </div>

            </div>
            <div class="right-product-x">
            	<button class="transparent-button x-delete-button hover1 clean">
                	<img src="img/close.png" class="hover1a close-img" alt="Delete" title="Delete">
                    <img src="img/close2.png" class="hover1b close-img" alt="Delete" title="Delete">
                </button>
            </div>
        </div> 
        <div class="per-product-div">
        	<div class="left-product-check">
                <label for="product2" class="filter-label filter-label3 clean">
                	<div class="left-cart-img-div">
                    	<img src="img/product-2.jpg" class="width100" alt="Product Name" title="Product Name">
                    </div>
                    <input type="checkbox" name="product2" id="product2" value="0" class="filter-input clean" />
                    <span class="checkmark"></span>
                </label>
                    <div class="left-product-details">
                    	<p class="text-overflow width100 green-text cart-product-title">
                        	Pedigree Dentastix Puppy 56g Dog Treats
                        </p>
                    	<p class="left-quantity1">
                        	Quantity
                        </p>  
                        <div class="numbers-row5 numbers-row-css cart-numbers">
                        
						</div>  
                        <div class="clear"></div> 
                        <p class="left-shipping1">
                        	Shipping
                        </p> 
                        <p class="right-rm">
                        	RM0
                        </p>
                        <div class="clear"></div> 
                        <div class="cart-border"></div>
                        <p class="left-total">
                        	Total (Exclude Shipping)
                        </p> 
                        <p class="right-rm2">
                        	RM6.67
                        </p>                                                
                                          
                    </div>

            	</div>
                <div class="right-product-x">
                    <button class="transparent-button x-delete-button hover1 clean">
                        <img src="img/close.png" class="hover1a close-img" alt="Delete" title="Delete">
                        <img src="img/close2.png" class="hover1b close-img" alt="Delete" title="Delete">
                    </button>
                </div>
            </div>
            <div class="per-product-div">
            	
            	<label for="voucher" class="filter-label filter-label3 voucher-label">Apply Vouchers (3)
                <input type="checkbox" name="voucher" id="voucher" value="0" class="filter-input" />
                <span class="checkmark"></span>
            	</label>
            <div id="voucherDiv" style="display:none">
            	<div class="voucher-option">
                    <label for="voucher1" class="filter-label filter-label2"><b>Voucher 1</b> - Descrption XXXXXXXXXXX<br>
                    <p class="voucher-exp">Expire on 20/12/20</p>
                        <input type="checkbox" name="voucher1" id="voucher1" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>  
                </div>
                <div class="voucher-option">
                    <label for="voucher1" class="filter-label filter-label2"><b>Voucher 2</b> - Descrption XXXXXXXXXXX<br>
                    <p class="voucher-exp">Expire on 20/12/20</p>
                        <input type="checkbox" name="voucher1" id="voucher1" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>  
                </div>
                <div class="voucher-option">
                    <label for="voucher1" class="filter-label filter-label2"><b>Voucher 3</b> - Descrption XXXXXXXXXXX<br>
                    <p class="voucher-exp">Expire on 20/12/20</p>
                        <input type="checkbox" name="voucher1" id="voucher1" value="0" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>  
                </div>
            </div>
            </div>
            <div class="clear"></div>
            <div class="no-sticky-bottom">
            		<div class="grey-border"></div>
                	<p class="left-bottom-price">Sub-total (1 item)</p>
                    <p class="right-bottom-price">RM6.67</p>
                    <div class="clear"></div>
                	<p class="left-bottom-price">Shipping</p>
                    <p class="right-bottom-price">RM0.00</p> 
                    <div class="clear"></div>
                	<p class="left-bottom-price">Discount</p>
                    <p class="right-bottom-price">RM0.00</p> 
                    <div class="clear"></div>            
            </div>
            <div class="sticky-bottom-price same-padding3">
                    <div class="grey-border"></div>
                	<p class="left-bottom-price weight900">Total</p>
                    <p class="right-bottom-price weight900">RM6.67</p>   
                    <div class="clear"></div>  
                    <div class="width100 text-center">                                                                         
                		<button class="green-button checkout-btn clean">Check Out</button>
                    </div>
            </div>
            <div class="sticky-distance-bottom"></div>
            
            
            
             
        </div>        
        
        
        
           	

	<div  id="Ship" class="tabcontent same-padding">
		<div class="per-product-div">
        	<div class="left-cart-img-div ship-product-img">
            	<img src="img/product-2.jpg" class="width100" alt="Product Name" title="Product Name">
        	</div>
            <div class="ship-right-info">
                    	<p class="text-overflow width100 green-text cart-product-title ship-product-title">
                        	Pedigree Dentastix Puppy 56g Dog Treats
                        </p> 
                        <p class="right-ship-amount">X1</p>
                        <p class="right-ship-price">RM6.67</p>           	
            </div>
        </div>
        <div class="per-product-div padding-top10">
                	<p class="left-bottom-price ow-no-margin-top ship-price1">Place Order</p>
                    <p class="right-bottom-price weight900 ow-no-margin-top ship-price1">14-01-2020</p>   
                    <div class="clear"></div>
                	<p class="left-bottom-price ship-price1">Will be Shipping Out</p>
                    <p class="right-bottom-price weight900 ship-price1">15-01-2020</p>                         
        </div>	
    </div> 
	<div  id="Receive" class="tabcontent same-padding">
		<div class="per-product-div">
        	<div class="left-cart-img-div ship-product-img">
            	<img src="img/product-2.jpg" class="width100" alt="Product Name" title="Product Name">
        	</div>
            <div class="ship-right-info">
                    	<p class="text-overflow width100 green-text cart-product-title ship-product-title">
                        	Pedigree Dentastix Puppy 56g Dog Treats
                        </p> 
                        <p class="right-ship-amount">X1</p>
                        <p class="right-ship-price">RM6.67</p>           	
            </div>
        </div>
        <div class="per-product-div padding-top10">
                	<p class="left-bottom-price ship-price1 receive-price">Delivered On:</p>
                    <p class="right-bottom-price weight900 ship-price1 receive-price">16-01-2020</p>         
        			<div class="clear"></div>
                    <div class="two-square-btn red-btn open-refund">Request Refund</div>
                    <div class="two-square-btn green-button open-review">Received</div>
        </div>        		
    </div>
	<div  id="Received" class="tabcontent same-padding">
    	<a href="orderDetails.php">
            <div class="per-product-div">
                <div class="left-cart-img-div ship-product-img">
                    <img src="img/product-2.jpg" class="width100" alt="Product Name" title="Product Name">
                </div>
                <div class="ship-right-info">
                            <p class="text-overflow width100 green-text cart-product-title ship-product-title">
                                Pedigree Dentastix Puppy 56g Dog Treats
                            </p> 
                            <p class="right-ship-amount">X1</p>
                            <p class="right-ship-price">RM6.67</p>           	
                </div>
            </div>
            <div class="per-product-div padding-top10">
                        <p class="left-bottom-price ow-no-margin-top ship-price1">Received</p>
                        <p class="right-bottom-price weight900 ow-no-margin-top ship-price1">15-01-2020</p>                         
            </div>
    	</a>
    </div>
 </div>        
</div>
<style>
	.animated.slideUp{
		animation:none !important;}
	.animated{
		animation:none !important;}
	.green-footer{
		display:none;}
</style>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>