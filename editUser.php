<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

//$id = $_SESSION['id'];
$conn = connDB();

//$userRows = getUser($conn," WHERE id = ? ",array("id"),array($id),"i");
//$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit User Details | Mypetslibrary" />
<title>Edit User Details | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'userHeaderAfterLogin.php'; ?>
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Edit User Details</h1>
            <div class="green-border"></div>
   </div>
   <div class="border-separation">
        <div class="clear"></div>
        
 		<form method="POST" action="utilities/editUserFunction.php" enctype="multipart/form-data">
            <?php
            if(isset($_POST['user_id']))
            {
                $conn = connDB();
                $userDetails = getUser($conn,"WHERE id = ? ", array("id") ,array($_POST['user_id']),"i");
            ?>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Username*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $userDetails[0]->getName();?>" required name="update_name" id="update_name">      
                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Email*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $userDetails[0]->getEmail();?>" required name="update_email" id="update_email">     
                </div>        
                <div class="clear"></div>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Facebook ID (Optional)</p>
                    <input class="input-name clean input-textarea admin-input" type="text" placeholder="Facebook ID (Optional)" name="update_fbId" id="update_fbId" value="<?php echo $userDetails[0]->getFbId();?>">      
                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Contact No.*</p>
                    <input class="input-name clean input-textarea admin-input" type="text" value="<?php echo $userDetails[0]->getPhoneNo();?>" name="update_phone" id="update_phone">    
                </div>   
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Birthday</p>
                    <input class="input-name clean input-textarea admin-input" type="date" placeholder="Birthday" name="update_birthday" id="update_birthday" value="<?php echo $userDetails[0]->getBirthday();?>">      
                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Gender</p>
                    <select class="input-name clean admin-input" name="update_gender" id="update_gender" value="<?php echo $userDetails[0]->getGender();?>" required >
                        <!-- <option>Male</option>
                        <option>Female</option> -->
                        <?php
                            if($userDetails[0]->getGender() == '')
                            {
                            ?>
                                <option value="Male"  name='Male'>Male</option>
                                <option value="Female"  name='Female'>Female</option>
                                <option selected value=""  name=''></option>
                            <?php
                            }
                            else if($userDetails[0]->getGender() == 'Female')
                            {
                            ?>
                                <option value="Male"  name='Male'>Male</option>
                                <option selected value="Female"  name='Female'>Female</option>
                            <?php
                            }
                            else if($userDetails[0]->getGender() == 'Male')
                            {
                            ?>
                                <option selected value="Male"  name='Male'>Male</option>
                                <option value="Female"  name='Female'>Female</option>
                        <?php
                        }
                        ?>
                    </select>   
                </div>           
                <div class="clear"></div>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Account Status</p>
                    <select class="input-name clean admin-input" name="update_status" id="update_status" value="<?php echo $userDetails[0]->getAccountStatus();?>" required >
                        <!-- <option>Active</option>
                        <option>Banned</option> -->
                        <?php
                            if($userDetails[0]->getAccountStatus() == '')
                            {
                            ?>
                                <option value="Active"  name='Active'>Active</option>
                                <option value="Banned"  name='Banned'>Banned</option>
                                <option selected value=""  name=''></option>
                            <?php
                            }
                            else if($userDetails[0]->getAccountStatus() == 'Banned')
                            {
                            ?>
                                <option value="Active"  name='Active'>Active</option>
                                <option selected value="Banned"  name='Banned'>Banned</option>
                            <?php
                            }
                            else if($userDetails[0]->getAccountStatus() == 'Active')
                            {
                            ?>
                                <option selected value="Active"  name='Active'>Active</option>
                                <option value="Banned"  name='Banned'>Banned</option>
                        <?php
                        }
                        ?>
                    </select>   
                </div>   
                <div class="clear"></div>  
                <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Upload Profile Picture</p>
                    <!-- Photo cropping into square size feature -->
                </div> 
                <div class="clear"></div>
                <p class="review-product-name">Shipping Details</p>        
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Receiver Name</p>
                    <input class="input-name clean input-textarea admin-input" type="text" placeholder="Receiver Name" name="update_receiver_name" id="update_receiver_name" value="<?php echo $userDetails[0]->getReceiverName();?>">    
                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Receiver Contact No.</p>
                    <input class="input-name clean input-textarea admin-input" type="text" placeholder="Receiver Contact Number" name="update_receiver_no" id="update_receiver_no" value="<?php echo $userDetails[0]->getReceiverContactNo();?>">    
                </div>        
                <div class="clear"></div>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">State</p>
                    <select class="input-name clean admin-input" name="update_state" id="update_state" value="<?php echo $userDetails[0]->getShippingState();?>">
                        <!-- <option>State</option>
                        <option>Penang</option> -->
                        <?php
                            if($userDetails[0]->getShippingState() == '')
                            {
                            ?>
                                <option value="State"  name='State'>State</option>
                                <option value="Penang"  name='Penang'>Penang</option>
                                <option selected value=""  name=''></option>
                            <?php
                            }
                            else if($userDetails[0]->getShippingState() == 'Penang')
                            {
                            ?>
                                <option value="State"  name='State'>State</option>
                                <option selected value="Penang"  name='Penang'>Penang</option>
                            <?php
                            }
                            else if($userDetails[0]->getShippingState() == 'State')
                            {
                            ?>
                                <option selected value="State"  name='State'>State</option>
                                <option value="Penang"  name='Penang'>Penang</option>
                        <?php
                        }
                        ?>
                    </select>      
                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Area</p>
                    <select class="input-name clean admin-input" name="update_area" id="update_area" value="<?php echo $userDetails[0]->getShippingArea();?>">
                        <!-- <option>Area</option>
                        <option>Bayan Baru</option> -->
                        <?php
                            if($userDetails[0]->getShippingArea() == '')
                            {
                            ?>
                                <option value="Area"  name='Area'>Area</option>
                                <option value="BB"  name='BB'>Bayan Baru</option>
                                <option selected value=""  name=''></option>
                            <?php
                            }
                            else if($userDetails[0]->getShippingArea() == 'BB')
                            {
                            ?>
                                <option value="Area"  name='Area'>Area</option>
                                <option selected value="BB"  name='BB'>Bayan Baru</option>
                            <?php
                            }
                            else if($userDetails[0]->getShippingArea() == 'Area')
                            {
                            ?>
                                <option selected value="Area"  name='Area'>Area</option>
                                <option value="BB"  name='BB'>Bayan Baru</option>
                        <?php
                        }
                        ?>
                    </select>       
                </div>         
                <div class="clear"></div>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Postal Code</p>
                    <input class="input-name clean input-textarea admin-input" type="text" placeholder="Postal Code" name="update_postal" id="update_postal" value="<?php echo $userDetails[0]->getShippingPostalCode();?>">    
                </div> 
        
                <div class="clear"></div>                
                <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Shipping Address</p>
                    <textarea class="input-name clean input-textarea address-textarea admin-address-textarea" type="text" placeholder="Shipping Address" name="update_address" id="update_address"><?php echo $userDetails[0]->getShippingAddress();?></textarea>
                </div>
                <div class="clear"></div>
                <p class="review-product-name">Bank Account Details 1</p> 
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Bank</p>
                    <select class="input-name clean admin-input" name="update_bank" id="update_bank" value="<?php echo $userDetails[0]->getBankName();?>" required >
                        <!-- <option>Bank</option>
                        <option>Maybank</option>
                        <option>CIMB</option> -->
                        <?php
                            if($userDetails[0]->getBankName() == '')
                            {
                            ?>
                                <option value="Bank"  name='Bank'>Bank</option>
                                <option value="Maybank"  name='Maybank'>Maybank</option>
                                <option value="CIMB"  name='CIMB'>CIMB</option>
                                <option selected value=""  name=''></option>
                            <?php
                            }
                            else if($userDetails[0]->getBankName() == 'CIMB')
                            {
                            ?>
                                <option value="Bank"  name='Bank'>Bank</option>
                                <option value="Maybank"  name='Maybank'>Maybank</option>
                                <option selected value="CIMB"  name='CIMB'>CIMB</option>
                            <?php
                            }
                            else if($userDetails[0]->getBankName() == 'Maybank')
                            {
                                ?>
                                    <option value="Bank"  name='Bank'>Bank</option>
                                    <option selected value="Maybank" name='Maybank'>Maybank</option>
                                    <option value="CIMB"  name='CIMB'>CIMB</option>
                            <?php
                            }
                            else if($userDetails[0]->getBankName() == 'Bank')
                            {
                            ?>
                                <option selected value="Bank"  name='Bank'>Bank</option>
                                <option value="Maybank" name='Maybank'>Maybank</option>
                                <option value="CIMB"  name='CIMB'>CIMB</option>
                        <?php
                        }
                        ?>
                    </select>     
                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Bank Account Holder Name</p>
                    <input class="input-name clean input-textarea admin-input" type="text" placeholder="Bank Account Holder Name" required name="update_bank_holder" id="update_bank_holder" value="<?php echo $userDetails[0]->getBankAccountHolder();?>">      
                </div>        
                <div class="clear"></div>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Bank Account No.</p>
                    <input class="input-name clean input-textarea admin-input" type="text" placeholder="Bank Account No." required name="update_bank_no" id="update_bank_no" value="<?php echo $userDetails[0]->getBankAccountNo();?>">         
                </div>
                <div class="clear"></div>
                <p class="review-product-name">Credit/Debit Card 1</p>

                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Name on Card</p>
                    <input class="input-name clean input-textarea admin-input" type="text" placeholder="Name on Card" required name="update_name_onCard" id="update_name_onCard" value="<?php echo $userDetails[0]->getNameOnCard();?>">    
                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Card Number</p>
                    <input class="input-name clean input-textarea admin-input" type="text" placeholder="Card Number" required name="update_card_no" id="update_card_no" value="<?php echo $userDetails[0]->getCardNo();?>">    
                </div>        
                <div class="clear"></div>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">Card type</p>
                    <select class="input-name clean admin-input" name="update_card_type" id="update_card_type" value="<?php echo $userDetails[0]->getCardType();?>" required >
                        <!-- <option>Card Type</option>
                        <option>Credit</option>
                        <option>Debit</option> -->
                        <?php
                            if($userDetails[0]->getCardType() == '')
                            {
                            ?>
                                <option value="CT"  name='CT'>Card Type</option>
                                <option value="Credit"  name='Credit'>Credit</option>
                                <option value="Debit"  name='Debit'>Debit</option>
                                <option selected value="" name=''></option>
                            <?php
                            }
                            else if($userDetails[0]->getCardType() == 'Debit')
                            {
                            ?>
                                <option value="CT"  name='CT'>Card Type</option>
                                <option value="Credit" name='Credit'>Credit</option>
                                <option selected value="Debit" name='Debit'>Debit</option>
                            <?php
                            }
                            else if($userDetails[0]->getCardType() == 'Credit')
                            {
                                ?>
                                    <option value="CT"  name='CT'>Card Type</option>
                                    <option selected value="Credit" name='Credit'>Credit</option>
                                    <option value="Debit" name='Debit'>Debit</option>
                            <?php
                            }
                            else if($userDetails[0]->getCardType() == 'CT')
                            {
                            ?>
                                <option selected value="CT"  name='CT'>Card Type</option>
                                <option value="Credit" name='Credit'>Credit</option>
                                <option value="Debit" name='Debit'>Debit</option>
                        <?php
                        }
                        ?>
                    </select>      
                </div>
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Expiry Date</p>
                    <input class="input-name clean input-textarea admin-input" type="date" placeholder="Expiry Date" required name="update_expiry" id="update_expiry" value="<?php echo $userDetails[0]->getExpiryDate();?>">          
                </div>         
                <div class="clear"></div>
                <div class="dual-input">
                    <p class="input-top-p admin-top-p">CCV</p>
        
                        <input class="input-name clean input-password edit-password-input admin-input"  type="text" placeholder="CCV" required name="update_ccv" id="update_ccv" value="<?php echo $userDetails[0]->getCcv();?>">
                        <p class="edit-p-password open-ccv"><img src="img/ccv.png" class="hover1a edit-password-img ccv-img" alt="CCV" title="CCV"><img src="img/ccv2.png" class="hover1b edit-password-img ccv-img" alt="CCV" title="CCV"></p>
                        
                </div> 
                <div class="dual-input second-dual-input">
                    <p class="input-top-p admin-top-p">Postal Code</p>
                    <input class="input-name clean input-textarea admin-input" type="text" placeholder="Postal Code" required name="update_postal_code" id="update_postal_code" value="<?php echo $userDetails[0]->getPostalCode();?>">          
                </div>         
                <div class="clear"></div>
                <div class="width100 overflow">
                    <p class="input-top-p admin-top-p">Billing Address</p>
                    <textarea class="input-name clean address-textarea admin-address-textarea" type="text" placeholder="Billing Address" required name="update_billing_address" id="update_billing_address"><?php echo $userDetails[0]->getBillingAddress();?></textarea>      
                </div>

                <input type="hidden" id="id" name="id" value="<?php echo $userDetails[0]->getId() ?>">
            
            <?php
            }
            ?>

            <div class="clear"></div>  
            <div class="width100 overflow text-center">     
                <button class="green-button white-text clean2 edit-1-btn margin-auto" type="submit" id ="editSubmit" name ="editSubmit">Submit</button>
            </div>
        </form>
	</div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

</body>
</html>