<?php  
class Product {
    /* Member variables */
    var $id,$uid,$name,$sku,$slug,$animalType,$category,$brand,$expiryDate,$status,$description,$link,$imageOne,$imageTwo,
    $imageThree,$imageFour,$variationOne,$variationOnePrice,$variationOneStock,$variationOneImage,$variationTwo,
    $variationTwoPrice,$variationTwoStock,$variationTwoImage,$variationThree,$variationThreePrice,$variationThreeStock,
    $variationThreeImage,$variationFour,$variationFourPrice,$variationFourStock,$variationFourImage,$variationFive,
    $variationFivePrice,$variationFiveStock,$variationFiveImage,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getAnimalType()
    {
        return $this->animalType;
    }

    /**
     * @param mixed $animalType
     */
    public function setAnimalType($animalType)
    {
        $this->animalType = $animalType;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param mixed $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return mixed
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }

    /**
     * @param mixed $expiryDate
     */
    public function setExpiryDate($expiryDate)
    {
        $this->expiryDate = $expiryDate;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getImageOne()
    {
        return $this->imageOne;
    }

    /**
     * @param mixed $imageOne
     */
    public function setImageOne($imageOne)
    {
        $this->imageOne = $imageOne;
    }

    /**
     * @return mixed
     */
    public function getImageTwo()
    {
        return $this->imageTwo;
    }

    /**
     * @param mixed $imageTwo
     */
    public function setImageTwo($imageTwo)
    {
        $this->imageTwo = $imageTwo;
    }

    /**
     * @return mixed
     */
    public function getImageThree()
    {
        return $this->imageThree;
    }

    /**
     * @param mixed $imageThree
     */
    public function setImageThree($imageThree)
    {
        $this->imageThree = $imageThree;
    }

    /**
     * @return mixed
     */
    public function getImageFour()
    {
        return $this->imageFour;
    }

    /**
     * @param mixed $imageFour
     */
    public function setImageFour($imageFour)
    {
        $this->imageFour = $imageFour;
    }

    /**
     * @return mixed
     */
    public function getVariationOne()
    {
        return $this->variationOne;
    }

    /**
     * @param mixed $variationOne
     */
    public function setVariationOne($variationOne)
    {
        $this->variationOne = $variationOne;
    }

    /**
     * @return mixed
     */
    public function getVariationOnePrice()
    {
        return $this->variationOnePrice;
    }

    /**
     * @param mixed $variationOnePrice
     */
    public function setVariationOnePrice($variationOnePrice)
    {
        $this->variationOnePrice = $variationOnePrice;
    }

    /**
     * @return mixed
     */
    public function getVariationOneStock()
    {
        return $this->variationOneStock;
    }

    /**
     * @param mixed $variationOneStock
     */
    public function setVariationOneStock ($variationOneStock)
    {
        $this->variationOneStock = $variationOneStock;
    }

    /**
     * @return mixed
     */
    public function getVariationOneImage()
    {
        return $this->variationOneImage;
    }

    /**
     * @param mixed $variationOneImage
     */
    public function setVariationOneImage($variationOneImage)
    {
        $this->variationOneImage = $variationOneImage;
    }

    /**
     * @return mixed
     */
    public function getVariationTwo()
    {
        return $this->variationTwo;
    }

    /**
     * @param mixed $variationTwo
     */
    public function setVariationTwo($variationTwo)
    {
        $this->variationTwo = $variationTwo;
    }

    /**
     * @return mixed
     */
    public function getVariationTwoPrice()
    {
        return $this->variationTwoPrice;
    }

    /**
     * @param mixed $variationTwoPrice
     */
    public function setVariationTwoPrice($variationTwoPrice)
    {
        $this->variationTwoPrice = $variationTwoPrice;
    }

    /**
     * @return mixed
     */
    public function getVariationTwoStock()
    {
        return $this->variationTwoStock;
    }

    /**
     * @param mixed $variationTwoStock
     */
    public function setVariationTwoStock($variationTwoStock)
    {
        $this->variationTwoStock = $variationTwoStock;
    }

    /**
     * @return mixed
     */
    public function getVariationTwoImage()
    {
        return $this->variationTwoImage;
    }

    /**
     * @param mixed $variationTwoImage
     */
    public function setVariationTwoImage($variationTwoImage)
    {
        $this->variationTwoImage = $variationTwoImage;
    }

    /**
     * @return mixed
     */
    public function getVariationThree()
    {
        return $this->variationThree;
    }

    /**
     * @param mixed $variationThree
     */
    public function setVariationThree($variationThree)
    {
        $this->variationThree = $variationThree;
    }

    /**
     * @return mixed
     */
    public function getVariationThreePrice()
    {
        return $this->variationThreePrice;
    }

    /**
     * @param mixed $variationThreePrice
     */
    public function setVariationThreePrice($variationThreePrice)
    {
        $this->variationThreePrice = $variationThreePrice;
    }

    /**
     * @return mixed
     */
    public function getVariationThreeStock()
    {
        return $this->variationThreeStock;
    }

    /**
     * @param mixed $variationThreeStock
     */
    public function setVariationThreeStock($variationThreeStock)
    {
        $this->variationThreeStock = $variationThreeStock;
    }

    /**
     * @return mixed
     */
    public function getVariationThreeImage()
    {
        return $this->variationThreeImage;
    }

    /**
     * @param mixed $variationThreeImage
     */
    public function setVariationThreeImage($variationThreeImage)
    {
        $this->variationThreeImage = $variationThreeImage;
    }

    /**
     * @return mixed
     */
    public function getVariationFour()
    {
        return $this->variationFour;
    }

    /**
     * @param mixed $variationFour
     */
    public function setVariationFour($variationFour)
    {
        $this->variationFour = $variationFour;
    }

    /**
     * @return mixed
     */
    public function getVariationFourPrice()
    {
        return $this->variationFourPrice;
    }

    /**
     * @param mixed $variationFourPrice
     */
    public function setVariationFourPrice($variationFourPrice)
    {
        $this->variationFourPrice = $variationFourPrice;
    }

    /**
     * @return mixed
     */
    public function getVariationFourStock()
    {
        return $this->variationFourStock;
    }

    /**
     * @param mixed $variationFourStock
     */
    public function setVariationFourStock($variationFourStock)
    {
        $this->variationFourStock = $variationFourStock;
    }

    /**
     * @return mixed
     */
    public function getVariationFourImage()
    {
        return $this->variationFourImage;
    }

    /**
     * @param mixed $variationFourImage
     */
    public function setVariationFourImage($variationFourImage)
    {
        $this->variationFourImage = $variationFourImage;
    }

    /**
     * @return mixed
     */
    public function getVariationFive()
    {
        return $this->variationFive;
    }

    /**
     * @param mixed $variationFive
     */
    public function setVariationFive($variationFive)
    {
        $this->variationFive = $variationFive;
    }

    /**
     * @return mixed
     */
    public function getVariationFivePrice()
    {
        return $this->variationFivePrice;
    }

    /**
     * @param mixed $variationFivePrice
     */
    public function setVariationFivePrice($variationFivePrice)
    {
        $this->variationFivePrice = $variationFivePrice;
    }

    /**
     * @return mixed
     */
    public function getVariationFiveStock()
    {
        return $this->variationFiveStock;
    }

    /**
     * @param mixed $variationFiveStock
     */
    public function setVariationFiveStock($variationFiveStock)
    {
        $this->variationFiveStock = $variationFiveStock;
    }

    /**
     * @return mixed
     */
    public function getVariationFiveImage()
    {
        return $this->variationFiveImage;
    }

    /**
     * @param mixed $variationFiveImage
     */
    public function setVariationFiveImage($variationFiveImage)
    {
        $this->variationFiveImage = $variationFiveImage;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getProduct($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","name","sku","slug","animal_type","category","brand","expiry_date","status",
    "description","link","image_one","image_two","image_three","image_four","variation_one","variation_one_price",
    "variation_one_stock","variation_one_image","variation_two","variation_two_price","variation_two_stock",
    "variation_two_image","variation_three","variation_three_price","variation_three_stock","variation_three_image",
    "variation_four","variation_four_price","variation_four_stock","variation_four_image","variation_five",
    "variation_five_price","variation_five_stock","variation_five_image","date_created","date_updated");//follow database

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"product"); //database name
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$name,$sku,$slug,$animalType,$category,$brand,$expiryDate,$status,$description,$link,$imageOne,$imageTwo,
        $imageThree,$imageFour,$variationOne,$variationOnePrice,$variationOneStock,$variationOneImage,$variationTwo,
        $variationTwoPrice,$variationTwoStock,$variationTwoImage,$variationThree,$variationThreePrice,$variationThreeStock,
        $variationThreeImage,$variationFour,$variationFourPrice,$variationFourStock,$variationFourImage,$variationFive,
        $variationFivePrice,$variationFiveStock,$variationFiveImage,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Product;
            $class->setId($id);
            $class->setUid($uid);
            $class->setName($name);
            $class->setSku($sku);
            $class->setSlug($slug);
            $class->setAnimalType($animalType);
            $class->setCategory($category);
            $class->setBrand($brand);
            $class->setExpiryDate($expiryDate);
            $class->setStatus($status);
            $class->setDescription($description);
            $class->setLink($link);
            $class->setImageOne($imageOne);
            $class->setImageTwo($imageTwo);
            $class->setImageThree($imageThree);
            $class->setImageFour($imageFour);
            $class->setVariationOne($variationOne);
            $class->setVariationOnePrice($variationOnePrice);
            $class->setVariationOneStock($variationOneStock);
            $class->setVariationOneImage($variationOneImage);
            $class->setVariationTwo($variationTwo);
            $class->setVariationTwoPrice($variationTwoPrice);
            $class->setVariationTwoStock($variationTwoStock);
            $class->setVariationTwoImage($variationTwoImage);
            $class->setVariationThree($variationThree);
            $class->setVariationThreePrice($variationThreePrice);
            $class->setVariationThreeStock($variationThreeStock);
            $class->setVariationThreeImage($variationThreeImage);
            $class->setVariationFour($variationFour);
            $class->setVariationFourPrice($variationFourPrice);
            $class->setVariationFourStock($variationFourStock);
            $class->setVariationFourImage($variationFourImage);
            $class->setVariationFive($variationFive);
            $class->setVariationFiveImage($variationFivePrice);
            $class->setVariationFiveStock($variationFiveStock);
            $class->setVariationFiveImage($variationFiveImage);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
          
            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}

function createProductList($products,$cartType = 1,$postQuantityRows = null,$isIncludeNotSelectedProductToo = true){
    /*
     * CART TYPE
     * 1 = normal product display cart
     * 2 = checkout cart
     */

    $productListHtml = "";

    if(!$products){
        return $productListHtml; //no product return empty list
    }

    $index = 0;
    foreach ($products as $product){  //foreach element(of $array) defined as $value. Elements of the array will be retrieved inside the loop as $value
        $quantity = 0;
        if($postQuantityRows){
            $quantity = $postQuantityRows[$index];
        }

        if($quantity <= 0 && !$isIncludeNotSelectedProductToo){
            $productListHtml .= '<div style="display: none;">';
        }else{
            $productListHtml .= '<div style="display: block;">';
        }

        $conn=connDB();
        //$productArray = getProduct($conn);
            $id  = $product->getName();
            // Include the database configuration file


            // Get images from the database
            $query = $conn->query("SELECT image_one FROM product WHERE name = '$id'");

            if($query->num_rows > 0){
                while($row = $query->fetch_assoc()){
                    $imageURL = './uploads/'.$row["image_one"];

                    $productListHtml .= '
                            <!-- Product -->
                            <form action="productDetails.php?" method="POST">
                                    <button class="shadow-white-box featured four-box-size" type="submit" name="product_id" value='.$product->getId().'>
                                        <div class="width100 white-bg">
                                            <img src="'.$imageURL.'" class="width100 two-border-radius" alt="'.$product->getName().'" title="'.$product->getName().'" >
                                        </div>
                                        

                                        <div class="width100 product-details-div">
                                            <p class="width100 text-overflow slider-product-name">'.$product->getName().'</p>
                                            <p class="product-price-width text-overflow slider-product-price left-price">RM '.$product->getVariationoneprice().'.00</p>
                                            <p class="slider-product-price right-like hover1">
                                                <img src="img/favourite-1.png" class="hover1a like-img width100" alt="Favourite" title="Favourite">
                                                <img src="img/favourite-2.png" class="hover1b like-img width100" alt="Favourite" title="Favourite">
                                            </p>
                                            <input type="hidden" value="'.$product->getId().'" name="product-list-id-input['.$index.']">
                                            <div class="clear"></div>
                                            <p class="left-rating">5<img src="img/yellow-star.png" class="rating-tiny-star" alt="Rating" title="Rating"></p>
                                            <p class="right-sold">'.$product->getVariationOneStock().' <span class="sold-color">'.$product->getStatus().'</span></p>
                                        </div>
                                    </button>
                            </form>  

                                </div>
                        ';
                }
            }
            $index++;
    }
    return $productListHtml;
}

function getProductPrice($conn,$productId){
    $price = 0;

    $productRows = getProduct($conn," WHERE id = ? ",array("id"),array($productId),"i");

    if($productRows){
        $price = $productRows[0]->getPrice();
    }

    return $price;
}

function addToCart(){
    $shoppingCart = array();

    $totalProductCount = count($_POST['product-list-id-input']);
    for($i = 0; $i < $totalProductCount; $i++){
        $productId = $_POST['product-list-id-input'][$i];
        $quantity = $_POST['product-list-quantity-input'][$i];

//        if($quantity > 0){
        $thisOrder = array();
        $thisOrder['productId'] = $productId;
        $thisOrder['quantity'] = $quantity;
        array_push($shoppingCart,$thisOrder);
//        }
    }

    if(count($shoppingCart) > 0) {
        $_SESSION['shoppingCart'] = $shoppingCart;
    }

}

function clearCart(){
    unset ($_SESSION["shoppingCart"]);
}

//when checkout only create order
function createOrder($conn,$uid){
    if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart'] && count($_SESSION['shoppingCart']) > 0){
        $shoppingCart = $_SESSION['shoppingCart'];
        $orderId = insertDynamicData($conn,"orders",array("uid"),array($uid),"s");
        //$orderId = insertDynamicData($conn,"orders",array("uid","username","full_name"),array($uid,$username,$fullName),"sss");

        if($orderId){
            $totalPrice = 0;

            for($index = 0; $index < count($shoppingCart); $index++){
                $thisCart = $shoppingCart[$index];
                $productId = $thisCart['productId'];
                $quantity = $thisCart['quantity'];
                $originalPrice = getProductPrice($conn,$productId);
                $totalPrice += ($originalPrice * $quantity);

                if(!insertDynamicData($conn,"product_orders",array("product_id","order_id","quantity","final_price","original_price","discount_given"),
                    array($productId,$orderId,$quantity,$originalPrice,$originalPrice,0),"iiiddd")){
                    promptError("error creating order for product : $productId");
                }
            }


            //todo this 2 code is AFTER payment successfully done then only execute
//            insertIntoTransactionHistory($conn,$totalPrice,0,$uid,null,null,null,2,null,$orderId,3,null,null);
//            initiateReward($conn,$orderId,$uid,$totalPrice);

        }else{
            promptError("error creating order");
        }

    }
}

function getShoppingCart($conn,$cartType = 2,$isIncludeNotSelectedProductToo = false){
    $productListHtml = "";

    if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
        $products = array();
        $quantities = array();
        for($index = 0; $index < count($_SESSION['shoppingCart']); $index++){
            $thisCart = $_SESSION['shoppingCart'][$index];

            $tempProductRows = getProduct($conn," WHERE id = ? ",array("id"),array($thisCart['productId']),"i");
            if($tempProductRows){
                array_push($products,$tempProductRows[0]);
                array_push($quantities,$thisCart['quantity']);
            }
        }

        if(count($products) > 0 && count($quantities) > 0 && count($products) == count($quantities)){
            $productListHtml = createProductList($products,$cartType,$quantities,$isIncludeNotSelectedProductToo);
        }
    }

    return $productListHtml;
}