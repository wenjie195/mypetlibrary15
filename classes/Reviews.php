<?php
class Reviews {
    /* Member variables */
    var $id,$uid,$authorUid,$authorName,$title,$paragraphOne,$paragraphTwo,$paragraphThree,$paragraphFour,$image,$type,$display,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getAuthorUid()
    {
        return $this->authorUid;
    }

    /**
     * @param mixed $authorUid
     */
    public function setAuthorUid($authorUid)
    {
        $this->authorUid = $authorUid;
    }

    /**
     * @return mixed
     */
    public function getAuthorName()
    {
        return $this->authorName;
    }

    /**
     * @param mixed $authorName
     */
    public function setAuthorName($authorName)
    {
        $this->authorName = $authorName;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getParagraphOne()
    {
        return $this->paragraphOne;
    }

    /**
     * @param mixed $paragraphOne
     */
    public function setParagraphOne($paragraphOne)
    {
        $this->paragraphOne = $paragraphOne;
    }

    /**
     * @return mixed
     */
    public function getParagraphTwo()
    {
        return $this->paragraphTwo;
    }

    /**
     * @param mixed $paragraphTwo
     */
    public function setParagraphTwo($paragraphTwo)
    {
        $this->paragraphTwo = $paragraphTwo;
    }

    /**
     * @return mixed
     */
    public function getParagraphThree()
    {
        return $this->paragraphThree;
    }

    /**
     * @param mixed $paragraphThree
     */
    public function setParagraphThree($paragraphThree)
    {
        $this->paragraphThree = $paragraphThree;
    }

    /**
     * @return mixed
     */
    public function getParagraphFour()
    {
        return $this->paragraphFour;
    }

    /**
     * @param mixed $paragraphFour
     */
    public function setParagraphFour($paragraphFour)
    {
        $this->paragraphFour = $paragraphFour;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * @param mixed $display
     */
    public function setDisplay($display)
    {
        $this->display = $display;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getReviews($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","author_uid","author_name","title","paragraph_one","paragraph_two","paragraph_three","paragraph_four","image","type","display",
                                "date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"reviews");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$authorUid,$authorName,$title,$paragraphOne,$paragraphTwo,$paragraphThree,$paragraphFour,$image,
                                $type,$display,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Reviews;
            $class->setId($id);
            $class->setUid($uid);
            $class->setAuthorUid($authorUid);
            $class->setAuthorName($authorName);
            $class->setTitle($title);
            $class->setParagraphOne($paragraphOne);
            $class->setParagraphTwo($paragraphTwo);
            $class->setParagraphThree($paragraphThree);
            $class->setParagraphFour($paragraphFour);
            $class->setImage($image);
            $class->setType($type);
            $class->setDisplay($display);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
