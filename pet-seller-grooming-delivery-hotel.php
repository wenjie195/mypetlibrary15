<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pet Seller and Other Services | Mypetslibrary" />
<title>Pet Seller and Other Services | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="pet seller, partner, Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'userHeaderAfterLogin.php'; ?>
	<div class="fix-filter width100 small-padding overflow partner-big-div">
        <h1 class="green-text user-title left-align-title">Partners</h1>
        <div class="partners-search-div">
         <input type="text" placeholder="Search" class="filter-search clean">
         <button class="transparent-button filter-search-btn">
          <img src="img/search.png" class="filter-search-img opacity-hover" alt="Search" title="Search"> 
         </button>
        </div>
        <div class="filter-div">
            <a class="open-partnerfilter filter-a green-a">Filter</a>
        </div>
    </div>

	<div class="clear"></div>    
<div class="width100 small-padding overflow min-height-with-filter filter-distance partner-filter-distance shadow-padding">
	
	<div class="width103">
        <a href="petSellerDetails.php">
            <div class="shadow-white-box four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/pet-seller.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name seller-name">Pet Name Pet Name Pet Name Pet Name </p>
                        <p class="width100 text-overflow slider-location">Location (4/5 <img src="img/yellow-star.png" alt="Rating" title="Rating" class="seller-rating">)</p>
                        <p class="width100 text-overflow slider-location slider-services">Puppy Seller, Pet Grooming, Delivery Services, Restaurant</p>
                  </div>
            </div>
        </a> 
        <a href="petSellerDetails.php">
            <div class="shadow-white-box four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/pet-seller.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name seller-name">Pet Name Pet Name Pet Name Pet Name </p>
                        <p class="width100 text-overflow slider-location">Location (4/5 <img src="img/yellow-star.png" alt="Rating" title="Rating" class="seller-rating">)</p>
                        <p class="width100 text-overflow slider-location slider-services">Puppy Seller, Pet Grooming, Delivery Services, Restaurant</p>
                  </div>
            </div>
        </a>        
        <a href="petSellerDetails.php">
            <div class="shadow-white-box four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/pet-seller.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name seller-name">Pet Name Pet Name Pet Name Pet Name </p>
                        <p class="width100 text-overflow slider-location">Location (4/5 <img src="img/yellow-star.png" alt="Rating" title="Rating" class="seller-rating">)</p>
                        <p class="width100 text-overflow slider-location slider-services">Puppy Seller, Pet Grooming, Delivery Services, Restaurant</p>
                  </div>
            </div>
        </a> 
        <a href="petSellerDetails.php">
            <div class="shadow-white-box four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/pet-seller.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name seller-name">Pet Name Pet Name Pet Name Pet Name </p>
                        <p class="width100 text-overflow slider-location">Location (4/5 <img src="img/yellow-star.png" alt="Rating" title="Rating" class="seller-rating">)</p>
                        <p class="width100 text-overflow slider-location slider-services">Puppy Seller, Pet Grooming, Delivery Services, Restaurant</p>
                  </div>
            </div>
        </a>         
        <a href="petSellerDetails.php">
            <div class="shadow-white-box four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/pet-seller.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name seller-name">Pet Name Pet Name Pet Name Pet Name </p>
                        <p class="width100 text-overflow slider-location">Location (4/5 <img src="img/yellow-star.png" alt="Rating" title="Rating" class="seller-rating">)</p>
                        <p class="width100 text-overflow slider-location slider-services">Puppy Seller, Pet Grooming, Delivery Services, Restaurant</p>
                  </div>
            </div>
        </a> 
        <a href="petSellerDetails.php">
            <div class="shadow-white-box four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/pet-seller.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name seller-name">Pet Name Pet Name Pet Name Pet Name </p>
                        <p class="width100 text-overflow slider-location">Location (4/5 <img src="img/yellow-star.png" alt="Rating" title="Rating" class="seller-rating">)</p>
                        <p class="width100 text-overflow slider-location slider-services">Puppy Seller, Pet Grooming, Delivery Services, Restaurant</p>
                  </div>
            </div>
        </a>        
        <a href="petSellerDetails.php">
            <div class="shadow-white-box four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/pet-seller.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name seller-name">Pet Name Pet Name Pet Name Pet Name </p>
                        <p class="width100 text-overflow slider-location">Location (4/5 <img src="img/yellow-star.png" alt="Rating" title="Rating" class="seller-rating">)</p>
                        <p class="width100 text-overflow slider-location slider-services">Puppy Seller, Pet Grooming, Delivery Services, Restaurant</p>
                  </div>
            </div>
        </a> 
        <a href="petSellerDetails.php">
            <div class="shadow-white-box four-box-size">
                  <div class="width100 white-bg">
                    <img src="img/pet-seller.jpg" alt="Pet Name" title="Pet Name" class="width100 two-border-radius">
                  </div>
                  <div class="width100 product-details-div">
                        <p class="width100 text-overflow slider-product-name seller-name">Pet Name Pet Name Pet Name Pet Name </p>
                        <p class="width100 text-overflow slider-location">Location (4/5 <img src="img/yellow-star.png" alt="Rating" title="Rating" class="seller-rating">)</p>
                        <p class="width100 text-overflow slider-location slider-services">Puppy Seller, Pet Grooming, Delivery Services, Restaurant</p>
                  </div>
            </div>
        </a>                 
        
        
        
        
        
           	
    </div>
</div>
<div class="clear"></div>
<style>
	.animated.slideUp{
		animation:none !important;}
	.animated{
		animation:none !important;}
	.partner-a .hover1a{
		display:none !important;}
	.partner-a .hover1b{
		display:inline-block !important;}	
</style>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>