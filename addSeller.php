<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Seller.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Add Seller | Mypetslibrary" />
<title>Add Seller | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<!-- <?php //include 'userHeaderAfterLogin.php'; ?> -->
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance padding-bottom30">
	<div class="width100">
            <h1 class="green-text h1-title">Add Sellers</h1>
            <div class="green-border"></div>
   </div>
   <div class="border-separation">
        <div class="clear"></div>
 		<form action="utilities/registerSellerFunction.php" method="POST">
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Company Name*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Company Name" required name="register_name" id="register_name" required>      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Company Slug (For URL)*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Company Slug" required name="register_slug" id="register_slug">     
        </div>        
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Company Registered No.</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Company Registered No." name="register_registration_no" id="register_registration_no" required>      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Company Contact</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Company Contact" name="register_contact_no" id="register_contact_no" required>    
        </div>        
        <div class="clear"></div>        
        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Company Address</p>
        	<textarea class="input-name clean input-textarea address-textarea admin-address-textarea" type="text" placeholder="Company Address" name="register_address" id="register_address" required></textarea>
        </div>
        <div class="clear"></div>
        
        <div class="dual-input">
            <p class="input-top-p admin-top-p">Company State</p>
            <input class="input-name clean input-textarea admin-input" type="text" placeholder="Company State" name="register_state" id="register_state" required> 
        	<!-- <select class="input-name clean admin-input" required name="register_state" id="register_state">
            	<option>State</option>
                <option>Penang</option>
            </select>       -->
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Account Status</p>
        	<select class="clean input-name admin-input" type="text" name="register_accStatus" id="register_accStatus" required>
                <option value="" name=" ">Select A Status</option>
            	<option value="Active" name="Active">Active</option>
                <option value="Inactive" name="Inactive">Inactive</option>
            </select>       
        </div>         
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Contact Person Name*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Contact Person Name" name="register_contact_person" id="register_contact_person" required>      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Contact Person Contact No.*</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Contact No." name="register_contact_personNo" id="register_contact_personNo" required>    
        </div>         
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p admin-top-p">Years of Experience</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Years of Experience"  name="register_experience" id="register_experience">      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p admin-top-p">Cert</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Cert"  name="register_cert" id="register_cert">    
        </div>         
        <div class="clear"></div>
        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Services</p>
                <div class="filter-option">
                    <!-- <label for="puppySellers" class="filter-label filter-label2">Puppy Sellers -->
                    <label class="filter-label filter-label2">Puppy Sellers
                        <input type="checkbox" name="register_services[]" id="register_services" value="Puppy Sellers" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>  
                </div>
                <div class="filter-option">
                    <!-- <label for="petHotel" class="filter-label filter-label2">Pet Hotel -->
                        <!-- <input type="checkbox" name="petHotel" id="petHotel" value="0" class="filter-option" /> -->
                    <label class="filter-label filter-label2">Pet Hotel
                        <input type="checkbox" name="register_services[]" id="register_services" value="Pet Hotel" class="filter-option" />
                        <span class="checkmark"></span>
                    </label> 
                </div>
                <div class="filter-option">                
                    <!-- <label for="kittenSellers" class="filter-label filter-label2">Kitten Sellers
                        <input type="checkbox" name="kittenSellers" id="kittenSellers" value="0" class="filter-option" /> -->
                    <label class="filter-label filter-label2">Kitten Sellers
                        <input type="checkbox" name="register_services[]" id="register_services" value="Kitten Sellers" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>  
                </div>
                <div class="filter-option">    
                    <!-- <label for="vet" class="filter-label filter-label2">Vet
                        <input type="checkbox" name="vet" id="vet" value="0" class="filter-option" /> -->
                    <label class="filter-label filter-label2">Vet
                        <input type="checkbox" name="register_services[]" id="register_services" value="Vet" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>  
                </div>
                <div class="filter-option">                 
                    <!-- <label for="reptileSellers" class="filter-label filter-label2">Reptile Sellers
                        <input type="checkbox" name="reptileSellers" id="reptileSellers" value="0" class="filter-option" /> -->
                    <label class="filter-label filter-label2">Reptile Sellers
                        <input type="checkbox" name="register_services[]" id="register_services" value="Reptile Sellers" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>                                     	
                </div>
                <div class="filter-option">                 
                    <!-- <label for="grooming" class="filter-label filter-label2">Grooming
                        <input type="checkbox" name="grooming" id="grooming" value="0" class="filter-option" /> -->
                    <label class="filter-label filter-label2">Grooming
                        <input type="checkbox" name="register_services[]" id="register_services" value="Grooming" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>                                     	
                </div>
                <div class="filter-option">                 
                    <!-- <label for="deliveryServices" class="filter-label filter-label2">Delivery Services
                        <input type="checkbox" name="deliveryServices" id="deliveryServices" value="0" class="filter-option" /> -->
                    <label class="filter-label filter-label2">Delivery Services
                        <input type="checkbox" name="register_services[]" id="register_services" value="Delivery Services" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>                                     	
                </div>
                <div class="filter-option">                 
                    <!-- <label for="restaurant" class="filter-label filter-label2">Restaurant
                        <input type="checkbox" name="restaurant" id="restaurant" value="0" class="filter-option" /> -->
                    <label class="filter-label filter-label2">Restaurant
                        <input type="checkbox" name="register_services[]" id="register_services" value="Restaurant" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>                                     	
                </div>
                <div class="filter-option">                 
                    <!-- <label for="others" class="filter-label filter-label2">Others
                        <input type="checkbox" name="others" id="others" value="0" class="filter-option" /> -->
                    <label class="filter-label filter-label2">Others
                        <input type="checkbox" name="register_services[]" id="register_services" value="Others" class="filter-option" />
                        <span class="checkmark"></span>
                    </label>                                     	
                </div>                                        	
        </div>
        <div class="clear"></div>
        <div class="width100 overflow padding-top30">
        	<p class="input-top-p admin-top-p">Type of Breed</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Type of Breed"  name="register_breed" id="register_breed">           			
        </div>
        <div class="clear"></div>
        <div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Other Info</p>
        	<input class="input-name clean input-textarea admin-input" type="text" placeholder="Other Info Like FB, Insta Link"  name="register_info" id="register_info">        			
        </div>
        <div class="clear"></div>
		<div class="width100 overflow">
        	<p class="input-top-p admin-top-p">Upload Company Logo</p>
            <!-- Photo cropping into square size feature -->
        </div>       
        <div class="clear"></div>  
        <div class="width100 overflow text-center">     
        	<button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
        </div>
        </form>
	</div>
</div>
<div class="clear"></div>
<?php include 'js.php'; ?>

<?php

if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully registered new seller !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "There are no referrer with this email ! Please register again";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "User password must be more than 5 !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "User password does not match";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Registration of new seller failed!";
        } 
        else if($_GET['type'] == 6)
        {
            $messageType = "Registration of new seller as an user !";
        } 
        else if($_GET['type'] == 7)
        {
            $messageType = "Registration data has been taken ! <br> Please enter a new data.";
        } 
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>