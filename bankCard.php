<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Credit/Debit Card | Mypetslibrary" />
<title>Credit/Debit Card | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
	<div class="width100 same-padding overflow min-height menu-distance2">
    	<p class="review-product-name">Credit/Debit Card</p>
        <!-- <form> -->
        <form method="POST" action="utilities/addCreditCardFunction.php">
        <div class="dual-input">
        	<p class="input-top-p">Name on Card</p>
        	<input class="input-name clean" type="text" placeholder="Name on Card" name="card_name" id="card_name" required>      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p">Card No.</p>
        	<input class="input-name clean" type="text" placeholder="Card Number" name="card_number" id="card_number" required>      
        </div>        
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p">Card type</p>
        	<select class="input-name clean" name="card_type" id="card_type" required>
            	<option>Card Type</option>
                <option name="Credit">Credit</option>
                <option name="Debit">Debit</option>
            </select>      
        </div>
        <div class="dual-input second-dual-input">
        	<p class="input-top-p">Expiry Date</p>
        	<input class="input-name clean" type="text" placeholder="MM / YYYY" name="card_expired_date" id="card_expired_date" required>             
        </div>         
        <div class="clear"></div>
        <div class="dual-input">
        	<p class="input-top-p">CCV</p>
            <div class="edit-password-input-div">
            	<input class="input-name clean input-password edit-password-input"  type="text" placeholder="CCV" name="card_ccv" id="card_ccv" required>
                <p class="edit-p-password open-ccv"><img src="img/ccv.png" class="hover1a edit-password-img ccv-img" alt="CCV" title="CCV"><img src="img/ccv2.png" class="hover1b edit-password-img ccv-img" alt="CCV" title="CCV"></p>
            </div>     
        </div> 
        
        <!-- <div class="dual-input second-dual-input">
        	<p class="input-top-p">Postal Code</p>
        	<input class="input-name clean" type="text" placeholder="Postal Code" name="postal_code" id="postal_code" required>             
        </div>          -->

        <div class="clear"></div>
        
        <div class="dual-input">
        	<p class="input-top-p">Billing Address</p>
        	<textarea class="input-name clean address-textarea" type="text" placeholder="Billing Address" name="billing_address" id="billing_address" required></textarea>      
        </div>  

        <div class="dual-input second-dual-input">
        	<p class="input-top-p">Postal Code</p>
        	<input class="input-name clean" type="text" placeholder="Postal Code" name="postal_code" id="postal_code" required>             
        </div>   
               
        <div class="clear"></div>       
        <div class="width100 overflow text-center">     
        	<button class="green-button white-text clean2 edit-1-btn margin-auto" name="submit">Submit</button>
        </div>
        </form>
	</div>
<div class="clear"></div>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>