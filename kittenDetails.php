<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Kitten.php';
// require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $puppies = getPuppy($conn, "WHERE status = 'Available' ");
$kittens = getKitten($conn, "WHERE status = 'Available' ");
// $products = getProduct($conn, "WHERE status = 'Available' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Tri Colour Welsh Corgi (Female) Kitten For Sale | Mypetslibrary" />
<title>Tri Colour Welsh Corgi (Female) Kitten For Sale | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary - Tri Colour Welsh Corgi (Female) Kitten For Sale" />
<meta name="description" content="Mypetslibrary - Tri Colour Welsh Corgi (Female) Kitten For Sale" />
<meta name="keywords" content="Tri Colour Welsh Corgi, Kitten For Sale, Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<?php 
// Program to display URL of current page. 
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') 
$link = "https"; 
else
$link = "http"; 

// Here append the common URL characters. 
$link .= "://"; 

// Append the host(domain name, ip) to the URL. 
$link .= $_SERVER['HTTP_HOST']; 

// Append the requested resource location to the URL 
$link .= $_SERVER['REQUEST_URI']; 

if(isset($_GET['id']))
{
    $referrerUidLink = $_GET['id'];
    // echo $referrerUidLink;
}
else 
{
    $referrerUidLink = "";
    // echo $referrerUidLink;
}
?>

<?php
if(isset($_GET['id']))
{
$conn = connDB();
// $puppiesDetails = getKitten($conn,"WHERE uid = ? ", array("uid") ,array($_GET['id']),"s");
$kittensDetails = getKitten($conn,"WHERE uid = ? ", array("uid") ,array($_GET['id']),"s");
// $puppiesDetails = $puppiesUid[0];
?>
<?php
}
?>

<div class="width100 menu-distance3 same-padding min-height2">

	<div class="left-image-div">

        <?php
        if($kittensDetails)
        {
        for($cnt = 0;$cnt < count($kittensDetails) ;$cnt++)
        {
        ?>

        <div class="item">            
            <div class="clearfix">
                <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                    <li data-thumb="uploads/<?php echo $kittensDetails[$cnt]->getImageOne();?>" class="pet-slider-li"> 
                        <img src="uploads/<?php echo $kittensDetails[$cnt]->getImageOne();?>" class="pet-slider-img" alt="Pet Name" title="Pet Name" />
                    </li>
                    <li data-thumb="uploads/<?php echo $kittensDetails[$cnt]->getImageTwo();?>" class="pet-slider-li"> 
                        <img src="uploads/<?php echo $kittensDetails[$cnt]->getImageTwo();?>" class="pet-slider-img" alt="Pet Name" title="Pet Name"  />
                    </li>
                    <li data-thumb="uploads/<?php echo $kittensDetails[$cnt]->getImageThree();?>" class="pet-slider-li"> 
                        <img src="uploads/<?php echo $kittensDetails[$cnt]->getImageThree();?>" class="pet-slider-img" alt="Pet Name" title="Pet Name"  />
                    </li>
                    <li data-thumb="uploads/<?php echo $kittensDetails[$cnt]->getImageFour();?>" class="pet-slider-li"> 
                        <img src="uploads/<?php echo $kittensDetails[$cnt]->getImageFour();?>" class="pet-slider-img" alt="Pet Name" title="Pet Name"  />
                         </li>
                    <li data-thumb="img/video.jpg" class="pet-slider-li"> 
                        <iframe src="https://player.vimeo.com/video/383039356" class="video-iframe" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                    </li>
                </ul>
            </div>
        </div>
        
        <!-- Display none or add class hidden if the dog not yet sold -->
        <div class="sold-label">Sold</div>
        
        <?php
        }
        ?>
        <?php
        }
        ?>

    </div>
    
    <div class="right-content-div2">

        <?php
        if($kittensDetails)
        {
        for($cnt = 0;$cnt < count($kittensDetails) ;$cnt++)
        {
        ?>
            <p class="green-text breed-p"><?php echo $kittensDetails[$cnt]->getName();?></p>
            <h1 class="green-text pet-name"><?php echo $kittensDetails[$cnt]->getColor();?> <?php echo $kittensDetails[$cnt]->getName();?> (<?php echo $kittensDetails[$cnt]->getGender();?>) Kitten For Sale</h1>
            <p class="price-p2"><?php echo $kittensDetails[$cnt]->getPrice();?></p>
        <?php
        }
        ?>
        <?php
        }
        ?>

        <div class="right-info-div">
        	<a href="tel:+60383190000" class="contact-icon hover1">
            	<img src="img/call.png" class="hover1a" alt="Call" title="Call">
                <img src="img/call2.png" class="hover1b" alt="Call" title="Call">
            </a>
        	<a class="contact-icon hover1">
            	<img src="img/sms.png" class="hover1a" alt="SMS" title="SMS">
                <img src="img/sms2.png" class="hover1b" alt="SMS" title="SMS">
            </a>  
        	<a class="contact-icon hover1">
            	<img src="img/whatsapp.png" class="hover1a" alt="Whatsapp" title="Whatsapp">
                <img src="img/whatsapp2.png" class="hover1b" alt="Whatsapp" title="Whatsapp">
            </a>  
         	<a class="contact-icon hover1">
            	<img src="img/favourite-1.png" class="hover1a" alt="Favourite" title="Favourite">
                <img src="img/favourite-2.png" class="hover1b" alt="Favourite" title="Favourite">
            </a>  

         	<a class="contact-icon hover1 last-contact-icon open-social">
                <img src="img/share.png" class="hover1a" alt="Share" title="Share">
                <img src="img/share2.png" class="hover1b" alt="Share" title="Share">
            </a>
        </div>

        <div class="clear"></div>
            
            <?php
            if($kittensDetails)
            {
            for($cnt = 0;$cnt < count($kittensDetails) ;$cnt++)
            {
            ?>

            <div class="pet-details-div">
            	<table class="pet-table">
                	<tr>
                    	<td class="grey-p">SKU</td>
                        <td class="grey-p">:</td>
                        <!-- <td>Jan-F-02</td> -->
                        <td><?php echo $kittensDetails[$cnt]->getSKU();?></td>
                    </tr>
                	<tr>
                    	<td class="grey-p">Gender</td>
                        <td class="grey-p">:</td>
                        <td><?php echo $kittensDetails[$cnt]->getGender();?></td>
                    </tr>  
                	<tr>
                    	<td class="grey-p">Age</td>
                        <td class="grey-p">:</td>
                        <td><?php echo $kittensDetails[$cnt]->getAge();?></td>
                    </tr>
                	<tr>
                    	<td class="grey-p">Size</td>
                        <td class="grey-p">:</td>
                        <td><?php echo $kittensDetails[$cnt]->getSize();?></td>
                    </tr>  
                	<tr>
                    	<td class="grey-p">Colour</td>
                        <td class="grey-p">:</td>
                        <td><?php echo $kittensDetails[$cnt]->getColor();?></td>
                    </tr>
                	<tr>
                    	<td class="grey-p">Vaccinated</td>
                        <td class="grey-p">:</td>
                        <td><?php echo $kittensDetails[$cnt]->getVaccinated();?></td>
                    </tr> 
                	<tr>
                    	<td class="grey-p">Dewormed</td>
                        <td class="grey-p">:</td>
                        <td><?php echo $kittensDetails[$cnt]->getDewormed();?></td>
                    </tr>
                	<tr>
                    	<td class="grey-p">Additional Information</td>
                        <td class="grey-p">:</td>
                        <!-- <td>With MKA Cert & Microchip</td> -->
                        <td><?php echo $kittensDetails[$cnt]->getDetails();?></td>
                    </tr>                	
                    <tr>
                    	<td class="grey-p">Published Date</td>
                        <td class="grey-p">:</td>
                        <!-- <td>06-Jan-20</td> -->
                        <td><?php echo date("d-m-Y",strtotime($kittensDetails[$cnt]->getDateCreated()));?></td>
                    </tr>                    
                    <tr>
                    	<td class="grey-p">Location</td>
                        <td class="grey-p">:</td>
                        <td><?php echo $kittensDetails[$cnt]->getLocation();?></td>
                    </tr>                    
                    <tr>
                    	<td class="grey-p">Delivery Service</td>
                        <td class="grey-p">:</td>
                        <!-- <td>Yes</td> -->
                        <td><?php echo $kittensDetails[$cnt]->getFeature();?></td>
                    </tr>                                                                                                                                        
                </table>
            </div>
                
            <h1 class="green-text seller-h1">Seller</h1>

            <div class="clear"></div>
            <div class="seller-profile-div">
            	<a href="petSellerDetails.php" class="opacity-hover"><img src="img/partner2.png" class="seller-profile" alt="Pet Seller" title="Pet Seller"></a>
            </div>

            <div class="right-seller-details">
                <!-- <a href="petSellerDetails.php" class="opacity-hover"><h1 class="seller-name-h1">Fur Fur Fur Fur Fur Fur Fur FUr</h1></a> -->
                <a href="#" class="opacity-hover"><h1 class="seller-name-h1"><?php echo $kittensDetails[$cnt]->getSeller();?></h1></a>
                <!-- <a href="petSellerDetails.php" class="opacity-hover"><h1 class="seller-name-h1"><?php //echo $puppiesDetails[$cnt]->getSeller();?></h1></a> -->

                <p class="left-review-p grey-p">Reviews</p>
                <p class="left-review-mark">4/5</p>
                <p class="right-review-star">
                	<img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                    <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                    <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                    <img src="img/yellow-star.png" alt="Review" title="Review" class="star-img">
                    <img src="img/grey-star.png" alt="Review" title="Review" class="star-img last-star">
                </p>
            </div>    

            <?php
            }
            ?>
            <?php
            }
            ?>

    </div>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<div class="sticky-distance2 width100"></div>
<div class="sticky-call-div">
<table class="width100 sticky-table">
	<tbody>
    	<tr>
        	<td>
                <a  href="tel:+60383190000" class="text-center clean transparent-button same-text" >
                    Call
                </a>
        	</td>
            <td>
                <button class="text-center clean transparent-button same-text">
                    SMS
                </button> 
        	</td>
            <td>
                <button class="text-center clean transparent-button same-text">
                    Whatsapp
                </button>
        	</td>
    	</tr>
    </tbody>    
</table>   
</div>

<?php include 'stickyFooter.php'; ?>

<style>
.social-dropdown {
    width: 360px;
}
</style>

</body>
</html>